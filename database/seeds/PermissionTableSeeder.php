<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
            exit();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('permissions')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        //User Permission

        $permission = new \App\Permission;

		$permUser = $permission->create([ 
		    'name'        => 'user',
		    'slug'        => [          // pass an array of permissions.
		        'create'     => true,
		        'view'       => true,
		        'update'     => true,
		        'delete'     => true
		    ],
		    'description' => 'Kullanıcı İzinleri'
		]);

		//KDS Permission

		$permKDS = $permission->create([ 
		    'name'        => 'kds',
		    'slug'        => [          // pass an array of permissions.
		        'view.tig'       => true,
		        'view.dogum'       => true
		    ],
		    'description' => 'KDS Ekranları İzinleri'
		]);

    }
}
