<?php

use Illuminate\Database\Seeder;

class HospitalsKhbTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
            exit();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('HospitalsKhb')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $khb = new \App\Khb;

        $khb->insert([
    [ 'KurumKod' => '108001','KurumAd' => 'TKHK ADANA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ADANA' ],
    [ 'KurumKod' => '108002','KurumAd'  => 'TKHK ADIYAMAN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ADIYAMAN' ],
    [ 'KurumKod' => '108003','KurumAd'  => 'TKHK AFYONKARAHİSAR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'AFYONKARAHİSAR' ],
    [ 'KurumKod' => '108004','KurumAd'  => 'TKHK AĞRI KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'AĞRI' ],
    [ 'KurumKod' => '108005','KurumAd'  => 'TKHK AMASYA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'AMASYA' ],
    [ 'KurumKod' => '108006','KurumAd'  => 'TKHK ANKARA 1.BÖLGE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ANKARA' ],
    [ 'KurumKod' => '108007','KurumAd'  => 'TKHK ANTALYA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ANTALYA' ],
    [ 'KurumKod' => '108008','KurumAd'  => 'TKHK ARTVİN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ARTVİN' ],
    [ 'KurumKod' => '108009','KurumAd'  => 'TKHK AYDIN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'AYDIN' ],
    [ 'KurumKod' => '108010','KurumAd'  => 'TKHK BALIKESİR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BALIKESİR' ],
    [ 'KurumKod' => '108011','KurumAd'  => 'TKHK BİLECİK KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BİLECİK' ],
    [ 'KurumKod' => '108012','KurumAd'  => 'TKHK BİNGÖL KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BİNGÖL' ],
    [ 'KurumKod' => '108013','KurumAd'  => 'TKHK BİTLİS KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BİTLİS' ],
    [ 'KurumKod' => '108014','KurumAd'  => 'TKHK BOLU KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BOLU' ],
    [ 'KurumKod' => '108015','KurumAd'  => 'TKHK BURDUR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BURDUR' ],
    [ 'KurumKod' => '108016','KurumAd'  => 'TKHK BURSA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BURSA' ],
    [ 'KurumKod' => '108017','KurumAd'  => 'TKHK ÇANAKKALE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ÇANAKKALE' ],
    [ 'KurumKod' => '108018','KurumAd'  => 'TKHK ÇANKIRI KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ÇANKIRI' ],
    [ 'KurumKod' => '108019','KurumAd'  => 'TKHK ÇORUM KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ÇORUM' ],
    [ 'KurumKod' => '108020','KurumAd'  => 'TKHK DENİZLİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'DENİZLİ' ],
    [ 'KurumKod' => '108021','KurumAd'  => 'TKHK DİYARBAKIR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'DİYARBAKIR' ],
    [ 'KurumKod' => '108022','KurumAd'  => 'TKHK EDİRNE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'EDİRNE' ],
    [ 'KurumKod' => '108023','KurumAd'  => 'TKHK ELAZIĞ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ELAZIĞ' ],
    [ 'KurumKod' => '108024','KurumAd'  => 'TKHK ERZİNCAN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ERZİNCAN' ],
    [ 'KurumKod' => '108025','KurumAd'  => 'TKHK ERZURUM KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ERZURUM' ],
    [ 'KurumKod' => '108026','KurumAd'  => 'TKHK ESKİŞEHİR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ESKİŞEHİR' ],
    [ 'KurumKod' => '108027','KurumAd'  => 'TKHK GAZİANTEP KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'GAZİANTEP' ],
    [ 'KurumKod' => '108028','KurumAd'  => 'TKHK GİRESUN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'GİRESUN' ],
    [ 'KurumKod' => '108029','KurumAd'  => 'TKHK GÜMÜŞHANE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'GÜMÜŞHANE' ],
    [ 'KurumKod' => '108030','KurumAd'  => 'TKHK HAKKARİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'HAKKARİ' ],
    [ 'KurumKod' => '108031','KurumAd'  => 'TKHK HATAY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'HATAY' ],
    [ 'KurumKod' => '108032','KurumAd'  => 'TKHK ISPARTA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ISPARTA' ],
    [ 'KurumKod' => '108033','KurumAd'  => 'TKHK MERSİN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'MERSİN' ],
    [ 'KurumKod' => '108034','KurumAd'  => 'TKHK İSTANBUL BEYOĞLU KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İSTANBUL' ],
    [ 'KurumKod' => '108035','KurumAd'  => 'TKHK İZMİR KUZEY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İZMİR' ],
    [ 'KurumKod' => '108036','KurumAd'  => 'TKHK KARS KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KARS' ],
    [ 'KurumKod' => '108037','KurumAd'  => 'TKHK KASTAMONU KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KASTAMONU' ],
    [ 'KurumKod' => '108038','KurumAd'  => 'TKHK KAYSERİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KAYSERİ' ],
    [ 'KurumKod' => '108039','KurumAd'  => 'TKHK KIRKLARELİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KIRKLARELİ' ],
    [ 'KurumKod' => '108040','KurumAd'  => 'TKHK KIRŞEHİR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KIRŞEHİR' ],
    [ 'KurumKod' => '108041','KurumAd'  => 'TKHK KOCAELİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KOCAELİ' ],
    [ 'KurumKod' => '108042','KurumAd'  => 'TKHK KONYA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KONYA' ],
    [ 'KurumKod' => '108043','KurumAd'  => 'TKHK KÜTAHYA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KÜTAHYA' ],
    [ 'KurumKod' => '108044','KurumAd'  => 'TKHK MALATYA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'MALATYA' ],
    [ 'KurumKod' => '108045','KurumAd'  => 'TKHK MANİSA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'MANİSA' ],
    [ 'KurumKod' => '108046','KurumAd'  => 'TKHK KAHRAMANMARAŞ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KAHRAMANMARAŞ' ],
    [ 'KurumKod' => '108047','KurumAd'  => 'TKHK MARDİN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'MARDİN' ],
    [ 'KurumKod' => '108048','KurumAd'  => 'TKHK MUĞLA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'MUĞLA' ],
    [ 'KurumKod' => '108049','KurumAd'  => 'TKHK MUŞ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'MUŞ' ],
    [ 'KurumKod' => '108050','KurumAd'  => 'TKHK NEVŞEHİR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'NEVŞEHİR' ],
    [ 'KurumKod' => '108051','KurumAd'  => 'TKHK NİĞDE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'NİĞDE' ],
    [ 'KurumKod' => '108052','KurumAd'  => 'TKHK ORDU KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ORDU' ],
    [ 'KurumKod' => '108053','KurumAd'  => 'TKHK RİZE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'RİZE' ],
    [ 'KurumKod' => '108054','KurumAd'  => 'TKHK SAKARYA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'SAKARYA' ],
    [ 'KurumKod' => '108055','KurumAd'  => 'TKHK SAMSUN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'SAMSUN' ],
    [ 'KurumKod' => '108056','KurumAd'  => 'TKHK SİİRT KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'SİİRT' ],
    [ 'KurumKod' => '108057','KurumAd'  => 'TKHK SİNOP KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'SİNOP' ],
    [ 'KurumKod' => '108058','KurumAd'  => 'TKHK SİVAS KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'SİVAS' ],
    [ 'KurumKod' => '108059','KurumAd'  => 'TKHK TEKİRDAĞ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'TEKİRDAĞ' ],
    [ 'KurumKod' => '108060','KurumAd'  => 'TKHK TOKAT KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'TOKAT' ],
    [ 'KurumKod' => '108061','KurumAd'  => 'TKHK TRABZON KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'TRABZON' ],
    [ 'KurumKod' => '108062','KurumAd'  => 'TKHK TUNCELİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'TUNCELİ' ],
    [ 'KurumKod' => '108063','KurumAd'  => 'TKHK ŞANLIURFA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ŞANLIURFA' ],
    [ 'KurumKod' => '108064','KurumAd'  => 'TKHK UŞAK KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'UŞAK' ],
    [ 'KurumKod' => '108065','KurumAd'  => 'TKHK VAN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'VAN' ],
    [ 'KurumKod' => '108066','KurumAd'  => 'TKHK YOZGAT KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'YOZGAT' ],
    [ 'KurumKod' => '108067','KurumAd'  => 'TKHK ZONGULDAK KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ZONGULDAK' ],
    [ 'KurumKod' => '108068','KurumAd'  => 'TKHK AKSARAY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'AKSARAY' ],
    [ 'KurumKod' => '108069','KurumAd'  => 'TKHK BAYBURT KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BAYBURT' ],
    [ 'KurumKod' => '108070','KurumAd'  => 'TKHK KARAMAN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KARAMAN' ],
    [ 'KurumKod' => '108071','KurumAd'  => 'TKHK KIRIKKALE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KIRIKKALE' ],
    [ 'KurumKod' => '108072','KurumAd'  => 'TKHK BATMAN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BATMAN' ],
    [ 'KurumKod' => '108073','KurumAd'  => 'TKHK ŞIRNAK KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ŞIRNAK' ],
    [ 'KurumKod' => '108074','KurumAd'  => 'TKHK BARTIN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'BARTIN' ],
    [ 'KurumKod' => '108075','KurumAd'  => 'TKHK ARDAHAN KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ARDAHAN' ],
    [ 'KurumKod' => '108076','KurumAd'  => 'TKHK IĞDIR KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'IĞDIR' ],
    [ 'KurumKod' => '108077','KurumAd'  => 'TKHK YALOVA KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'YALOVA' ],
    [ 'KurumKod' => '108078','KurumAd'  => 'TKHK KARABÜK KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KARABÜK' ],
    [ 'KurumKod' => '108079','KurumAd'  => 'TKHK KİLİS KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'KİLİS' ],
    [ 'KurumKod' => '108080','KurumAd'  => 'TKHK OSMANİYE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'OSMANİYE' ],
    [ 'KurumKod' => '108081','KurumAd'  => 'TKHK DÜZCE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'DÜZCE' ],
    [ 'KurumKod' => '760439','KurumAd'  => 'TKHK ANKARA 2.BÖLGE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ANKARA' ],
    [ 'KurumKod' => '760440','KurumAd'  => 'TKHK İSTANBUL FATİH KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İSTANBUL' ],
    [ 'KurumKod' => '760441','KurumAd'  => 'TKHK İSTANBUL BAKIRKÖY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İSTANBUL' ],
    [ 'KurumKod' => '760442','KurumAd'  => 'TKHK İSTANBUL ANADOLU KUZEY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İSTANBUL' ],
    [ 'KurumKod' => '760443','KurumAd'  => 'TKHK İSTANBUL ANADOLU GÜNEY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İSTANBUL' ],
    [ 'KurumKod' => '760444','KurumAd'  => 'TKHK İZMİR GÜNEY KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İZMİR' ],
    [ 'KurumKod' => '797817','KurumAd'  => 'TKHK İSTANBUL ÇEKMECE BÖLGESİ KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'İSTANBUL' ],
    [ 'KurumKod' => '821140','KurumAd'  => 'TKHK ANKARA 3.BÖLGE KAMU HASTANELERİ BİRLİĞİ GENEL SEKRETERLİĞİ', 'Il'=> 'ANKARA' ]
]);
    }
}
