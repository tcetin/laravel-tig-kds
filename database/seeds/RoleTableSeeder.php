<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        if (App::environment() === 'production') {
            exit();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('roles')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \App\Role::create([
            'id'            => 1,
            'name'          => 'Owner',
            'slug'			=> 'owner',
            'description'   => 'En yetkili roldür.Genel KDS ye erişir ve kullanıcı yetkilendirmesi yapar.'
        ]);
        \App\Role::create([
            'id'            => 2,
            'name'          => 'Administrator',
            'slug'			=> 'administrator',
            'description'   => 'Genel KDS ye erişir.'
        ]);
        \App\Role::create([
            'id'            => 3,
            'name'          => 'GSMember',
            'slug'			=> 'gsmember',
            'description'   => 'Genel Sekreterlik Kullanıcısıdır.Kendine bağlı kurumların verisini görür.'
        ]);
       \App\Role::create([
            'id'            => 4,
            'name'          => 'HSMember',
            'slug'			=> 'hsmember',
            'description'   => 'Sağlık Tesisi Kullanıcısıdır.Daha Çok Hastane Yöneticisidir.'
        ]);

    }
}
