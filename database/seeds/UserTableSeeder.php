<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
            exit();
        }

        //create admin user

        $user = new \App\User;

        $user->create([
            'username' => 'admin',
            'first_name' => 'owner',
            'last_name' => 'owner',
            'email' => 'admin@admin.com',
            'password' => bcrypt('tigkds332211')
        ]);
    }
}
