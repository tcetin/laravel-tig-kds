<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->call(RoleTableSeeder::class);
       $this->call(PermissionTableSeeder::class);
       $this->call(UserTableSeeder::class);
       $this->call(HospitalsKhbTableSeeder::class);
       $this->call(SutKodlariTableSeeder::class);

       //owner role permission ver

       \App\Role::where('slug','=','owner')->firstOrFail()->assignPermission('user,kds');



       //ownera rol ata(admin kullanıcısı)

        $user = \App\User::where('username','=','admin')->firstOrFail();
        if(!$user->hasRole('owner')){
	        $user->assignRole('owner');
	        $user->save();
	    }

    }
}
