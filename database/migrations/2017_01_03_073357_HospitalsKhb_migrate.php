<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HospitalsKhbMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('HospitalsKhb', function (Blueprint $table) {
            $table->increments('id');
            $table->string('KurumKod')->unique();
            $table->string('KurumAd')->unique();
            $table->string('Il');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('HospitalsKhb');
    }
}
