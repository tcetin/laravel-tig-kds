<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSutIslemleriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sut_islem_dosyalari', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hastane_kodu')->unsigned()->index();
            $table->string('kullanici');
            $table->string('yil');
            $table->timestamps();
        });

        Schema::create('sut_islemleri', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sut_kodu')->index();
            $table->string('islem_sayisi')->nullable();
            $table->string('ortalama_yatis_suresi')->nullable();
            $table->string('tibbi_malzeme_tutari')->nullable();
            $table->string('ilac_tutar')->nullable();
            $table->string('islem_tutari')->nullable();
            $table->string('uzman_hekim_sayisi')->nullable();
            $table->string('anestezi_uzmani_sayisi')->nullable();
            $table->string('ysp_sayisi')->nullable();//yardımcı sağlık pers.
            $table->string('asistan_hekim_sayisi')->nullable();
            $table->string('islem_suresi_dk')->nullable();
            $table->integer('dosya_id')->unsigned();
            $table->timestamps();

            $table->foreign('dosya_id')
                ->references('id')->on('sut_islem_dosyalari')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sut_islemleri');
        Schema::dropIfExists('sut_islem_dosyalari');
    }
}
