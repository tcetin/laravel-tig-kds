<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSutEk2cIslemleriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sut_ek2c_islem_dosyalari', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hastane_kodu')->unsigned()->index();
            $table->string('kullanici');
            $table->string('yil');
            $table->timestamps();
        });

        Schema::create('sut_ek2c_islemleri', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sut_kodu')->index();
            $table->string('hasta_basina_tibbi_malzeme_gideri_paket')->nullable();//paket dahil
            $table->string('hasta_basina_tibbi_malzeme_gideri')->nullable();
            $table->string('hasta_basina_ilac_gideri_paket')->nullable();
            $table->string('hasta_basina_ilac_gideri')->nullable();
            $table->string('hasta_basina_lab_gideri')->nullable();
            $table->string('hasta_basina_radyoloji_gideri')->nullable();
            $table->string('hasta_basina_diger_gideri')->nullable();
            $table->integer('dosya_id')->unsigned();
            $table->timestamps();

            $table->foreign('dosya_id')
                ->references('id')->on('sut_ek2c_islem_dosyalari')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sut_ek2c_islem_dosyalari');
        Schema::dropIfExists('sut_ek2c_islemleri');
    }
}
