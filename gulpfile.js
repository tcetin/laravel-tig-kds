const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {

    mix.sass('app.scss')
       .webpack('app.js');

    mix.scripts([
        // "jquery.js",
        "bootstrapp.js",
        "bootstrap-progressbar.js",
        "fastclick.js",
        "nprogress.js",
        "icheck.js",
        "custom.js",
        "bootstrap-table.js",
        "bootstrap-table-export.js",
        "table-export.js",
        "bootstrap-table-tr.js",
        "bootstrap-table-filter-control.js",
        "tig-custom.js",
        "ion.range-slider.min.js",
        "jquery.gsap.min.js",
        "TimeLineLite.min.js",
        "TwinLite.min.js",
        // "select2.min.js",
        "TweenMax.min.js",
        "amcharts.min.js",
        "amcharts.serial.min.js",
        "amcharts.export.min.js",
        "amcharts.light.min.js",
        "amcharts.fileSaver.js",
        "amcharts.jszip.min.js",
        "amcharts.fabric.min.js",
        "amcharts.xlsx.min.js",
        "amcharts.pdfmake.min.js",
        "amcharts.lang.tr.js",
        "amcharts.export.translations.js",
        "amcharts.dataloader.js",
        "ammap.min.js",
        "ammap_amcharts_extension.min.js",
        "ammap.turkeyLow.js",
        "amcharts.xy.min.js",
        "jquery.autocomplete.js",
        "d3.min.js",
        "crossfilter.js",
        "dc.min.js",
        "d3.tip.js",
        "geo-choropleth-chart.js",
        "yd-vis.js",
        "ndogum-tani-vis.js",
        "tani-yas-cinsiyet-vis.js",
        "kullanici_yonetimi.js",
        "kurum_yonetimi.js",
        "bootstrap-table-init.js",
        "axios.min.js",
        "paper-bootstrap-wizard.js",
        "jquery.bootstrap.wizard.js",
        "jquery.validate.min.js",
        "dinamik_rapor.js",
        "holdon.js",
        "piwik.js"

    ]);

    mix.styles([
        "bootstrap.css",
        "font-awesome.css",
        "nprogress.css",
        "green.css",
        "bootstrap-progressbar-3.3.4.css",
        "custom.css",
        "fresh-bootstrap-table.css",
        "tig-custom.css",
        "ion.rangeSlider.css",
        "ion.range-slider-skin-modern.css",
        "amcharts.export.min.css",
        "ammap.css",
        "dc.css",
        "d3.tip.css",
        // "select2.min.css",
        "paper-bootstrap-wizard.css",
        "themify-icons.css",
        "holdon.css"
        //"bootstrap-table.min.css"

    ]);
});
