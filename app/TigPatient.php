<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\Filterable;

class TigPatient extends Model
{
    protected $connection='sqlsrv';
    protected $table='TIGPatient';

}
