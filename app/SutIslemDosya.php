<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SutIslemDosya extends Model
{
    protected $table = 'sut_islem_dosyalari';
    protected $fillable = ['hastane_kodu','kullanici','yil'];

    //1-n
    public function sutIslemleri()
    {
        return $this->hasMany('App\SutIslem','dosya_id','id');
    }
}
