<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SutIslem extends Model
{
    protected $table='sut_islemleri';
    protected $fillable = [
        'sut_kodu',
        'islem_sayisi',
        'ortalama_yatis_suresi',
        'tibbi_malzeme_tutari',
        'ilac_tutar',
        'islem_tutari',
        'uzman_hekim_sayisi',
        'anestezi_uzmani_sayisi',
        'ysp_sayisi',
        'asistan_hekim_sayisi',
        'islem_suresi_dk',
        'dosya_id' 
    ];

    public function islemDosya()
    {
        return $this->belongsTo('App\SutIslemDosya','dosya_id');
    }
}
