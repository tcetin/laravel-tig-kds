<?php
namespace App\Filters;
use Illuminate\Database\Eloquent\Builder;

class OecdHastaneBazliKlinikVeriFilters extends QueryFilters
{

    public function HASTANE_KODU($hospital)
    {
       return $this->builder->where('HASTANE_KODU',$hospital); 
    }

    public function from($from)
    {
       return $this->builder->where('DONEM_KODU','>=',$from);
    }

    public function to($to)
    {
       return $this->builder->where('DONEM_KODU','<=',$to);
    }
    
    public function offset($offset)
    {
        return $this->builder->offset($offset);
    }

    public function limit($limit)
    {
        return $this->builder->limit($limit);
    }

    public function filter($filter)
    {
        $filteredDataObject = json_decode($filter);
        
        $filterData = array();
        foreach($filteredDataObject as $key=>$value){
           $filterData[$key]=$value;  
        }

        return $this->builder->where($filterData);
    }


}

?>