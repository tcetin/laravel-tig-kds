<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
  //protected $connection='sqlsrv';
  protected $table='Hospitals';

  public function HospitalKhb()
  {
    return $this->belongsTo('App\Khb','Khb','KurumKod');
  }

    public function gider()
    {
        return $this->hasMany('\App\TibbiGider');
    }
}
