<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TigKod extends Model
{
    protected $connection='sqlsrv';
    protected $table ='KODLAR';
}
