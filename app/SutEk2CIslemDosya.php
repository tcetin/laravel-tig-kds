<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SutEk2CIslemDosya extends Model
{
    protected $table = 'sut_ek2c_islem_dosyalari';
    protected $fillable = ['hastane_kodu','kullanici','yil'];


    public function sutIslemleri()
    {
        return $this->hasMany('App\SutEk2CIslem','dosya_id','id');
    }
}
