<?php

namespace App\Http\Controllers;

use App\TigKod;
use App\TigPatient;
use App\TigPeriod;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use \Cache;
use Auth;
use League\Flysystem\Exception;
use Maatwebsite\Excel\Facades\Excel;
use Log;

class TigController extends Controller
{
    protected $donemsel_hastane_verileri;
    protected $donemsel_hastane_yatisbazli_veriler;
    protected $kurum_bazli_klinik_kodlayici_sayilari;
    protected $user;
    protected $hospital_list;


    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->hospital_list = DB::table('Hospitals')->select('*');

            if (Auth::user()->hasRole('hsmember')) {
                $this->hospital_list = $this->hospital_list->where('HospitalCode', $this->user->hospital_code);
            } elseif (Auth::user()->hasRole('gsmember')) {
                $this->hospital_list = $this->hospital_list->where('Khb', $this->user->hospital_code);
            }

            return $next($request);
        });


    }

    /**
     * returns max period from TIGPatient
     * @return [type] [description]
     */
    public function maxPeriod()
    {
        return DB::connection('sqlsrv')->table('TIGPatient')->max('PeriodId');
    }

    /**
     * @return View
     */
    public function donemselHastaneVerileri()
    {
        $donemArr = DB::connection('sqlsrv')->table('KART_DONEM AS tbl')
            ->select('tbl.Id', 'tbl.DonemAdi')
            ->where('tbl.Id', '>=', 39)
            ->get();

        return view('pages.tig.donemsel_hastane_verileri', compact('donemArr'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function donemselHastaneVerileriniGetir(Request $request)
    {

        $this->validate($request, [
            'to' => 'required',
            'from' => 'required'
        ]);


        $this->donemsel_hastane_verileri = DB::connection('sqlsrv')->table('kds.v_hastane_donemsel_hesaplama AS tbl')
            ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.PeriodId')
            ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'tbl.HospitalCode')
            ->join('Cities', 'Cities.CityCode', '=', 'Hospitals.HospitalCity')
            ->where('PeriodId', '<=', $request->to)
            ->where('PeriodId', '>=', $request->from)
            ->orderBy('PeriodId')
            ->select('tbl.*', 'KART_DONEM.DonemAdi', 'Hospitals.HospitalName','Cities.CityName',
                DB::raw('(CASE  WHEN Hospitals.Statu=\'O\' THEN \'Özel\' 
                                WHEN Hospitals.Statu=\'U\' THEN \'Üniversite\'
                                WHEN Hospitals.Statu=\'D\' THEN \'Kamu\' END) AS HospitalStatu'));

        if ($this->user->hasRole('hsmember')) {
            $this->donemsel_hastane_verileri = $this->donemsel_hastane_verileri->where('tbl.HospitalCode', $this->user->hospital_code);
        } elseif ($this->user->hasRole('gsmember')) {
            $hospitals = DB::table('Hospitals')->where('Khb', $this->user->hospital_code)->get();
            $hospArray = [];

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }

            $this->donemsel_hastane_verileri = $this->donemsel_hastane_verileri->whereIn('tbl.HospitalCode', $hospArray);
        }

        $hospitalDatas = $this->donemsel_hastane_verileri->get();

        return response()->json($hospitalDatas);

    }

    /**
     * @return View
     */
    public function bransBazliEmekVerileri()
    {
        $donemArr = DB::connection('sqlsrv')->table('KART_DONEM AS tbl')
            ->select('tbl.Id', 'tbl.DonemAdi')
            ->where('tbl.Id', '>=', 39)
            ->get();

        return view('pages.tig.hastane_brans_bazli_emek_verileri', compact('donemArr'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bransBazliEmekVerileriniGetir(Request $request)
    {
        $this->validate($request, [
            'to' => 'required',
            'from' => 'required'
        ]);

        $maxPeriod = $this->maxPeriod();

        $data = DB::connection('sqlsrv')
            ->table('TIGPatient AS tbl WITH(NOLOCK)')
            ->join('KART_HESAPLANANTIG', 'KART_HESAPLANANTIG.TIG', '=', 'tbl.DRG')
            ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.PeriodId')
            ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'tbl.HospitalCode')
            ->join('Cities', 'Cities.CityCode', '=', 'Hospitals.HospitalCity')
            ->join('TB_BRANS', 'TB_BRANS.BRANS', '=', 'tbl.DrBrans')
            ->where('tbl.PeriodId', '<=', $request->to)
            ->where('tbl.PeriodId', '>=', $request->from)
            ->groupBy('tbl.PeriodId', 'KART_DONEM.DonemAdi', 'tbl.HospitalCode', 'Hospitals.HospitalName', 'Cities.CityName', 'TB_BRANS.ACIKLAMA')
            ->select(
                'tbl.PeriodId AS DonemKodu',
                'KART_DONEM.DonemAdi',
                'tbl.HospitalCode',
                'Hospitals.HospitalName',
                'Cities.CityName',
                'TB_BRANS.ACIKLAMA AS Brans',
                DB::raw('COUNT(tbl.DRG) AS YatisTigFrekans'),
                DB::raw('COUNT(DISTINCT tbl.DRG) AS YatisTigCesitliligi'),
                DB::raw("REPLACE(SUM (KART_HESAPLANANTIG.EMEKBAGILDEGER),'.',',') AS YatisTigBagil"),
                DB::raw("REPLACE(SUM(KART_HESAPLANANTIG.EMEKBAGILDEGER)/COUNT(tbl.DRG),'.',',') AS Vki")
            );


        if ($this->user->hasRole('hsmember')) {
            $data = $data->where('tbl.HospitalCode', $this->user->hospital_code);
        } elseif ($this->user->hasRole('gsmember')) {
            $hospitals = DB::table('Hospitals')->where('Khb', $this->user->hospital_code)->get();
            $hospArray = [];

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }

            $data = $data->whereIn('tbl.HospitalCode', $hospArray);
        }

    //Start Pagination
    
   /* if($request->filter){
        $filteredDataObject = json_decode($request->filter);
        $filterData = array();
        foreach($filteredDataObject as $key=>$value){
           $filterData[$key]=$value;
        }
        $data = $data->where($filterData);
    }

    if($request->sort && $request->order){
        $data = $data->orderBy($request->sort,$request->order);
    }

    $limit = $request->limit ?? 10;
    
    $total = 100;


    $data = $data->offset($request->offset)->limit($request->limit);

    $responseData = $data->get();

    $response = ['total'=>$total,'rows'=>$responseData];

    //dd($response);

    //End Pagination
         */

        $responseData = $data->get();

        return response()->json($responseData);
    }

    /**
     * @return View
     */


    public function donemselHastaneYatisBazliTigVerileri()
    {

        $hospitals = DB::table('Hospitals')->select('HospitalCode', 'HospitalName');

        if ($this->user->hasRole('administrator') || $this->user->hasRole('owner')) {
            $hospitals = $hospitals->get();
        } elseif ($this->user->hasRole('hsmember')) {
            $hospitals = $hospitals->where('Hospitals.HospitalCode', $this->user->hospital_code)->get();
        } elseif ($this->user->hasRole('gsmember')) {
            $hospitals = $hospitals->where('Hospitals.Khb', $this->user->hospital_code)->get();
        }

        return view('pages.tig.donemsel_hastane_yatis_bazli_tig_verileri', compact('hospitals'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */

    public function donemselHastaneYatisBazliTigVerileriniGetir(Request $request)
    {

        ini_set('memory_limit', '-1');

        //\DB::setAttribute(DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER,true);
        //DB::connection('sqlsrv')->getPdo()->setAttribute(\PDO::DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER, true);

         //$drgVerileriCache = 'hastane_drg_verileri-cache'.$request->to.'_'.$request->from.'_'.implode("_",$request->hospitals);

        $response = DB::connection('sqlsrv')
            ->table('kds.v_hastane_maliyet')
            ->where('PeriodId', '<=', $request->to)
            ->where('PeriodId', '>=', $request->from)
            ->whereIn('HospitalCode', $request->hospitals)
            ->orderBy('PeriodId', 'asc')
            ->get();

        return response()->json($response);


    }

    /**
     * @param $s
     * @return string
     */

    public function normalizeChars($s)
    {
        $replace = array('ý' => 'ı', 'þ' => 'ş', 'ð' => 'ğ', 'Ý' => 'İ', 'Þ' => 'Ş', 'Ð' => 'Ğ');
        return strtr(utf8_encode($s), $replace);
    }



    /**
     * DönemSel Hastane Bazlı Veriler
     */


    public function donemselHastaneBazliDrgSayilari()
    {
        return view('pages.tig.donemsel_hastanebazli_drg_sayilari');
    }

    public function donemselHastaneBazliDrgSayilariniGetir(Request $request)
    {
        ini_set('memory_limit', '1G');

        $datas = Cache::remember('donemsel_hastane_bazli_drg_sayilari', '10080', function () {
            return DB::table('HastaneBazliDrgSayilari AS tbl')
                ->join('KART_DONEM', 'KART_DONEM.DonemKodu', '=', 'tbl.DonemKodu')
                ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'tbl.HospitalCode')
                ->join('KART_TIG', 'KART_TIG.TIG', '=', 'tbl.DRG')
                ->select('tbl.*', 'KART_DONEM.DonemAdi', 'Hospitals.HospitalName', 'KART_TIG.TANIM', 'KART_TIG.BAGIL')
                ->get();
        });

        if (isset($request->from) && isset($request->to)) {
            $datas = $datas->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
        } else {
            $datas = $datas->where('DonemKodu', '=', $this->maxPeriod());
        }

        return response()->json($datas);
    }

    /**
     * @return View
     */
    public function turkiyeGeneliDonemselVeriler()
    {
        return view('pages.tig.donemsel_turkiye_geneli_tig_verileri');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|View
     */
    public function turkiyeGeneliDonemselVerileriniGetir(Request $request)
    {

        $response = DB::connection('sqlsrv')->table('TIGPatient')
            ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'TIGPatient.PeriodId')
            ->join('KART_HESAPLANANTIG', 'TIGPatient.DRG', '=', 'KART_HESAPLANANTIG.TIG')
            ->select(
                'TIGPatient.PeriodId AS DonemKodu',
                'KART_DONEM.DonemAdi',
                'TIGPatient.PeriodId AS Durum',
                DB::raw('COUNT(TIGPatient.DRG) AS YatisTigFrekans'),
                DB::raw('SUM(KART_HESAPLANANTIG.BAGIL) AS YatisTigBagil'),
                DB::raw('COUNT(DISTINCT TIGPatient.DRG) AS YatisTigCesitliligi'),
                DB::raw('SUM(DATEDIFF(DAY,TIGPatient.AdmissionDate,TIGPatient.DischargeDate))/COUNT(*) AS OrtalamayatisGunu')
            )
            ->where('TIGPatient.PeriodId', '<=', $request->to)
            ->where('TIGPatient.PeriodId', '>=', $request->from)
            ->groupBy('TIGPatient.PeriodId', 'KART_DONEM.DonemAdi')
            ->orderBy('TIGPatient.PeriodId', 'asc')
            ->get();


        return response()->json($response);

    }

    /**
     * @return View
     */
    public function turkiyeGeneliDonemselDrgBazliVeriler()
    {
        return view('pages.tig.donemsel_turkiye_geneli_drg_bazli_veriler');
    }

    public function turkiyeGeneliDonemselDrgBazliVerileriniHesapla(Request $request)
    {
        ini_set('memory_limit', '1G');

        $cacheStr = 'donemsel_hastane_bazli_drg_verileri_' . $request->to . '_' . $request->from;

        $data = Cache::remember($cacheStr, '10080', function () use ($request) {
            return DB::connection('sqlsrv')->table('kds.v_hastane_bazli_drg_verileri as tbl')
                ->where('tbl.DonemKodu', '<=', $request->to)
                ->where('tbl.DonemKodu', '>=', $request->from)
                ->select('tbl.*')
                ->orderBy('tbl.DonemKodu')
                ->get();
        });

        return response()->json($data);
    }

    /**
     * @return View
     */

    public function turkiyeGeneliDonemselVakaSayilari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_vaka_sayilari');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function turkiyeGeneliDonemselVakaSayilariniGetir(Request $request)
    {

        $cacheStr = 'vaka_sayilari_' . $request->to . '_' . $request->from;

        $data = Cache::remember($cacheStr, '10080', function () use ($request) {

            $result = DB::connection('sqlsrv')->table('TIGPatient')
                ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'TIGPatient.PeriodId')
                ->select('KART_DONEM.DonemAdi', DB::raw('COUNT(TIGPatient.Id) AS Vaka'), DB::raw('COUNT(DISTINCT TIGPatient.PatientNo) AS TekilVaka'))
                ->where('TIGPatient.PeriodId', '<=', $request->to)
                ->where('TIGPatient.PeriodId', '>=', $request->from)
                ->groupBy('TIGPatient.PeriodId', 'KART_DONEM.DonemAdi')
                ->orderBy('TIGPatient.PeriodId', 'asc')
                ->get();

            return $result;
        });



        return response()->json($data, 200, [], JSON_NUMERIC_CHECK);

    }

    /**
     * @return View
     */

    public function turkiyeGeneliDonemselKlinikKodSayilari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_klinik_kod_verileri');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function turkiyeGeneliDonemselKlinikKodSayilariniGetir(Request $request)
    {
        ini_set('memory_limit', '1G');

        $this->validate($request, [
            'to' => 'required',
            'from' => 'required'
        ]);

        $cacheStr = 'turkiye_geneli_klinik_kod_sayilari_' . $request->to . '_' . $request->from;

        $datas = Cache::remember($cacheStr, '10080', function () use ($request) {
            return DB::connection('sqlsrv')->table('TIGPatient')
                ->join('TIGPatientCode', 'TIGPatient.Id', '=', 'TIGPatientCode.DRGPatientId')
                ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'TIGPatient.PeriodId')
                ->where('TIGPatient.PeriodId', '<=', $request->to)
                ->where('TIGPatient.PeriodId', '>=', $request->from)
                ->select(
                    'TIGPatient.PeriodId',
                    'KART_DONEM.DonemAdi',
                    'TIGPatientCode.Code',
                    'TIGPatientCode.CodeType',
                    DB::raw('COUNT(TIGPatientCode.Code) AS Sayi')
                )
                ->groupBy('TIGPatient.PeriodId', 'KART_DONEM.DonemAdi', 'TIGPatientCode.Code', 'TIGPatientCode.CodeType')
                ->orderBy('TIGPatient.PeriodId')
                ->get();
        });

        return response()->json($datas);

    }

    /**
     * @return View
     */
    public function donemselAnataniBasinaDusenEktaniSayilari()
    {
        return view('pages.tig.donemsel_anatani_basina_dusen_ektani');
    }

    public function klinikKodlar()
    {
        ini_set('memory_limit', '1G');

        $kod = Cache::remember('klinik_kodlar', '10080', function () {
            //return TigKod::where('TIP', 'I')->select(DB::raw('REPLACE(CONCAT(KOD2,\':\',TANIM),\'\'\'\',\'\') AS value'), 'KOD2')->take(1)->get();
            return TigKod::where('TIP', 'I')
                ->select(DB::raw('CONCAT(KOD2,\':\',TANIM) AS value'), 'KOD2')
                ->get();
        });

        return response()->json($kod);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function donemselAnataniBasinaDusenEktaniSayilariniHesapla(Request $request)
    {
        $this->validate($request, [
            'to' => 'required',
            'from' => 'required',
            'kod' => 'required'
        ]);

        $cacheStr = 'anatani_basina_ektani_' . $request->to . '_' . $request->from . '_' . $request->kod;

        $datas = Cache::remember($cacheStr, '10080', function () use ($request) {
            return DB::connection('sqlsrv')->table('kds.v_anatani_basina_dusen_ektani')
                ->where('Code', $request->kod)
                ->where('PeriodId', '<=', $request->to)
                ->where('PeriodId', '>=', $request->from)
                ->select('*')
                ->get();
        });

        return response()->json($datas);
    }

    /**
     * @return View
     * Dönemsel Hasta Başına İşlem Sayıları
     */

    public function donemselHastaBasinaDusenIslemSayilari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_islem_hasta_oranlari');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function donemselHastaBasinaDusenIslemSayilariniHesapla(Request $request)
    {
        if ($request->to && $request->from) {
            $cacheStr = 'donemsel_hastabasina_dusen_islem_hasta_oranlari' . $request->to . '_' . $request->from;
            $datas = Cache::remember($cacheStr, '10080', function () use ($request) {
                return DB::connection('sqlsrv')->table('kds.v_islem_hasta_orani AS tbl')
                    ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.DonemKodu')
                    ->select('tbl.*', 'KART_DONEM.DonemAdi', 'KART_DONEM.DonemYili')
                    ->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from)
                    ->orderBy('tbl.DonemKodu', 'asc')
                    ->get();

            });

            return response()->json($datas);
        }

        $maxPeriod = $this->maxPeriod();

        $datas = Cache::remember('donemsel_hastabasina_dusen_islem_hasta_oranlari_' . $maxPeriod, '10080', function () use ($maxPeriod) {
            return DB::connection('sqlsrv')->table('kds.v_islem_hasta_orani AS tbl')
                ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.DonemKodu')
                ->select('tbl.*', 'KART_DONEM.DonemAdi', 'KART_DONEM.DonemYili')
                ->where('tbl.DonemKodu', '=', $maxPeriod)
                ->orderBy('tbl.DonemKodu', 'asc')
                ->get();

        });

        return response()->json($datas);


    }

    /**
     * @return View
     * Dönemsel Cinsiyet ve Yaş Frekansları
     */

    public function donemselCinsiyetVeYasFrekanslari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_cinsiyet_yas_frekanslari');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function donemselCinsiyetVeYasFrekanslariniHesapla(Request $request)
    {
        if ($request->to && $request->from) {
            $cacheStr = 'donemsel_cinsiyet_ve_yas_frekanslarini_hesapla_' . $request->to . '_' . $request->from;

            $data = Cache::remember($cacheStr, '10080', function () use ($request) {
                return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_cinsiyet_ve_yas_frekanslari')
                    ->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from)
                    ->orderBy('DonemKodu', 'asc')
                    ->get();
            });

            $data = $data->toArray();

            return response()->json(array_values($data));
        }

        $maxPeriod = $this->maxPeriod();

        $data = Cache::remember('donemsel_cinsiyet_ve_yas_frekanslarini_hesapla_' . $maxPeriod, '10080', function () use ($maxPeriod) {
            return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_cinsiyet_ve_yas_frekanslari')
                ->where('DonemKodu', '=', $maxPeriod)
                ->get();
        });

        return response()->json($data);
    }

    public function donemselCinsiyetVeYasFrekanslariniToplamHesapla(Request $request)
    {
        $data = $this->donemselCinsiyetVeYasFrekanslariniHesapla($request)->getData();

        $cacheStr = 'donemsel_cinsiyet_ve_yas_frekanslarini_toplam_hesapla_';
        $maxPeriod = $this->maxPeriod();

        if ($request->to && $request->from) {
            $cacheStr .= $request->to . '_' . $request->from;
        } else {
            $cacheStr .= $maxPeriod;
        }

        $cachedData = Cache::remember($cacheStr, '10080', function () use ($maxPeriod, $data) {
            $newArray = [
                'Kadin' => [
                    'y_0_4' => 0,
                    'y_5_9' => 0,
                    'y_10_14' => 0,
                    'y_15_19' => 0,
                    'y_20_24' => 0,
                    'y_25_29' => 0,
                    'y_30_34' => 0,
                    'y_35_39' => 0,
                    'y_40_44' => 0,
                    'y_45_49' => 0,
                    'y_50_54' => 0,
                    'y_55_59' => 0,
                    'y_60_64' => 0,
                    'y_65_69' => 0,
                    'y_70_74' => 0,
                    'y_75_79' => 0,
                    'y_80_84' => 0,
                    'y_85+' => 0
                ],
                'Erkek' => [
                    'y_0_4' => 0,
                    'y_5_9' => 0,
                    'y_10_14' => 0,
                    'y_15_19' => 0,
                    'y_20_24' => 0,
                    'y_25_29' => 0,
                    'y_30_34' => 0,
                    'y_35_39' => 0,
                    'y_40_44' => 0,
                    'y_45_49' => 0,
                    'y_50_54' => 0,
                    'y_55_59' => 0,
                    'y_60_64' => 0,
                    'y_65_69' => 0,
                    'y_70_74' => 0,
                    'y_75_79' => 0,
                    'y_80_84' => 0,
                    'y_85+' => 0
                ]
            ];
            foreach ($data as $d) {

                if ($d->Cinsiyet === 'Kadın') {
                    $newArray['Kadin']['y_0_4'] += intval($d->y_0_4) ?? 0;
                    $newArray['Kadin']['y_5_9'] += intval($d->y_5_9) ?? 0;
                    $newArray['Kadin']['y_10_14'] += intval($d->y_10_14) ?? 0;
                    $newArray['Kadin']['y_15_19'] += intval($d->y_15_19) ?? 0;
                    $newArray['Kadin']['y_20_24'] += intval($d->y_20_24) ?? 0;
                    $newArray['Kadin']['y_25_29'] += intval($d->y_25_29) ?? 0;
                    $newArray['Kadin']['y_30_34'] += intval($d->y_30_34) ?? 0;
                    $newArray['Kadin']['y_35_39'] += intval($d->y_35_39) ?? 0;
                    $newArray['Kadin']['y_40_44'] += intval($d->y_40_44) ?? 0;
                    $newArray['Kadin']['y_45_49'] += intval($d->y_45_49) ?? 0;
                    $newArray['Kadin']['y_50_54'] += intval($d->y_50_54) ?? 0;
                    $newArray['Kadin']['y_55_59'] += intval($d->y_55_59) ?? 0;
                    $newArray['Kadin']['y_60_64'] += intval($d->y_60_64) ?? 0;
                    $newArray['Kadin']['y_65_69'] += intval($d->y_65_69) ?? 0;
                    $newArray['Kadin']['y_70_74'] += intval($d->y_70_74) ?? 0;
                    $newArray['Kadin']['y_75_79'] += intval($d->y_75_79) ?? 0;
                    $newArray['Kadin']['y_80_84'] += intval($d->y_80_84) ?? 0;
                    $newArray['Kadin']['y_85+'] += intval($d->y_85_ve_uzeri ?? 0);
                } elseif ($d->Cinsiyet === 'Erkek') {
                    $newArray['Erkek']['y_0_4'] += -1 * abs(intval($d->y_0_4) ?? 0);
                    $newArray['Erkek']['y_5_9'] += -1 * abs(intval($d->y_5_9) ?? 0);
                    $newArray['Erkek']['y_10_14'] += -1 * abs(intval($d->y_10_14) ?? 0);
                    $newArray['Erkek']['y_15_19'] += -1 * abs(intval($d->y_15_19) ?? 0);
                    $newArray['Erkek']['y_20_24'] += -1 * abs(intval($d->y_20_24) ?? 0);
                    $newArray['Erkek']['y_25_29'] += -1 * abs(intval($d->y_25_29) ?? 0);
                    $newArray['Erkek']['y_30_34'] += -1 * abs(intval($d->y_30_34) ?? 0);
                    $newArray['Erkek']['y_35_39'] += -1 * abs(intval($d->y_35_39) ?? 0);
                    $newArray['Erkek']['y_40_44'] += -1 * abs(intval($d->y_40_44) ?? 0);
                    $newArray['Erkek']['y_45_49'] += -1 * abs(intval($d->y_45_49) ?? 0);
                    $newArray['Erkek']['y_50_54'] += -1 * abs(intval($d->y_50_54) ?? 0);
                    $newArray['Erkek']['y_55_59'] += -1 * abs(intval($d->y_55_59) ?? 0);
                    $newArray['Erkek']['y_60_64'] += -1 * abs(intval($d->y_60_64) ?? 0);
                    $newArray['Erkek']['y_65_69'] += -1 * abs(intval($d->y_65_69) ?? 0);
                    $newArray['Erkek']['y_70_74'] += -1 * abs(intval($d->y_70_74) ?? 0);
                    $newArray['Erkek']['y_75_79'] += -1 * abs(intval($d->y_75_79) ?? 0);
                    $newArray['Erkek']['y_80_84'] += -1 * abs(intval($d->y_80_84) ?? 0);
                    $newArray['Erkek']['y_85+'] += -1 * abs(intval($d->y_85_ve_uzeri ?? 0));
                }

            }
            $dataArr = array();
            if (count($data) > 0) {

                $dataArr = [
                    ['age' => '0_4', 'female' => intval($newArray['Kadin']['y_0_4']), 'male' => intval($newArray['Erkek']['y_0_4'])],
                    ['age' => '5_9', 'female' => intval($newArray['Kadin']['y_5_9']), 'male' => intval($newArray['Erkek']['y_5_9'])],
                    ['age' => '10_14', 'female' => intval($newArray['Kadin']['y_10_14']), 'male' => intval($newArray['Erkek']['y_10_14'])],
                    ['age' => '15_19', 'female' => intval($newArray['Kadin']['y_15_19']), 'male' => intval($newArray['Erkek']['y_15_19'])],
                    ['age' => '20_24', 'female' => intval($newArray['Kadin']['y_20_24']), 'male' => intval($newArray['Erkek']['y_20_24'])],
                    ['age' => '25_29', 'female' => intval($newArray['Kadin']['y_25_29']), 'male' => intval($newArray['Erkek']['y_25_29'])],
                    ['age' => '30_34', 'female' => intval($newArray['Kadin']['y_30_34']), 'male' => intval($newArray['Erkek']['y_30_34'])],
                    ['age' => '35_39', 'female' => intval($newArray['Kadin']['y_35_39']), 'male' => intval($newArray['Erkek']['y_35_39'])],
                    ['age' => '40_44', 'female' => intval($newArray['Kadin']['y_40_44']), 'male' => intval($newArray['Erkek']['y_40_44'])],
                    ['age' => '45_49', 'female' => intval($newArray['Kadin']['y_45_49']), 'male' => intval($newArray['Erkek']['y_45_49'])],
                    ['age' => '50_54', 'female' => intval($newArray['Kadin']['y_50_54']), 'male' => intval($newArray['Erkek']['y_50_54'])],
                    ['age' => '55_59', 'female' => intval($newArray['Kadin']['y_55_59']), 'male' => intval($newArray['Erkek']['y_55_59'])],
                    ['age' => '60_64', 'female' => intval($newArray['Kadin']['y_60_64']), 'male' => intval($newArray['Erkek']['y_60_64'])],
                    ['age' => '65_69', 'female' => intval($newArray['Kadin']['y_65_69']), 'male' => intval($newArray['Erkek']['y_65_69'])],
                    ['age' => '70_74', 'female' => intval($newArray['Kadin']['y_70_74']), 'male' => intval($newArray['Erkek']['y_70_74'])],
                    ['age' => '75_79', 'female' => intval($newArray['Kadin']['y_75_79']), 'male' => intval($newArray['Erkek']['y_75_79'])],
                    ['age' => '80_84', 'female' => intval($newArray['Kadin']['y_80_84']), 'male' => intval($newArray['Erkek']['y_80_84'])],
                    ['age' => '85+', 'female' => intval($newArray['Kadin']['y_85+']), 'male' => intval($newArray['Erkek']['y_85+'])],

                ];
            } else {

                $dataArr = [
                    ['age' => '0', 'female' => 0, 'male' => 0],
                ];
            }

            return $dataArr;
        });
        return response()->json($cachedData);
    }

    /**
     * @return View
     * Tig Bazlı Dönemsel Cinsiyet ve Yaş Frekansları
     */
    public function donemselTigBazliCinsiyetVeYasFrekanslari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslari');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function donemselTigBazliCinsiyetVeYasFrekanslariniHesapla(Request $request)
    {
        if ($request->to && $request->from) {
            $cacheStr = 'donemselTigBazliCinsiyetVeYasFrekanslariniHesapla_cache_' . $request->to . '_' . $request->from;
            $data = Cache::remember($cacheStr, '10080', function () use ($request) {
                return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_tig_bazli_cinsiyet_ve_yas_frekanslari')
                    ->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from)
                    ->select('*')
                    ->orderBy('DonemKodu')
                    ->get();
            });

            return response()->json($data);
        }

        $maxPeriod = $this->maxPeriod();

        $cacheStr = 'donemselTigBazliCinsiyetVeYasFrekanslariniHesapla_cache_' . $maxPeriod;
        $data = Cache::remember($cacheStr, '10080', function () use ($maxPeriod) {
            return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_tig_bazli_cinsiyet_ve_yas_frekanslari')
                ->where('DonemKodu', $maxPeriod)
                ->select('*')
                ->orderBy('DonemKodu')
                ->get();
        });

        return response()->json($data);
    }

    public function donemselTigBazliCinsiyetVeYasFrekanslariniToplamHesapla(Request $request)
    {
        if ($request->to && $request->from) {
            $cacheStr = 'donemselTigBazliCinsiyetVeYasFrekanslariniToplamHesapla_cache' . $request->to . '_' . $request->from;
            $data = Cache::remember($cacheStr, '10080', function () use ($request) {
                return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_tig_bazli_cinsiyet_ve_yas_frekanslari AS tbl')
                    ->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from)
                    ->select(
                        'tbl.Cinsiyet',
                        'tbl.Drg',
                        'tbl.DonemKodu',
                        DB::raw('(SUM(tbl.y_0_4) +SUM(tbl.y_5_9) +SUM(tbl.y_10_14) +SUM(tbl.y_15_19) +SUM(tbl.y_20_24) +SUM(tbl.y_25_29) +
                              SUM(tbl.y_30_34) +SUM(tbl.y_35_39) +SUM(tbl.y_40_44) +SUM(tbl.y_45_49) +SUM(tbl.y_50_54) +SUM(tbl.y_55_59) +
                              SUM(tbl.y_60_64) +SUM(tbl.y_65_69)+SUM(tbl.y_70_74) +SUM(tbl.y_75_79) +SUM(tbl.y_80_84) +SUM(tbl.y_85_)) AS Say')
                    )
                    ->groupBy('tbl.Cinsiyet', 'tbl.Drg', 'tbl.DonemKodu')
                    ->orderBy('tbl.DonemKodu', 'asc')
                    ->get();
            });
        }

        $maxPeriod = $this->maxPeriod();
        $cacheStr = 'donemselTigBazliCinsiyetVeYasFrekanslariniToplamHesapla_cache_' . $maxPeriod;

        $data = Cache::remember($cacheStr, '10080', function () use ($maxPeriod) {
            return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_tig_bazli_cinsiyet_ve_yas_frekanslari AS tbl')
                ->where('DonemKodu', $maxPeriod)
                ->select(
                    'tbl.Cinsiyet',
                    'tbl.Drg',
                    'tbl.DonemKodu',
                    DB::raw('(SUM(tbl.y_0_4) +SUM(tbl.y_5_9) +SUM(tbl.y_10_14) +SUM(tbl.y_15_19) +SUM(tbl.y_20_24) +SUM(tbl.y_25_29) +
                              SUM(tbl.y_30_34) +SUM(tbl.y_35_39) +SUM(tbl.y_40_44) +SUM(tbl.y_45_49) +SUM(tbl.y_50_54) +SUM(tbl.y_55_59) +
                              SUM(tbl.y_60_64) +SUM(tbl.y_65_69)+SUM(tbl.y_70_74) +SUM(tbl.y_75_79) +SUM(tbl.y_80_84) +SUM(tbl.y_85_)) AS Say')
                )
                ->groupBy('tbl.Cinsiyet', 'tbl.Drg', 'tbl.DonemKodu')
                ->orderBy('tbl.DonemKodu', 'asc')
                ->get();
        });

        $newArray = array();

        // Drg bazli array oluştur

        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]->Cinsiyet == 'Kadın') {
                $newArray[$data[$i]->Drg]['Drg'] = $data[$i]->Drg;
                $newArray[$data[$i]->Drg]['Kadin'] = intval($data[$i]->Say);
            } else {
                $newArray[$data[$i]->Drg]['Drg'] = $data[$i]->Drg;
                $newArray[$data[$i]->Drg]['Erkek'] = intval($data[$i]->Say);
            }
            $newArray[$data[$i]->Drg]['Toplam'] = 0;
        }

        // Cinsiyette gelmeyen sayı varsa default 0 ata;

        foreach ($newArray as $key => $value) {
            $newArray[$key]['Erkek'] = $newArray[$key]['Erkek'] ?? 0;
            $newArray[$key]['Kadin'] = $newArray[$key]['Kadin'] ?? 0;
        }

        //Toplamı Getir

        foreach ($newArray as $key => $value) {
            $newArray[$key]['Toplam'] += $newArray[$key]['Erkek'];
            $newArray[$key]['Toplam'] += $newArray[$key]['Kadin'];
        }

        //Drg Keyli olan arrayi normal index arraya dönüştür.    
        $dataArray = array();
        $i = 0;

        foreach ($newArray as $key => $value) {
            $dataArray[$i] = $newArray[$key];
            $i++;
        }

        return response()->json($dataArray);
    }

    /**
     * Dönemsel Türkiye Geneli Yaş ve Cinsiyete Göre Tanı Sayıları(Ana Tanı ve Ek Tanı)
     */

    public function donemselTurkiyeGeneliYasveCinsiyetBazliTaniSayilari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilari');
    }

    public function donemselTurkiyeGeneliYasveCinsiyetBazliTaniSayilariniHesapla(Request $request)
    {
        ini_set('memory_limit', '1G');

        $cacheStr = 'turkiye_geneli_yas_ve_cinsiyet_bazli_tani_sayilari';

        if ($request->to && $request->from) {
            $cacheStr .= $request->to . '_' . $request->from;
            $data = Cache::remember($cacheStr, '10080', function () use ($request) {
                return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_yas_ve_cinsiyet_bazli_tani_sayilari AS tbl')
                    ->join('KART_DONEM AS donem', 'donem.Id', '=', 'tbl.DonemKodu')
                    ->join('KODLAR AS kod', 'kod.KOD2', '=', 'tbl.TaniKod')
                    ->where('tbl.DonemKodu', '<=', $request->to)
                    ->where('tbl.DonemKodu', '>=', $request->from)
                    ->select('tbl.*', 'donem.DonemAdi', 'donem.DonemYili', 'kod.TANIM')
                    ->orderBy('tbl.DonemKodu', 'asc')
                    ->get();
            });
        }

        $maxPeriod = $this->maxPeriod();

        $cacheStr .= $maxPeriod;

        $data = Cache::remember($cacheStr, '10080', function () use ($maxPeriod) {
            return DB::connection('sqlsrv')->table('kds.v_turkiye_geneli_yas_ve_cinsiyet_bazli_tani_sayilari AS tbl')
                ->join('KART_DONEM AS donem', 'donem.Id', '=', 'tbl.DonemKodu')
                ->join('KODLAR AS kod', 'kod.KOD2', '=', 'tbl.TaniKod')
                ->where('tbl.DonemKodu', $maxPeriod)
                ->select('tbl.*', 'donem.DonemAdi', 'donem.DonemYili', 'kod.TANIM')
                ->get();
        });


        return response()->json($data, 200, [], JSON_NUMERIC_CHECK);

    }




    /**
     * Dönemsel Günübirlik ve Diğer yatışlardaki vaka frekansları
     *
     * @return View
     * @author 
     **/
    public function donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslari()
    {
        return view('pages.tig.donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari');
    }


    public function donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslariHesapla(Request $request)
    {
        ini_set('memory_limit', '1G');

        $data = Cache::remember('donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslariHesapla_cache', '10080', function () {
            return DB::table('TurkiyeGeneliDonemselGunubirlikVeDigerVakaFrekanslari AS tbl')
                ->join('Hospitals AS h', 'h.HospitalCode', '=', 'tbl.HospitalCode')
                ->join('KART_TIG AS t', 't.TIG', '=', 'tbl.Drg')
                ->join('KART_DONEM AS donem', 'donem.DonemKodu', '=', 'tbl.DonemKodu')
                ->select('tbl.*', 'h.HospitalName', 'donem.DonemAdi', 'donem.DonemYili', 't.Tanim')
                ->orderBy('tbl.DonemKodu', 'asc')
                ->get();
        });

        if (isset($request->from) && isset($request->to)) {
            $data = $data->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
        } else {
            $maxYear = DB::table('KART_DONEM')->max('KART_DONEM.DonemYili');
            $data = $data->where('DonemYili', $maxYear);
        }

        return response()->json($data);
    }

    public function donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslariToplamHesapla(Request $request)
    {
        $data = Cache::remember('donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslariToplamHesapla_cache', '10080', function () {
            return DB::table('TurkiyeGeneliDonemselGunubirlikVeDigerVakaFrekanslari AS tbl')
                ->join('KART_DONEM AS donem', 'donem.DonemKodu', '=', 'tbl.DonemKodu')
                ->select(
                    'tbl.DonemKodu',
                    'donem.DonemYili',
                    'tbl.Drg',
                    DB::raw('SUM(tbl.GunubirlikVakaFrekans) AS GunubirlikVakaFrekans'),
                    DB::raw('SUM(tbl.DigerVakaFrekans) AS DigerVakaFrekans'),
                    DB::raw('SUM(tbl.GunubirlikVakaFrekans)+SUM(tbl.DigerVakaFrekans) AS Say')
                )
                ->orderBy('tbl.DonemKodu', 'asc')
                ->groupBy('tbl.DonemKodu', 'donem.DonemYili', 'tbl.Drg')
                ->get();
        });

        if (isset($request->from) && isset($request->to)) {
            $data = $data->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
        } else {
            $maxYear = DB::table('KART_DONEM')->max('KART_DONEM.DonemYili');
            $data = $data->where('DonemYili', $maxYear);
        }

        return response()->json($data);
    }


    /**
     * Kurum Bazli Klinik Kodlayici Sayıları
     *
     * @return View
     * @author 
     **/
    public function kurumBazliKlinikKodlayiciSayilari()
    {
        if ($this->user->hasRole('administrator') || $this->user->hasRole('owner')) {
            $this->kurum_bazli_klinik_kodlayici_sayilari = Cache::remember('kurumBazliKlinikKodlayiciSayilari_cache', '10080', function () {
                return DB::table('TIGUsers')
                    ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'TIGUsers.HospId')
                    ->join('Cities', 'Cities.CityCode', '=', 'Hospitals.HospitalCity')
                    ->where('TIGUsers.Rol', 1)
                    ->select(
                        'TIGUsers.HospId AS HastaneKodu',
                        'Hospitals.HospitalName AS HastaneAdi',
                        'Cities.CityName AS Il',
                        DB::raw('SUM(CASE WHEN TIGUsers.Active="1" THEN 1 ELSE 0 END) AS Aktif'),
                        DB::raw('SUM(CASE WHEN TIGUsers.Active="0" THEN 1 ELSE 0 END) AS Pasif')
                    )
                    ->groupBy('TIGUsers.HospId', 'Hospitals.HospitalName', 'Cities.CityName')
                    ->get();
            });
        } elseif ($this->user->hasRole('hsmember')) {
            $this->kurum_bazli_klinik_kodlayici_sayilari = DB::table('TIGUsers')
                ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'TIGUsers.HospId')
                ->join('Cities', 'Cities.CityCode', '=', 'Hospitals.HospitalCity')
                ->where('TIGUsers.Rol', 1)
                ->where('Hospitals.HospitalCode', $this->user->hospital_code)
                ->select(
                    'TIGUsers.HospId AS HastaneKodu',
                    'Hospitals.HospitalName AS HastaneAdi',
                    'Cities.CityName AS Il',
                    DB::raw('SUM(CASE WHEN TIGUsers.Active="1" THEN 1 ELSE 0 END) AS Aktif'),
                    DB::raw('SUM(CASE WHEN TIGUsers.Active="0" THEN 1 ELSE 0 END) AS Pasif')
                )
                ->groupBy('TIGUsers.HospId', 'Hospitals.HospitalName', 'Cities.CityName')
                ->get();
        } elseif ($this->user->hasRole('gsmember')) {
            $this->kurum_bazli_klinik_kodlayici_sayilari = DB::table('TIGUsers')
                ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'TIGUsers.HospId')
                ->join('HospitalsKhb', 'HospitalsKhb.KurumKod', '=', 'Hospitals.Khb')
                ->join('Cities', 'Cities.CityCode', '=', 'Hospitals.HospitalCity')
                ->where('TIGUsers.Rol', 1)
                ->where('HospitalsKhb.KurumKod', $this->user->hospital_code)
                ->select(
                    'TIGUsers.HospId AS HastaneKodu',
                    'Hospitals.HospitalName AS HastaneAdi',
                    'Cities.CityName AS Il',
                    DB::raw('SUM(CASE WHEN TIGUsers.Active="1" THEN 1 ELSE 0 END) AS Aktif'),
                    DB::raw('SUM(CASE WHEN TIGUsers.Active="0" THEN 1 ELSE 0 END) AS Pasif')
                )
                ->groupBy('TIGUsers.HospId', 'Hospitals.HospitalName', 'Cities.CityName')
                ->get();
        }

        return view('pages.tig.kurum_bazli_klinik_kodlayici_sayilari', ['datas' => $this->kurum_bazli_klinik_kodlayici_sayilari]);
    }

    /**
     * Doğum Verileri
     * @return view
     */
    public function dogumFrekansVerileri(Request $request)
    {
        $cacheStr = 'turkiye_geneli_donemsel_dogum_sayilari';

        if ($request->to && $request->from) {
            $cacheStr .= $request->to . '_' . $request->from;
            $data = Cache::remember($cacheStr, '10080', function () use ($request) {
                return DB::connection('sqlsrv')->table('kds.v_donemsel_dogum_sayilari AS tbl')
                    ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.DonemKodu')
                    ->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from)
                    ->select(
                        'tbl.DonemKodu',
                        'KART_DONEM.DonemYili',
                        DB::raw('SUM(tbl.NormalDogum) AS ndogum'),
                        DB::raw('SUM(tbl.MudehaleliDogum) AS mdogum'),
                        DB::raw('SUM(tbl.SezeryanDogum) AS sdogum'),
                        DB::raw('SUM(tbl.PrimerSezaryen) AS pdogum')
                    )
                    ->groupBy('tbl.DonemKodu', 'KART_DONEM.DonemYili')
                    ->orderBy('tbl.DonemKodu', 'asc')
                    ->get();
            });
        }

        $maxPeriod = $this->maxPeriod();

        $cacheStr .= $maxPeriod;

        $data = Cache::remember($cacheStr, '10080', function () use ($maxPeriod) {
            return DB::connection('sqlsrv')->table('kds.v_donemsel_dogum_sayilari AS tbl')
                ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.DonemKodu')
                ->where('DonemKodu', '=', $maxPeriod)
                ->select(
                    'tbl.DonemKodu',
                    'KART_DONEM.DonemYili',
                    DB::raw('SUM(tbl.NormalDogum) AS ndogum'),
                    DB::raw('SUM(tbl.MudehaleliDogum) AS mdogum'),
                    DB::raw('SUM(tbl.SezeryanDogum) AS sdogum'),
                    DB::raw('SUM(tbl.PrimerSezaryen) AS pdogum')
                )
                ->groupBy('tbl.DonemKodu', 'KART_DONEM.DonemYili')
                ->orderBy('tbl.DonemKodu', 'asc')
                ->get();
        });

        return view('pages.dogum.dogum_frekans_verileri', ['totalDogum' => $data]);
    }

    /**
     * Doğum Table Verileri
     */


    public function dogumVerileri(Request $request)
    {

        $cacheStr = 'turkiye_geneli_donemsel_dogum_sayilari_2';

        $maxPeriod = $this->maxPeriod();

        $data = DB::connection('sqlsrv')->table('kds.v_donemsel_dogum_sayilari AS tbl')
            ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.DonemKodu')
            ->select('tbl.*', 'KART_DONEM.DonemYili');

        if ($request->from && $request->to) {
            if (isset($request->city)) {
                $data = $data->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from)
                    ->where('CityCode', $request->city);
                $cacheStr .= $request->city . '_' . $request->to . '_' . $request->from;
            } else {
                $data = $data->where('DonemKodu', '<=', $request->to)
                    ->where('DonemKodu', '>=', $request->from);
                $cacheStr .= $request->to . '_' . $request->from;
            }
        } else {
            if (isset($request->city)) {
                $data = $data->where('DonemKodu', $maxPeriod)
                    ->where('CityCode', $request->city);
                $cacheStr .= $request->city . '_' . $maxPeriod;
            } else {
                $data = $data->where('DonemKodu', $maxPeriod);
            }
        }

        $response = Cache::remember($cacheStr, '10080', function () use ($data) {
            return $data->get();
        });

        return response()->json($response);
    }

    /**
     * Doğum Verileri Hesapla
     * @return json
     */
    public function dogumFrekansVerileriniKurumTuruBazindaHesapla(Request $request)
    {

        $cacheStr = 'turkiye_geneli_donemsel_dogum_sayilari_3';

        $maxPeriod = $this->maxPeriod();

        $data = DB::connection('sqlsrv')->table('kds.v_donemsel_dogum_sayilari AS tbl')
            ->select(
                'tbl.Statu AS statu',
                DB::raw('SUM(tbl.NormalDogum) AS ndogum'),
                DB::raw('SUM(tbl.MudehaleliDogum) AS mdogum'),
                DB::raw('SUM(tbl.SezeryanDogum) AS sdogum'),
                DB::raw('SUM(tbl.PrimerSezaryen) AS pdogum')
            );

        if ($request->from && $request->to) {
            if (isset($request->city)) {
                $data = $data->whereBetween('tbl.DonemKodu', [$request->from, $request->to])
                    ->where('tbl.CityCode', $request->city)
                    ->groupBy('tbl.Statu');

                $cacheStr .= $request->city . '_' . $request->to . '_' . $request->from;

            } else {
                $data = $data->whereBetween('tbl.DonemKodu', [$request->from, $request->to])->groupBy('tbl.Statu');
                $cacheStr .= $request->to . '_' . $request->from;
            }
        } else {
            if (isset($request->city)) {
                $data = $data->where('tbl.DonemKodu', $maxPeriod)
                    ->where('tbl.CityCode', $request->city)
                    ->groupBy('tbl.Statu');
                $cacheStr .= $request->city . '_' . $maxPeriod;
            } else {
                $data = $data->where('tbl.DonemKodu', $maxPeriod)
                    ->groupBy('tbl.Statu');
                $cacheStr .= $maxPeriod;
            }
        }

        $response = Cache::remember($cacheStr, '10080', function () use ($data) {
            return $data->get();
        });

        $dataArray = $response->toArray();

        $newArray = [
            'normal_dgm' => [
                'dogum_tur' => 'Normal Doğum',
                'devlet' => 0,
                'ozel' => 0,
                'universite' => 0,
                'toplam' => 0
            ],
            'sezaryen_dgm' => [
                'dogum_tur' => 'Sezaryan Doğum',
                'devlet' => 0,
                'ozel' => 0,
                'universite' => 0,
                'toplam' => 0
            ],
            'psezaryen_dgm' => [
                'dogum_tur' => 'Primer Sezaryan Doğum',
                'devlet' => 0,
                'ozel' => 0,
                'universite' => 0,
                'toplam' => 0
            ],
            'mudehaleli_dgm' => [
                'dogum_tur' => 'Müdehaleli Doğum',
                'devlet' => 0,
                'ozel' => 0,
                'universite' => 0,
                'toplam' => 0
            ],

        ];

        $toplamNormalDgm = 0;
        $toplamSDgm = 0;
        $toplamMDgm = 0;
        $toplamPDgm = 0;

        foreach ($dataArray as $data) {

            $toplamNormalDgm += intval($data->ndogum ?? 0);
            $toplamSDgm += intval($data->sdogum ?? 0);
            $toplamMDgm += intval($data->mdogum ?? 0);
            $toplamPDgm += intval($data->pdogum ?? 0);

            switch ($data->statu) {
                case 'Devlet':
                    $newArray['normal_dgm']['devlet'] = intval($data->ndogum);
                    $newArray['sezaryen_dgm']['devlet'] = intval($data->sdogum);
                    $newArray['psezaryen_dgm']['devlet'] = intval($data->pdogum);
                    $newArray['mudehaleli_dgm']['devlet'] = intval($data->mdogum);
                    break;

                case 'Özel':
                    $newArray['normal_dgm']['ozel'] = intval($data->ndogum);
                    $newArray['sezaryen_dgm']['ozel'] = intval($data->sdogum);
                    $newArray['psezaryen_dgm']['ozel'] = intval($data->pdogum);
                    $newArray['mudehaleli_dgm']['ozel'] = intval($data->mdogum);
                    break;

                case 'Üniversite':
                    $newArray['normal_dgm']['universite'] = intval($data->ndogum);
                    $newArray['sezaryen_dgm']['universite'] = intval($data->sdogum);
                    $newArray['psezaryen_dgm']['universite'] = intval($data->pdogum);
                    $newArray['mudehaleli_dgm']['universite'] = intval($data->mdogum);
                    break;
            }

        }

        $newArray['normal_dgm']['toplam'] = $toplamNormalDgm;
        $newArray['sezaryen_dgm']['toplam'] = $toplamSDgm;
        $newArray['psezaryen_dgm']['toplam'] = $toplamPDgm;
        $newArray['mudehaleli_dgm']['toplam'] = $toplamMDgm;

        $dataArr = [
            array_pull($newArray, 'normal_dgm'),
            array_pull($newArray, 'sezaryen_dgm'),
            array_pull($newArray, 'psezaryen_dgm'),
            array_pull($newArray, 'mudehaleli_dgm')
        ];


        return response()->json($dataArr);
    }


    /**
     * Doğum Verileri Hesapla
     * @return json
     */
    public function dogumFrekansVerileriniIlBazindaHesapla(Request $request)
    {
        $cacheStr = 'turkiye_geneli_donemsel_dogum_sayilari_4';

        $maxPeriod = $this->maxPeriod();

        $data = DB::connection('sqlsrv')->table('kds.v_donemsel_dogum_sayilari AS tbl')
            ->join('KART_DONEM', 'KART_DONEM.Id', '=', 'tbl.DonemKodu')
            ->select(
                'tbl.CityCode',
                'tbl.DonemKodu',
                'KART_DONEM.DonemYili',
                DB::raw('(CASE WHEN tbl.CityCode<10 THEN CONCAT(\'TR-0\',tbl.CityCode) ELSE CONCAT(\'TR-0\',tbl.CityCode) END) AS id'),
                DB::raw('SUM(tbl.NormalDogum) AS ndogum'),
                DB::raw('SUM(tbl.MudehaleliDogum) AS mdogum'),
                DB::raw('SUM(tbl.SezeryanDogum) AS sdogum'),
                DB::raw('SUM(tbl.PrimerSezaryen) AS pdogum'),
                DB::raw('(SUM(tbl.NormalDogum)+SUM(tbl.MudehaleliDogum)+SUM(tbl.SezeryanDogum)+SUM(tbl.PrimerSezaryen)) AS value')
            );

        if ($request->from && $request->to) {
            $data = $data->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
            $cacheStr .= $request->to . '_' . $request->from;

        } else {
            $data = $data->where('tbl.DonemKodu', $maxPeriod);
            $cacheStr .= $maxPeriod;
        }

        $response = Cache::remember($cacheStr, '10080', function () use ($data) {
            return $data->groupBy('tbl.CityCode', 'tbl.DonemKodu', 'KART_DONEM.DonemYili')
                ->orderBy('tbl.DonemKodu')
                ->get();
        });

        $mapData = ["map" => "turkeyLow", "areas" => $response];

        return response()->json($mapData, 200, [], JSON_NUMERIC_CHECK);
    }


    /**
     * Normal Doğum Ana Tanı ve Ek Tanı Sayıları
     * @return view
     */

    public function normalDgmTaniVerileri()
    {
        return view('pages.dogum.normal_sezaryen_dogum_anatani_ektani_verileri');
    }

    public function normalDgmTaniVerileriniHesapla(Request $request)
    {
        // $data = Cache::remember('normalDgmTaniVerileriniHesapla_cache','10080', function(){
        //     return DB::table('KurumBazliNormalVeSezaryenDogumAnaTaniveEkTaniSayilari AS tbl')
        //                 ->join('KART_DONEM AS donem', 'donem.DonemKodu', '=', 'tbl.DonemKodu')
        //                 ->join('Hospitals AS h', 'h.HospitalCode', '=', 'tbl.HospitalCode')
        //                 ->join('Cities AS c','.CityCode','=','h.HospitalCity')
        //                 ->join('KODLAR AS kod','kod.KOD2','=','tbl.Code')
        //                 ->select('tbl.*','donem.DonemAdi','donem.DonemYili','h.HospitalName', 'c.CityCode','kod.TANIM',
        //                   DB::raw('(CASE WHEN c.CityCode<10 THEN CONCAT("TR-0",c.CityCode) ELSE CONCAT("TR-",c.CityCode) END) AS id'),
        //                   'c.CityName')
        //                 ->get();
        //             });

        $cacheDogumStr = 'normal_dogum_verileri';
        $cacheSezaryenStr = 'normal_dogum_verileri';

        $ndogum = DB::connection('sqlsrv')->table('kds.v_normal_dogum_verileri')->select('*');
        $sdogum = DB::connection('sqlsrv')->table('kds.v_sezaryen_dogum_verileri')->select('*');

        if (isset($request->from) && isset($request->to)) {
            $ndogum = $ndogum->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
            $sdogum = $sdogum->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
            $cacheDogumStr .= $request->to . '_' . $request->from;
            $cacheSezaryenStr .= $request->to . '_' . $request->from;
        } else {
            $ndogum = $ndogum->where('DonemKodu', '=', $this->maxPeriod());
            $sdogum = $sdogum->where('DonemKodu', '=', $this->maxPeriod());
            $cacheDogumStr .= $this->maxPeriod();
            $cacheSezaryenStr .= $this->maxPeriod();
        }

        $data = $ndogum->union($sdogum);

        if (isset($request->from) && isset($request->to)) {
            $data = $data->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
        } else {
            $data = $data->where('DonemKodu', $this->maxPeriod());
        }

        $responseData = $data->get();

        return response()->json($responseData, 200, [], JSON_NUMERIC_CHECK);



    }

    /**
     * Yenidoğan Kilo Verileri
     * @return view
     */
    public function yenidoganKiloVerileri()
    {
        return view('pages.dogum.yd_kilo_verileri');
    }

    /**
     * Yenidoğan Kilo Verilerini Kurum Bazında Getirir
     * @return json
     */

    public function yenidoganKiloVerileriniKurumBazindaHesapla(Request $request)
    {
        ini_set('memory_limit', '-1');

        $data = DB::connection('sqlsrv')->table('kds.v_yd_kilo_verileri');

        if (isset($request->from) && isset($request->to)) {
            $data = $data->where('DonemKodu', '<=', $request->to)
                ->where('DonemKodu', '>=', $request->from);
        } else {
            $data = $data->where('DonemKodu', $this->maxPeriod());
        }

        $data = $data->get();

        $data = $data->toArray();

        return response()->json(array_values($data), 200, [], JSON_NUMERIC_CHECK);
    }

    public function oecdHastaneBazliKlinikVeri()
    {
        $hospitals = $this->hospital_list->get();
        return view('pages.tig.oecd_hastane_bazli_klinik_veri', compact('hospitals'));
    }

    public function oecdHastaneBazliKlinikVerileriGetir(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);


        $this->validate($request, [
            'to' => 'required',
            'from' => 'required',
            'hospital' => 'required'
        ]);

        $cacheStr = 'oecd_donemsel_hastane_bazli_klinik_verileri_';

        $maxPeriod = $this->maxPeriod();

        $data = DB::connection('sqlsrv')
            ->table('kds.v_hastane_bazli_klinik_vaka_verileri')
            ->groupBy(
                'DONEM_KODU',
                'HASTANE_KODU',
                'HASTANE_ADI',
                'YATIS_NO',
                'HASTA_YAS',
                'CINSIYET',
                'BRANS',
                'YATIS_SEKLI',
                'CIKIS_SEKLI',
                'YATIS_GUNU',
                'YB_YATIS_GUNU',
                'DRG_KODU',
                'DRG_KODU_ACIKLAMA',
                'DRG_BAGIL',
                'TANI_KODU',
                'TANI_KODU_ACIKLAMA',
                'TANI_KODU_SEKLI',
                'TANI_KODU_TIPI',
                'ISLEM_ZAMANI',
                'DONEM',
                'YIL',
                'YATIS_TARIHI',
                'CIKIS_TARIHI',
                'KODLAYICI'
            )
            ->select('*');

        //Role göre datayı getir.

        if ($this->user->hasRole('hsmember')) {
            $data = $data->where('HASTANE_KODU', $this->user->hospital_code);
        } elseif ($this->user->hasRole('gsmember')) {
            $hospitals = DB::table('Hospitals')->where('Khb', $this->user->hospital_code)->get();
            $hospArray = [];

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
            $data = $data->whereIn('HASTANE_KODU', $hospArray);
        }


        if ($request->from && $request->to) {
            $data = $data->where('DONEM_KODU', '<=', $request->to)
                ->where('DONEM_KODU', '>=', $request->from);
            $cacheStr .= $request->to . '_' . $request->from;
        } else {
            $data = $data->where('DONEM_KODU', $maxPeriod);
            $cacheStr .= $maxPeriod;
        }

        //Start Pagination

        if ($request->hospital) {
            $data = $data->where('HASTANE_KODU', $request->hospital);
            $cacheStr .= $request->hospital;
        }

        if ($request->filter) {
            $filteredDataObject = json_decode($request->filter);
            $filterData = array();
            foreach ($filteredDataObject as $key => $value) {
                $filterData[$key] = $value;
            }
            $data = $data->where($filterData);
        }

        if ($request->sort && $request->order) {
            $data = $data->orderBy($request->sort, $request->order);
        }

        $limit = $request->limit ?? 10;

        $page = $data->paginate($limit);

        $data = $data->offset($request->offset)->limit($request->limit);

        $responseData = $data->orderBy('DONEM_KODU')->get();

        Log::info($request->hospital . ' Kodlu Sağlık Tesisinden ' . $this->user->email . ' Epostasına sahip kullanıcı ' . $request->from . '-' . $request->to . ' dönemleri arasındaki tüm klinik verileri sorguladı.');

        $response = ['total' => $page->total(), 'rows' => $responseData];

        //End Pagination

        if ($request->export) {

            $exportData = array();

            $resData = Cache::remember($cacheStr, '10080', function () use ($data) {
                return $data->get();
            });

            foreach ($resData as $key => $res) {
                $exportData[$key]['DONEM'] = $res->DONEM;
                $exportData[$key]['HASTANE_KODU'] = $res->HASTANE_KODU;
                $exportData[$key]['HASTANE_ADI'] = $res->HASTANE_ADI;
                $exportData[$key]['YATIS_NO'] = $res->YATIS_NO;
                $exportData[$key]['HASTA_YAS'] = $res->HASTA_YAS;
                $exportData[$key]['CINSIYET'] = $res->CINSIYET;
                $exportData[$key]['BRANS'] = $res->BRANS;
                $exportData[$key]['CIKIS_SEKLI'] = $res->CIKIS_SEKLI;
                $exportData[$key]['YATIS_TARIHI'] = $res->YATIS_TARIHI;
                $exportData[$key]['CIKIS_TARIHI'] = $res->CIKIS_TARIHI;
                $exportData[$key]['YATIS_GUNU'] = $res->YATIS_GUNU;
                $exportData[$key]['YB_YATIS_GUNU'] = $res->YB_YATIS_GUNU;
                $exportData[$key]['DRG_KODU'] = $res->DRG_KODU;
                $exportData[$key]['DRG_KODU_ACIKLAMA'] = $res->DRG_KODU_ACIKLAMA;
                $exportData[$key]['DRG_BAGIL'] = $res->DRG_BAGIL;
                $exportData[$key]['TANI_KODU'] = $res->TANI_KODU;
                $exportData[$key]['TANI_KODU_ACIKLAMA'] = $res->TANI_KODU_ACIKLAMA;
                $exportData[$key]['TANI_KODU_SEKLI'] = $res->TANI_KODU_SEKLI;
                $exportData[$key]['TANI_KODU_TIPI'] = $res->TANI_KODU_TIPI;
                $exportData[$key]['ISLEM_ZAMANI'] = $res->ISLEM_ZAMANI;
                $exportData[$key]['KODLAYICI'] = $res->KODLAYICI;
            }

            Excel::create('Oecd Verileri', function ($excel) use ($exportData) {
                $excel->sheet('Oecd Verileri', function ($sheet) use ($exportData) {
                    $sheet->fromArray($exportData);
                });

            })->export('xlsx');

            return back();
        }

        return response()->json($response);

    }

    public function klinikKodlayiciTigVerileri()
    {
        $hospitals = DB::table('Hospitals')->select('HospitalCode', 'HospitalName');

        if ($this->user->hasRole('administrator') || $this->user->hasRole('owner')) {
            $hospitals = $hospitals->get();
        } elseif ($this->user->hasRole('hsmember')) {
            $hospitals = $hospitals->where('Hospitals.HospitalCode', $this->user->hospital_code)->get();
        } elseif ($this->user->hasRole('gsmember')) {
            $hospitals = $hospitals->where('Hospitals.Khb', $this->user->hospital_code)->get();
        }

        return view('pages.tig.klinik_kodlayici_tig_verileri',compact('hospitals'));
    }

    public function klinikKodlayiciTigVerileriniGetir(Request $request)
    {
        try {
            //validate
            $this->validate($request, [
                'to' => 'required',
                'from' => 'required',
                'hospital'=>'required'
            ]);

            //query
            $data = DB::connection('sqlsrv')
                        ->table('kds.v_klinik_kodlayici_bazli_tig_veri')
                        ->where('PeriodId', '<=', $request->to)
                        ->where('PeriodId', '>=', $request->from)
                        ->where('HospitalCode', $request->hospital)
                        ->orderBy('PeriodId', 'asc')
                        ->get();

            return response()->json($data);

        } catch (Exception $e) {
            return false;
        }
    }
}


