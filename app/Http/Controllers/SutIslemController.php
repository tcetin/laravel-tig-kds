<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use League\Flysystem\Exception;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;

class SutIslemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        \Carbon\Carbon::setlocale('tr');

        $this->user = Auth::user();

        $hospArray = [];

        $hospitals = DB::table('Hospitals')->select('HospitalCode', 'HospitalName', 'Khb');

        if ($this->user->hasRole('gsmember')) {
            $hospitals = $hospitals->where('Khb', $this->user->hospital_code)->get();

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        } elseif ($this->user->hasRole('administrator') || $this->user->hasRole('owner')) {
            $hospitals = $hospitals->get();
            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        }

        $dosya = \App\SutIslemDosya::whereIn('hastane_kodu', $hospArray)
            ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'sut_islem_dosyalari.hastane_kodu')
            ->select('sut_islem_dosyalari.id','sut_islem_dosyalari.hastane_kodu', 'Hospitals.HospitalName AS hastane_adi', 'sut_islem_dosyalari.yil', 'sut_islem_dosyalari.created_at', 'sut_islem_dosyalari.updated_at')
            ->get();

        return view('pages.yonetim.sut_islemleri.sut_islemleri_girisi.index', compact('hospitals', 'dosya'));
    }

    public function importData(Request $request)
    {
        \Carbon\Carbon::setlocale('tr');

        $formState = true;

        $this->user = Auth::user();

        $ext = strtolower($request->file->getClientOriginalExtension());

        $validator = Validator::make(
            [
                'file' => $request->file,
                'extension' => $ext,
                'hastane_kodu' => $request->hastane_kodu,
                'yil' => $request->yil
            ],
            [
                'file' => 'required',
                'extension' => 'required|in:csv,xlsx,xls',
                'hastane_kodu' => 'required',
                'yil' => 'required'
            ]
        );

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        } else {

            try {

                $datestamp = md5(Carbon::now());

                $fileName = $datestamp.request()->file('file')->hashName();

                $request->file->storeAs('public/sut_girisi', $fileName);

                $path = 'storage/sut_girisi/' . $fileName;

                //Dosya daha önce yüklenmiş mi?
                $sutDosya = \App\SutIslemDosya::where('hastane_kodu', $request->hastane_kodu)
                    ->where('yil', $request->yil);

                if ($sutDosya->first() != null) {
                    //Dosya mevcut is bu dosyaya ait islemleri sil
                    $sdosya = \App\SutIslemDosya::find($sutDosya->first()->id);
                    $sdosya->delete();//ilişkiden dolayı dosyaya ait işlemlerde silinecek.     
                }
            

                Excel::load($path, function ($reader) use ($request, $formState) {

                    $results = $reader->get()->toArray();

                    $dosya = null;

                    foreach ($results as $result) {
                        //Dosyaya ait işlemleri oluştur.

                        if ($result['sut_kodu'] != null
                            && ($result['ortalama_yatis_suresi'] != null || $result['islem_sayisi'] != null
                            || $result['tibbi_malzeme_tutari'] != null || $result['ilac_tutar'] != null
                            || $result['diger_islemler_tutari'] != null || $result['uzman_hekim_sayisi'] != null
                            || $result['yardimci_saglik_personeli_sayisi'] != null || $result['asistan_hekim_sayisi'] != null
                            || $result['anestezi_uzmani_sayisi']!=null || $result['islem_suresi'] != null)) {

                            //Dosyayı oluştur
                            if ($dosya == null) {
                                $dosya = new \App\SutIslemDosya;
                                $dosya->hastane_kodu = $request->hastane_kodu;
                                $dosya->yil = $request->yil;
                                $dosya->created_at = \Carbon\Carbon::now();
                                $dosya->updated_at = \Carbon\Carbon::now();
                                $dosya->kullanici = $this->user->username;

                                $dosya->save();
                            }

                            $islem = new \App\SutIslem;
                            $islem->sut_kodu = $result['sut_kodu'];
                            $islem->islem_sayisi = $result['islem_sayisi'];
                            $islem->ortalama_yatis_suresi = $result['ortalama_yatis_suresi'];
                            $islem->tibbi_malzeme_tutari = $result['tibbi_malzeme_tutari'];
                            $islem->ilac_tutar = $result['ilac_tutar'];
                            $islem->islem_tutari = $result['diger_islemler_tutari'];
                            $islem->uzman_hekim_sayisi = $result['uzman_hekim_sayisi'];
                            $islem->anestezi_uzmani_sayisi = $result['anestezi_uzmani_sayisi'];
                            $islem->ysp_sayisi = $result['yardimci_saglik_personeli_sayisi'];
                            $islem->asistan_hekim_sayisi = $result['asistan_hekim_sayisi'];
                            $islem->islem_suresi_dk = $result['islem_suresi'];
                            $islem->dosya_id = $dosya->id;

                            $islem->save();
                        } //endif
                    }

                });

                return back()->with('formState', $formState);
            } catch (\Exception $e) {

                return back()
                    ->with('errorMsg', 'Hata Oluştu:' . $e->getMessage());
            }

        }

    }

    public function exportData(Request $request)
    {
        \Carbon\Carbon::setlocale('tr');

        $formState = true;

        $this->user = Auth::user();

        $validator = Validator::make(
            [
                'dosya_id' => $request->dosya_id
            ],
            [
                'dosya_id' => 'required'
            ]
        ); 

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        } else {
            Excel::create('Sut İşlem Maliyet', function($excel) use($request) {
                $excel->sheet('İşlemler', function($sheet) use($request) {
                    $results = \App\SutIslemDosya::where('id',$request->dosya_id)->with('sutIslemleri')->get();
                    $resultArray = [];
                    $i = 0;
                    foreach ($results[0]->sutIslemleri as $result) {
                        $resultArray[$i]['Kurum Kodu'] = $results[0]->hastane_kodu;
                        $resultArray[$i]['Kurum Adı'] = DB::table('Hospitals')->where('HospitalCode',$results[0]->hastane_kodu)->first()->HospitalName;
                        $resultArray[$i]['Sut Kodu'] = $result->sut_kodu;
                        $resultArray[$i]['İşlem Sayısı'] = $result->islem_sayisi;
                        $resultArray[$i]['Ortalama Yatış Süresi'] = $result->ortalama_yatis_suresi;
                        $resultArray[$i]['Tibbi Malzeme Tutari'] = $result->tibbi_malzeme_tutari;
                        $resultArray[$i]['İlaç Tutarı'] = $result->ilac_tutar;
                        $resultArray[$i]['Diğer İşlemler Tutarı'] = $result->islem_tutari;
                        $resultArray[$i]['Uzman Hekim Sayısı'] = $result->uzman_hekim_sayisi;
                        $resultArray[$i]['Anestezi Uzmanı Sayısı'] = $result->anestezi_uzmani_sayisi;
                        $resultArray[$i]['Yardımcı Sağlık Personeli Sayısı'] = $result->ysp_sayisi;
                        $resultArray[$i]['Asistan Hekim Sayısı'] = $result->asistan_hekim_sayisi;
                        $resultArray[$i]['İşlem Süresi']= $result->islem_suresi_dk;
                        $i++;
                    }

                    $sheet->fromArray($resultArray);
                });
            })->export('xls');
            
            return back()->with('formState', $formState);

        }

    }

    public function destroyFile($id)
    {
        $formState = true;
        $model = \App\SutIslemDosya::find($id);
        $model->delete();
        return back()->with('formState', $formState);
    }

    public function avgSutIndex()
    {

        return view('pages.yonetim.sut_islemleri.sut_islemleri_girisi.sut_turkiye_ortalama');
    }

    public function avgSut(Request $request)
    {
        $data = DB::table('sut_islemleri')->select(
                'sut_kodu',
                DB::raw('
                sut_kodu,
                SUM(islem_sayisi) AS islem_sayisi,
                REPLACE(round(avg(ortalama_yatis_suresi),2),".",",") ortalama_yatis_suresi_ort,
                FORMAT(avg(tibbi_malzeme_tutari),2,"tr_TR") tibbi_malzeme_tutari_ort,
                FORMAT(avg(ilac_tutar),2,"tr_TR") ilac_tutar_ort,
                FORMAT(avg(islem_tutari),2,"tr_TR") islem_tutari_ort,
                REPLACE(round(avg(uzman_hekim_sayisi),2),".",",") uzman_hekim_sayisi_ort,
                REPLACE(round(avg(anestezi_uzmani_sayisi),2),".",",") anestezi_uzmani_sayisi_ort,
                REPLACE(round(avg(ysp_sayisi),2),".",",") ysp_sayisi_ort,
                REPLACE(round(avg(asistan_hekim_sayisi),2),".",",") asistan_hekim_sayisi_ort,
                REPLACE(round(avg(islem_suresi_dk),2),".",",") islem_suresi_dk_ort'))->groupBy('sut_kodu')->get();

        return response()->json($data);
    }
    
    
    public function sutEk2CIndex()
    {
       return view('pages.yonetim.sut_islemleri.ek2c.index',compact('dosya'));
    }

    public function getSutEk2C()
    {
        \Carbon\Carbon::setlocale('tr');

        $this->user = Auth::user();

        $hospArray = [];

        $hospitals = DB::table('Hospitals')->select('HospitalCode', 'HospitalName', 'Khb');

        if ($this->user->hasRole('gsmember')) {
            $hospitals = $hospitals->where('Khb', $this->user->hospital_code)->get();

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        } elseif ($this->user->hasRole('administrator') || $this->user->hasRole('owner')) {
            $hospitals = $hospitals->get();
            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        }

        $dosya = \App\SutEk2CIslemDosya::whereIn('hastane_kodu', $hospArray)
        ->join('Hospitals', 'Hospitals.HospitalCode', '=', 'sut_ek2c_islem_dosyalari.hastane_kodu')
        ->select('sut_ek2c_islem_dosyalari.id','sut_ek2c_islem_dosyalari.hastane_kodu', 'Hospitals.HospitalName AS hastane_adi', 'sut_ek2c_islem_dosyalari.yil', 
        'sut_ek2c_islem_dosyalari.created_at AS olusturulma_zamani', 'sut_ek2c_islem_dosyalari.updated_at AS guncellenme_zamani')
        ->get();

        return response()->json($dosya);
    }

    public function importEk2CData(Request $request)
    {
        \Carbon\Carbon::setlocale('tr');

        $formState = true;

        $this->user = Auth::user();

        $ext = strtolower($request->file->getClientOriginalExtension());

        $validator = Validator::make(
            [
                'file' => $request->file,
                'extension' => $ext,
                'hastane_kodu' => $request->hastane_kodu,
                'yil' => $request->yil
            ],
            [
                'file' => 'required',
                'extension' => 'required|in:csv,xlsx,xls',
                'hastane_kodu' => 'required',
                'yil' => 'required'
            ]
        );

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        } else {

            try {

                $datestamp = md5(Carbon::now());

                $fileName = $datestamp.request()->file('file')->hashName();

                $request->file->storeAs('public/sut_girisi/ek2c', $fileName);

                $path = 'storage/sut_girisi/ek2c/' . $fileName;

                //Dosya daha önce yüklenmiş mi?
                $sutDosya = \App\SutEk2CIslemDosya::where('hastane_kodu', $request->hastane_kodu)
                    ->where('yil', $request->yil);

                if ($sutDosya->first() != null) {
                    //Dosya mevcut is bu dosyaya ait islemleri sil
                    $sdosya = \App\SutEk2CIslemDosya::find($sutDosya->first()->id);
                    $sdosya->delete();//ilişkiden dolayı dosyaya ait işlemlerde silinecek.     
                }
            

                Excel::load($path, function ($reader) use ($request, $formState) {

                    $results = $reader->get()->toArray();

                    $dosya = null;

                    foreach ($results as $result) {
                        //Dosyaya ait işlemleri oluştur.

                        if ($result['sut_kodu'] != null
                            && ($result['hasta_basina_tibbi_malzeme_gideri_paket'] != null 
                            || $result['hasta_basina_tibbi_malzeme_gideri'] != null
                            || $result['hasta_basina_ilac_gideri_paket'] != null
                            || $result['hasta_basina_ilac_gideri'] != null
                            || $result['hasta_basina_lab_gideri']!=null
                            || $result['hasta_basina_radyoloji_gideri']!=null
                            || $result['hasta_basina_diger_gideri']!=null)) {

                            //Dosyayı oluştur
                            if ($dosya == null) {
                                $dosya = new \App\SutEk2CIslemDosya;
                                $dosya->hastane_kodu = $request->hastane_kodu;
                                $dosya->yil = $request->yil;
                                $dosya->created_at = \Carbon\Carbon::now();
                                $dosya->updated_at = \Carbon\Carbon::now();
                                $dosya->kullanici = $this->user->username;

                                $dosya->save();
                            }

                            $islem = new \App\SutEk2CIslem;
                            $islem->sut_kodu = $result['sut_kodu'];
                            $islem->hasta_basina_tibbi_malzeme_gideri_paket = $result['hasta_basina_tibbi_malzeme_gideri_paket'];
                            $islem->hasta_basina_tibbi_malzeme_gideri = $result['hasta_basina_tibbi_malzeme_gideri'];
                            $islem->hasta_basina_ilac_gideri_paket = $result['hasta_basina_ilac_gideri_paket'];
                            $islem->hasta_basina_ilac_gideri = $result['hasta_basina_ilac_gideri'];
                            $islem->hasta_basina_lab_gideri = $result['hasta_basina_lab_gideri'];
                            $islem->hasta_basina_radyoloji_gideri = $result['hasta_basina_radyoloji_gideri'];
                            $islem->hasta_basina_diger_gideri = $result['hasta_basina_diger_gideri'];
                            $islem->dosya_id = $dosya->id;

                            $islem->save();
                        } //endif
                    }

                });

                return response()->json('deneme');

            } catch (\Exception $e) {

                return response()->json(['errorMsg'=>$e->getMessage()]);
            }

        }

    }

    public function destroyEk2CFile($id)
    {
        $formState = true;
        $model = \App\SutEk2CIslemDosya::find($id);
        $model->delete();
        return response()->json(['formState'=>$formState]);
    }

    public function avgSutEk2CIndex()
    {
        return view('pages.yonetim.sut_islemleri.ek2c.ek2c_turkiye_ortalama');
    }

    public function avgSutEk2C()
    {
        $data = DB::table('sut_ek2c_islemleri')->select(
                'sut_kodu',
                DB::raw('
                FORMAT(avg(hasta_basina_tibbi_malzeme_gideri_paket),2,"tr_TR") hasta_basina_tibbi_malzeme_gideri_paket_ort,
                FORMAT(avg(hasta_basina_tibbi_malzeme_gideri),2,"tr_TR") hasta_basina_tibbi_malzeme_gideri_ort,
                FORMAT(avg(hasta_basina_ilac_gideri_paket),2,"tr_TR") hasta_basina_ilac_gideri_paket_ort,
                REPLACE(round(avg(hasta_basina_ilac_gideri),2),".",",") hasta_basina_ilac_gideri_ort,
                REPLACE(round(avg(hasta_basina_lab_gideri),2),".",",") hasta_basina_lab_gideri_ort,
                REPLACE(round(avg(hasta_basina_radyoloji_gideri),2),".",",") hasta_basina_radyoloji_gideri_ort,
                REPLACE(round(avg(hasta_basina_diger_gideri),2),".",",") hasta_basina_diger_gideri_ort'))->groupBy('sut_kodu')->get();

        return response()->json($data);
    }


}
