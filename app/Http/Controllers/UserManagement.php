<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\User;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Snowfire\Beautymail\Beautymail;
use Illuminate\Routing\UrlGenerator;

class UserManagement extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setHospitalCode($request)
    {
        $hospital = 0;
        switch ($request->rol) {
            case 'gsmember':
                $hospital = $request->hospitalsKhb;
                break;
            case 'hsmember':
                $hospital = $request->hospitals;
                break;
            default:
               # code...
                break;
        }

        return $hospital;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();

        $hospitals = DB::table('Hospitals')
            ->where('Hospitals.Aktif', '=', '1')
            ->select('Hospitals.HospitalCode', 'Hospitals.HospitalName','Hospitals.HospitalCity')
            ->get();

        $hospitalsKhb = DB::table('HospitalsKhb')
            ->select('HospitalsKhb.*')
            ->get();

        if ($user->roles->first()->name == 'GSMember') {
            $hospitals = $hospitals->where('HospitalCity',$user->hospital_code);
        }

        return view('pages.yonetim.kullanici_yonetimi')->with(['hospitals' => $hospitals, 'hospitalsKhb' => $hospitalsKhb]);
    }

    public function createUser(Request $request)
    {
        //dd($request);

        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'username'		=> 'required|max:11|unique:users',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'rol'           => 'required',
            'email'         => 'required|email',
            'hospitals'     => 'required' 
        );

       $messages = array(
            'required'=>':attribute alanı gereklidir.',
            'min'=>':attribute alanına en az :min karakter girmelisiniz',
            'alphaNum'=>':attribute alanına alfanümerik karakterler girebilirsiniz',
            'max'=>':attribute alanına en fazla :max karakter girmelisiniz',
            'unique'=>':attribute alanı birden fazla tanımlanamaz.'
        );
        
        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('/kullanici_yonetimi')
                ->withErrors($validator);
        } else {
            // store
            //dd($request);
            $user = new User;
            $pass = str_random(6);
            $user->username = Input::get('username');
            $user->first_name       = Input::get('first_name');
            $user->last_name       = Input::get('last_name');
            $user->email      = Input::get('email');
            $user->password = bcrypt($pass);
            $user->phone = Input::get('phone');
            $user->hospital_code = $this->setHospitalCode($request);
            

            $loginkey = md5(Input::get('username').$pass);

            $loginurl = url('/kdsdirectlogincheck').'?username='.Input::get('username').'&password='.$pass.'&loginkey='.$loginkey;

    

            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

            $beautymail->send('pages.emails.send_account', ['username'=>Input::get('username'),'pass'=>$pass,'loginurl'=>$loginurl], function($message)
            {
                $message
                    ->from('tigkds@saglik.gov.tr','Tig KDS')
                    ->to(Input::get('email'), Input::get('first_name').' '.Input::get('last_name'))
                    ->subject('Tig KDS Kullanıcı Bilgileri');
            });

             $user->save();

             $user->assignRole(Input::get('rol'));

            return Redirect::to('/kullanici_yonetimi');
        }
    }



    public function readUsers()
    {
    
        // SELECT
        //     u.*, r2.`name` AS RoleName,
        //     r2.description,
        //     (CASE r2.`name` WHEN 'HSMember' THEN h1.HospitalCode
        //                                     WHEN 'GSMember' THEN h2.KurumKod END) AS userHospCode,
        //     (CASE r2.`name` WHEN 'HSMember' THEN h1.HospitalName
        //                                     WHEN 'GSMember' THEN h2.KurumAd END) AS userHospitalName,
        //     h1.HospitalCity
        // FROM users u
        // INNER JOIN role_user r1 ON u.id = r1.user_id
        // INNER JOIN roles r2 ON r1.role_id = r2.id
        // LEFT JOIN Hospitals h1 ON u.hospital_code = h1.HospitalCode
        // LEFT JOIN HospitalsKhb h2 ON u.hospital_code = h2.KurumKod
        // ORDER BY u.updated_at DESC


    	$users = DB::table('v_user_list')->select('*');
        

        $currentUser = \Auth::user();

        if ($currentUser->roles->first()->name == 'GSMember') {
            // $filtered = array_where($users, function ($value, $key) use ($currentUser) {
            //     return $value->HospitalCity == $currentUser->hospital_code;
            // });
            $users = $users->where('HospitalCity',$currentUser->hospital_code)
                           ->where('id','<>',$currentUser->id);
        }

        return response()->json($users->get());  
    	
    }


    public function updateUser(Request $request)
    {
        
        $rules = array(
            //'username'      => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'rol'           => 'required',
            'email'         => 'required|email',
        );

       $messages = array(
            'required'=>':attribute alanı gereklidir.',
            'min'=>':attribute alanına en az :min karakter girmelisiniz',
            'alphaNum'=>':attribute alanına alfanümerik karakterler girebilirsiniz',
            'max'=>':attribute alanına en fazla :max karakter girmelisiniz'
        );
        
        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('/kullanici_yonetimi')
                ->withErrors($validator);
        } else {

            $user = User::where('username',$request->username)->first();
            //$user->username = $request->username;
            $user->first_name       = Input::get('first_name');
            $user->last_name       = Input::get('last_name');
            $user->email      = Input::get('email');
            $user->phone = Input::get('phone');

            if($request->rol){
                $user->revokeAllRoles();
                $user->hospital_code = $this->setHospitalCode($request);
                $user->assignRole($request->rol);
            }


            $user->save();


            return Redirect::to('/kullanici_yonetimi');
        }
    }

    public function destroyUser($id)
    {
        //var_dump($id);
        $user = User::find($id);
        //var_dump($user);
        $user->revokeAllRoles();
        $user->destroy($id);
        return Redirect::to('/kullanici_yonetimi');
    }


}
