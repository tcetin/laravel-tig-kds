<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use League\Flysystem\Exception;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;

class TibbiGiderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex()
    {
        \Carbon\Carbon::setlocale('tr');

        $this->user = Auth::user();

        $periods =\App\TigPeriod::all();

        $hospArray = [];

        if ($this->user->hasRole('gsmember')) {
            $hospitals = DB::table('Hospitals')->where('Khb', $this->user->hospital_code)->get();

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        }elseif($this->user->hasRole('administrator') || $this->user->hasRole('owner')){
            $hospitals = DB::table('Hospitals')->get();
            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        }

        $giderTurKategori = \App\TibbiGiderTurKategori::all();

        $gider = \App\TibbiGider::whereIn('hastane_kodu',$hospArray)->get();

        return view('pages.yonetim.tibbi_gider.index',compact('periods','giderTurKategori','gider'));
    }

    public function sampleExcel()
    {
        $this->user = Auth::user();


        Excel::create('Tıbbi Gider Örnek Exceli', function($excel) {

            $excel->sheet('Tıbbi Gider', function($sheet) {

                $sheet->setOrientation('landscape');

                // Manipulate first row
                $sheet->row(1, array(
                    'Hastane Kodu','Yatis No','Tig Kodu','Gider Miktarı(TL)'
                ));

                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  15,
                        'bold'      =>  true
                    )
                ));

                $sheet->setBorder('A1:E1', 'thin');

                $sheet->freezeFirstRow();

                $sheet->row(1, function($row) {
                    $row->setBackground('#ceb7b7');

                });

            });

        })->export('xls');
    }

    public function importData(Request $request)
    {
        $formState = true;

        $this->user = Auth::user();

        $hospArray = [];

        if ($this->user->hasRole('gsmember')) {
            $hospitals = DB::table('Hospitals')->where('Khb', $this->user->hospital_code)->get();

            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        }elseif($this->user->hasRole('administrator') || $this->user->hasRole('owner')){
            $hospitals = DB::table('Hospitals')->get();
            foreach ($hospitals as $hosp) {
                array_push($hospArray, $hosp->HospitalCode);
            }
        }

        $ext = strtolower($request->file->getClientOriginalExtension());

        $validator = Validator::make(
            [
                'file'      => $request->file,
                'extension' => $ext,
                'period'    => $request->period,
                'gider_tur' => $request->gider_tur
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:csv,xlsx,xls',
                'period'        => 'required',
                'gider_tur'     => 'required'
            ]
        );

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        }else {

            try {

                $datestamp = Carbon::now();

                $fileName = request()->file('file')->hashName().$datestamp.'.'.$ext;

                $request->file->storeAs('public/tibbi_gider',$fileName);

                $path = 'storage/tibbi_gider/'.$fileName;

                Excel::load($path, function ($reader) use ($request,$formState,$hospArray) {

                    $results = $reader->get()->toArray();

                    foreach ($results as $result) {

                        if (in_array($result['hastane_kodu'], $hospArray)) {
                            $isYatisNoExist = \App\TigPatient::where([
                                'HospitalCode' => $result['hastane_kodu'],
                                'Durum' => $request->period,
                                'Drg' => $result['tig_kodu'],
                                'AdmissionNo' => $result['yatis_no']
                            ])->first();

                            if ($isYatisNoExist) {

                                $matchThese = array(
                                    'donem_kodu' => $request->period,
                                    'hastane_kodu' => $result['hastane_kodu'],
                                    'yatis_no' => $result['yatis_no'],
                                    'drg' => $result['tig_kodu'],
                                    'gider_tur' => $request->gider_tur
                                );

                                $gider = \App\TibbiGider::where($matchThese)->first();


                                if ($gider) {
                                    $gider->miktar = $result['gider_miktaritl'];
                                    $gider->save();
                                } else {

                                    $model = new \App\TibbiGider;

                                    $model->donem_kodu = $request->period;
                                    $model->hastane_kodu = $result['hastane_kodu'];
                                    $model->yatis_no = $result['yatis_no'];
                                    $model->drg = $result['tig_kodu'];
                                    $model->gider_tur = $request->gider_tur;
                                    $model->miktar = $result['gider_miktaritl'];

                                    $model->save();

                                }
                            } else {
                                $formState = false;
                                $msg = $result['hastane_kodu'] . ' sağlık tesisine ait ' .
                                    $result['yatis_no'] . ' numaralı yatış numarası sistemde bulunamadı.Tig Kodu,Hastane Kodu ve Yatış numarasını tekrar kontrol ediniz.';
                                return back()
                                    ->with(['errorMsg' => 'Hata Oluştu:' . $msg, 'formState' => $formState]);
                            }
                        } else {
                            $formState = false;
                            $msg = $result['hastane_kodu'] . ' sağlık tesisine yetkiniz bulunmamakta.';
                            return back()
                                ->with(['errorMsg' => 'Hata Oluştu:' . $msg, 'formState' => $formState]);
                        }
                    }

                });

                return back()->with('formState',$formState);
            } catch (\Exception $e) {

                return back()
                    ->with('errorMsg','Hata Oluştu:'.$e->getMessage());
            }






        }

    }

    public function updateGider(Request $request,$id)
    {

        $formState = true;

        $this->validate($request,['gider_tur'=>'required','miktar'=>'required']);

        $model = \App\TibbiGider::find($id);

        $model->gider_tur = $request->gider_tur;
        $model->miktar = $request->miktar;
        $model->save();

        return back()->with('formState',$formState);
    }

    public function destroyGider($id)
    {
        $formState = true;
        $model = \App\TibbiGider::find($id);
        $model->delete();
        return back()->with('formState',$formState);
    }
}
