<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TibbiGiderTurController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Carbon\Carbon::setlocale('tr');

        $giderTur= \App\TibbiGiderTur::all();

        return view('pages.yonetim.tibbi_gider.gider_tur.index',compact('giderTur'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Carbon\Carbon::setlocale('tr');

        $formState = true;

        $this->validate($request, [
            'gider_tur' => 'required',
            'kategori_id' => 'required'
        ]);

        \App\TibbiGiderTur::create($request->all());


        return back()->with('formState',$formState);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        $formState = true;

        $this->validate($request, [
            'kategori_id' => 'required',
            'gider_tur' => 'required'
        ]);

        \App\TibbiGiderTur::where('id',$id)->update(array_except($fields,['_method','_token']));


        return back()->with('formState',$formState);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formState = true;
        $model = \App\TibbiGiderTur::find($id);
        $model->delete();
        return back()->with('formState',$formState);
    }
}
