<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\SendTestEmailNotification;
use Snowfire\Beautymail\Beautymail;
use \Auth;

class UserProfileController extends Controller
{
    protected $user_email;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show User Profile
     * @return view
     */
    public function showUserProfile()
    {
    	return view('pages.user.profile');
    }

    /**
     * Show	Change Email Form
     * @return view
     */


    public function showChangeEmailForm()
    {
    	return view('pages.user.change_email');
    }

    /**
     * Change Email
     * @return redirect redirect page
     */

    public function changeEmail(Request $request)
    {
        $this->user_email = $request->email;

        $rules = array(
            'email' => 'required|email|unique:users'
        );

       $messages = array(
            'required'=>':attribute alanı gereklidir.',
            'unique'=>'Sistemde birden fazla aynı eposta olamaz.Lütfen başka bir eposta adresi deneyiniz.',
            'email'=>':attribute alanı email formatında olmalı.'
        );
        
        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
        	return back()->withErrors($validator)->with('statu',false);
        } else {

            $user_email = $request->email;

            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

            $beautymail->send('pages.emails.send_test_email',[],function($message)
                         {
                                $message
                                    ->to($this->user_email)
                                    ->subject('Tig KDS TEST');
                          });    

            if(count(\Mail::failures()) > 0 ){
                return back()->withErrors('Email kullanılabilir bir adres değil.');
            }       

        	$user = Auth::user();
        	$user->email = $request->email;
        	$user->save();

            \Session::flash('statu',true); 

        	return back();
        }

    }

    public function showChangePasswordForm()
    {
    	return view('pages.user.change_password');
    }

    /**
     * Change Pass
     * @return  redirect redirect page
     */

    public function changePassword(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'password'=> 'required|min:6'
        );

       $messages = array(
            'required'=>':attribute alanı gereklidir.',
            'min'=>':attribute alanı en az :min karakterli olmalı.'
        );
        
        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
        	return back()->withErrors($validator);
        } else {
        	$user = Auth::user();
        	$user->password = bcrypt($request->password);
        	$user->save();

            \Session::flash('statu',true);

        	return back();
        }

    }


}
