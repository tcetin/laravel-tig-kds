<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Log;
use App\Repositories\HospitalRepository as hospitalRepo;
use App\Repositories\Criteria\Hospitals\FindAllInSearchCriteria;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Redirect;
use App\Hospital;

class HospitalController extends Controller
{
    private $_repository;

    public function __construct(hospitalRepo $repository)
    {
        $this->_repository = $repository;

        $this->middleware('auth');
    }

    public function index()
    {
        $hospitalsKhb = \App\Khb::all();

        return view('pages.yonetim.kurum_yonetimi', compact("hospitalsKhb"));
    }

    public function getHospitals(Request $request)
    {
        $criteria = new FindAllInSearchCriteria("Statu", $request->extra, $request->search, "HospitalName");
        $this->_repository->pushCriteria($criteria);

        return response()->json($this->_repository->all());
    }

    public function getHospitalsDetails(Request $request)
    {
        return $this->_repository->getHospitalsWithDetails();
    }

    public function createHospital(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        
        $rules = array(
            'hospital_code' => 'required',
            'hospital_name' => 'required',
            'hospital_khb' => 'required',
            'hospital_type' => 'required'
        );

        $messages = array(
            'required' => ':attribute alanı gereklidir.'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        // process the login
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $isExist = \App\Hospital::where('HospitalCode', '=', $request->hospital_code)->first();
            if ($isExist == null) {

                $hospital = new \App\Hospital;
                $hospital->HospitalCode = $request->hospital_code;
                $hospital->HospitalName = $request->hospital_name;
                $hospital->HospitalCity = $request->hospital_khb;
                $hospital->Khb = $request->hospital_khb;
                $hospital->Aktif='1';
                $hospital->save();
            }
        }

    }

    public function updateHospital(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        
        $rules = array(
            'id' => 'required',
            'hospital_code' => 'required',
            'hospital_name' => 'required',
            'hospital_khb' => 'required',
            'hospital_type' => 'required'
        );

        $messages = array(
            'required' => ':attribute alanı gereklidir.'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        // process the login
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $hospital = \App\Hospital::where('id', '=', $request->id)->first();

            if ($hospital != null) {
                $hospital->HospitalCode = $request->hospital_code;
                $hospital->HospitalName = $request->hospital_name;
                $hospital->HospitalCity = $request->hospital_khb;
                $hospital->Khb = $request->hospital_khb;
                $hospital->save();
            }
        }

    }

    public function destroyHospital($id)
    {
        $hosp = Hospital::where('id', '=', $id)->first();
        if (!is_null($hosp)) {
            $hosp->destroy($id);
        }
    }

}
