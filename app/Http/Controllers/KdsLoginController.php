<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class KdsLoginController extends Controller
{

    public function showLoginForm()
    {
        return view('pages.login');
    }

    public function kdsLoginCheck(Request $request){

        $rules = array(
            'username'    => 'required|min:3',
            'password' => 'required'
        );

        $messages = array(
            'required'=>':attribute alanı gereklidir.',
            'min'=>':attribute alanına en az :min karakter girmelisiniz',
            'alphaNum'=>':attribute alanına alfanümerik karakterler girebilirsiniz'
        );

        $validator = Validator::make(Input::all(), $rules,$messages);
   

        if ($validator->fails()) {
            return Redirect::to('kdslogin')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {


            if (Auth::attempt(array('username' => $request->username, 'password' => $request->password))) {

                flash()->success('Giriş başarılı');

                return Redirect::to('/');

            }

            flash()->error('Giriş başarısız');

            return Redirect::to('/kdslogin');
        }
    }
    public function kdsDirectLogin(Request $request)
    {
        $rules = array(
            'username'    => 'required|min:3',
            'password' => 'required',
            'loginkey'=>'required'
        );

        $messages = array(
            'required'=>':attribute alanı gereklidir.',
            'min'=>':attribute alanına en az :min karakter girmelisiniz',
            'alphaNum'=>':attribute alanına alfanümerik karakterler girebilirsiniz'
        );

        $validator = Validator::make(Input::all(), $rules,$messages);

        if ($validator->fails()) {
            return Redirect::to('kdslogin')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

        if ($request->loginkey==md5($request->username.$request->password)) {

            if (Auth::attempt(array('username' => $request->username, 'password' => $request->password))) {

                flash()->success('Giriş başarılı');

                return Redirect::to('/');

            }

         }

            flash()->error('Giriş başarısız');

            return Redirect::to('/kdslogin');
        }
    }

    public function kdsLogout(){
        Auth::logout();
        return Redirect::to('/kdslogin');
    }

}
