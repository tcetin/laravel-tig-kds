<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Log;
use App\Repositories\HospitalRepository as Hospital;
use App\Repositories\BranchRepository as Branch;

class DinamikRaporlamaController extends Controller
{
    private $_hospitals;
    private $_branches;

    public function __construct(Hospital $hospitals,Branch $branch)
    {
        $this->_hospitals = $hospitals;
        $this->_branches = $branch;

        $this->middleware('auth');
    }

    public function index()
    {
        $hospitals = $this->_hospitals->all();
        return view('pages.tig.dinamik_raporlama', compact('hospitals'));
    }

    public function getBranches(Request $request)
    {
        if (!empty($request->search) && !empty($request->list)) {
            $collection = $this->_branches->findAllIn('BRANS', $request->list);
            $query = $collection->where('ACIKLAMA', 'like', '%' . $request->search . '%')->get();
            return response()->json($query);
        } elseif (!empty($request->search)) {
            $query = $this->_branches->whereLike('ACIKLAMA', $request->search);
            return response()->json($query);
        }

        return response()->json($this->_branches->all());
    }
}
