<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use League\Flysystem\Exception;
use Maatwebsite\Excel\Facades\Excel;
use Validator;


class ExcelController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');  
    }
    public function sampleExcel(Request $request)
    {
        $this->user = Auth::user();

        $this->validate($request,[
            'name' => 'required',
            '_headers' => 'required',
            'path' => 'required'
        ]);  

        // array(
        //     'Sut Kodu', 'Islem Adi','Islem Sayisi','Ortalama Yatis Suresi', 'Tibbi Malzeme Tutari', 'Ilac Tutar',
        //     'Diger Islemler Tutari', 'Uzman Hekim Sayisi', 'Anestezi Uzmani Sayisi','Yardimci Saglik Personeli Sayisi',
        //     'Asistan Hekim Sayisi', 'Islem Suresi'
        // )

        Excel::create($request->name, function ($excel) use($request) {
            $excel->sheet('Sayfa', function ($sheet) use($request) {

                $sheet->setOrientation('landscape');
                
                // Manipulate first row
                $sheet->row(1,$request->_headers);

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Calibri',
                        'size' => 12,
                        'bold' => true
                    )
                ));

                $sheet->setBorder('A1:E1', 'thin');

                $sheet->freezeFirstRow();

                $sheet->row(1, function ($row) {
                    $row->setBackground('#EDEDED');

                });

                //Set Cell values from starting A2 to n 

                // $url = public_path('sut_kodlari.json');

                $url = public_path($request->path);

                $json = json_decode(file_get_contents($url), true);

                $start = 2;

                foreach ($json as $key => $value) {

                    $sheet->cell('A' . $start, function ($cell) use ($key) {
                        $cell->setValue($key);

                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => false
                        ));

                        $cell->setBackground('#EDEDED');

                    });

                    $sheet->cell('B' . $start, function ($cell) use ($value) {
                        $cell->setValue($value);

                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => false
                        ));

                        $cell->setBackground('#EDEDED');
                    });

                    $start++;
                }



            });

        })->export('xls');
    }
}
