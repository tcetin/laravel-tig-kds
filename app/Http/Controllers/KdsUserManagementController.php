<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class KdsUserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        $hospitals = DB::table('Hospitals')
                            ->where('Hospitals.Aktif','=','1')  
                            ->select('Hospitals.HospitalCode','Hospitals.HospitalName')
                            ->get();   

         $hospitalsKhb = DB::table('HospitalsKhb')
                            ->select('HospitalsKhb.*')
                            ->get();  

        return view('pages.yonetim.kullanici_yonetimi')->with(['hospitals'=>$hospitals,'hospitalsKhb'=>$hospitalsKhb]);
    }

    public function show()
    {
       $users = DB::select('
                    SELECT u.*,r2.*,h1.HospitalCode AS userHospCode,h1.HospitalName AS userHospitalName,h2.KurumKod AS adminHospCode,h2.KurumAd AS adminHospName
                    FROM users u 
                    INNER JOIN role_user r1 ON u.id=r1.user_id
                    INNER JOIN roles r2 ON r1.role_id=r2.id
                    LEFT JOIN Hospitals h1 ON u.hospital_code=u.hospital_code
                    LEFT JOIN HospitalsKhb h2 ON u.hospital_code = h2.KurumKod');

        return response()->json($users);    
        
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd('test');
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'first_name'      => 'required',
            'last_name'       => 'required',
            'rol'       => 'required',
            'email'      => 'required|email'
        );

        
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('/kullanici_yonetimi')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $user = new User;
            $user->first_name       = Input::get('first_name');
            $user->last_name       = Input::get('last_name');
            $user->email      = Input::get('email');
            $user->phone = Input::get('phone');
            $user->save();

            // redirect
            Session::flash('message', 'Successfully created nerd!');
            return Redirect::to('/kullanici_yonetimi');
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
