<?php

namespace App\Http\Controllers;

use phpDocumentor\Reflection\Types\Array_;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class PagesController extends Controller
{
    protected $cntHospitals;
    protected $cntUsers;
    protected $hospital;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole('administrator|owner')) {
            $this->cntHospitals = DB::table('Hospitals') ->where('Aktif','1')->count();
            $this->cntUsers = DB::table('TIGUsers')->where('Active',1)->where('Rol',1)->count();
        }elseif($user->hasRole('hsmember')){
            $this->cntHospitals = DB::table('Hospitals') ->where('Aktif','1')->where('HospitalCode',$user->hospital_code)->count();
            $this->cntUsers = DB::table('TIGUsers')->where('Active',1)->where('Rol',1)->where('HospId',$user->hospital_code)->count();
            $hospitalName = DB::table('Hospitals') ->where('Aktif','1')->where('HospitalCode',$user->hospital_code)->select('HospitalName')->first();
            $this->hospital=$hospitalName->HospitalName;
        }elseif($user->hasRole('gsmember')){
            $this->cntHospitals = DB::table('Hospitals') ->where('Aktif','1')->where('Khb',$user->hospital_code)->count();
            $this->cntUsers = DB::table('TIGUsers')
                                    ->join('Hospitals','TIGUsers.HospId','=','Hospitals.HospitalCode')
                                    ->where('TIGUsers.Active',1)
                                    ->where('TIGUsers.Rol',1)
                                    ->where('Hospitals.Khb',$user->hospital_code)
                                    ->count();

           $hospitalName = DB::table('HospitalsKhb')->where('KurumKod',$user->hospital_code)->select('KurumAd')->first();  
           $this->hospital=$hospitalName->KurumAd;
        }


        $cntDrg =DB::connection('sqlsrv')->table('KART_HESAPLANANTIG')->count();

        $cntCode =DB::connection('sqlsrv')->table('KODLAR')->count();

        $cntIcd = DB::connection('sqlsrv')->table('KODLAR')->where('TIP','I')->count();

        $cntAchi = DB::connection('sqlsrv')->table('KODLAR')->where('TIP','A')->count();

        $cntMorph = DB::connection('sqlsrv')->table('KODLAR')->where('TIP','M')->count();

        //$hospitalName = ($hospitalName or '') == true ? $hospitalName : '';

        $reports = [
                    'hospitals_cnt'=>$this->cntHospitals,
                    'users_cnt'=>$this->cntUsers,
                    'drg_cnt'=>$cntDrg,
                    'code_cnt'=>$cntCode,
                    'icd_cnt'=>$cntIcd,
                    'achi_cnt'=>$cntAchi,
                    'morph_cnt'=>$cntMorph,
                    'hospital_name'=>$this->hospital
                ];


        return view('pages.index',$reports);
    }
}
