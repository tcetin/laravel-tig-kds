<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TibbiGiderTurKategori extends Model
{
    protected $connection = 'sqlsrv';
    protected $fillable = ['kategori'];
    protected $table='kds.tibbi_gider_tur_kategori';

    public function giderTur()
    {
        return $this->hasMany('App\TibbiGiderTur','kategori_id');
    }
}
