<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use StringifyGuidRule;

class Khb extends Model
{
	protected $table = 'HospitalsKhb';
    protected $fillable =['KurumKod','KurumAd','Il'];

    public function Hospitals()
    {
        return $this->hasMany('\App\Hospital','Khb','KurumKod');
    }

    // protected static function boot()
    // {
    //     parent::boot();

    //     //Any time this model is used, it will implement the StringifyGuidRule
    //     static::addGlobalScope(new StringifyGuidRule());
    // }
}
