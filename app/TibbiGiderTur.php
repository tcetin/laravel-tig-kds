<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TibbiGiderTur extends Model
{
    protected $connection = 'sqlsrv';
    protected $fillable = ['gider_tur','kategori_id'];
    protected $table='kds.tibbi_gider_tur';

    public function kategori()
    {
        return $this->belongsTo('App\TibbiGiderTurKategori');
    }

    public function gider(){
        return $this->hasMany('App\TibbiGider');
    }

}
