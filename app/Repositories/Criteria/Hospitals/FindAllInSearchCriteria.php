<?php namespace App\Repositories\Criteria\Hospitals;

use App\Repositories\Contracts\RepositoryInterface as Repository;
use App\Repositories\Contracts\RepositoryInterface;
use Auth;
use App\Repositories\Criteria\Criteria;

class FindAllInSearchCriteria extends Criteria {

    private $_inColumn;
    private $_array;
    private $_searchText;
    private $_searchColumn;

    public function __construct($inColumn,$array,$searchText,$searchColumn)
    {
        $this->_inColumn = $inColumn;
        $this->_array = $array;
        $this->_searchText = $searchText;
        $this->_searchColumn = $searchColumn;
    }


    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model;

        if (!empty($this->_array) && !empty($this->_inColumn)) {
            $query = $query->whereIn($this->_inColumn, $this->_array);
        }

        if (!empty($this->_searchText) && !empty($this->_searchColumn)) {
            $query = $query->where($this->_searchColumn, 'like', '%' . $this->_searchText . '%');
        }

        $query = $query->select('HospitalCode AS id', 'HospitalName AS text');

        return $query;
    }
}