<?php namespace App\Repositories;

use App\Repositories\Eloquent\Repository;
use App\Hospital;
use Illuminate\Database\Eloquent\Model;

class HospitalRepository extends Repository {

    // model property on class instances
    protected $model;

    // // Constructor to bind model to repo
    // public function __construct(Hospital $model)
    // {
    //     $this->model = $model;
    // }

    public function model()
    {
        return '\App\Hospital';
    }

    // Get all instances of model

    // create a new record in the database
    public function create(array $data)
    {
        return response()->json($data);
    }

    public function getHospitalsWithDetails()
    {
        $hospitals = [];
        $hospitalKhb = \App\Khb::all();

        foreach ($hospitalKhb as $khb) {
            foreach ($khb->Hospitals as $hospital) {
                $hospitals[] = [
                    'HospitalCode' => $hospital->HospitalCode,
                    'HospitalName' => $hospital->HospitalName,
                    'Il'=> $khb->Il,
                    'hospitalId'=>$hospital->id
                ];
            }
        }
        
        return response()->json($hospitals);
    }


    // // update record in the database
    // public function update(array $data, $id)
    // {
    //     $record = $this->find($id);
    //     return $record->update($data);
    // }   
    
    // // remove record from the database
    // public function delete($id)
    // {
    //     return $this->model->destroy($id);
    // } 
    
    // // show the record with the given id
    // public function show($id)
    // {
    //     return $this->model-findOrFail($id);
    // }

    // // Get the associated model
    // public function getModel()
    // {
    //     return $this->model;
    // }

    // // Set the associated model
    // public function setModel($model)
    // {
    //     $this->model = $model;
    //     return $this;
    // }

    // // Eager load database relationships
    // public function with($relations)
    // {
    //     return $this->model->with($relations);
    // }

}
