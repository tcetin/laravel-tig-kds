<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TigPeriod extends Model
{
    protected $fillable = ['Id','DonemAdi'];
    protected $connection='sqlsrv';
    protected $table='KART_DONEM';

    public function gider()
    {
        return $this->hasMany('App\TibbiGider');
    }
}
