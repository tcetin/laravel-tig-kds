<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TibbiGider extends Model
{
    protected $connection='sqlsrv';
    protected $table='kds.tig_tibbi_gider';

    public function giderTur()
    {
        return $this->belongsTo('App\TibbiGiderTur','gider_tur');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospital','hastane_kodu','HospitalCode');
    }

    public function tigKodu()
    {
        return $this->belongsTo('App\KartTig','drg','TIG');
    }

    public function donem()
    {
        return $this->belongsTo('App\TigPeriod','donem_kodu','Id');
    }


}
