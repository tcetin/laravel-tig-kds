<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KartTig extends Model
{
    protected $connection='sqlsrv';
    protected $table ='KART_HESAPLANANTIG';

    public function gider()
    {
        return $this->hasMany('App\TibbiGider');
    }
}
