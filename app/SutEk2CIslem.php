<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SutEk2CIslem extends Model
{
    protected $table='sut_ek2c_islemleri';
    protected $fillable = [
        'sut_kodu',
        'hasta_basina_tibbi_malzeme_gideri_paket',
        'hasta_basina_tibbi_malzeme_gideri',
        'hasta_basina_ilac_gideri_paket',
        'hasta_basina_ilac_gideri',
        'hasta_basina_lab_gideri',
        'hasta_basina_radyoloji_gideri',
        'hasta_basina_diger_gideri',
        'dosya_id' 
    ];

    public function islemDosya()
    {
        return $this->belongsTo('App\SutEk2CIslemDosya','dosya_id');
    }
}
