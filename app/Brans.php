<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brans extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'TB_BRANS';
}
