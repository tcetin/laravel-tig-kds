<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SutKod extends Model
{
    protected $table = 'sut_kodlari';

    protected $fillable = ['kod','aciklama'];
}
