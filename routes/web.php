<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('pages.index');
});*/

Route::get('/kdslogin','KdsLoginController@showLoginForm');
Route::post('/kdslogincheck','KdsLoginController@kdsLoginCheck');
Route::get('/kdsdirectlogincheck','KdsLoginController@kdsDirectLogin');
Route::get('/kdslogout','KdsLoginController@kdsLogout');

Route::get('/password/reset','Auth\ForgotPasswordController@showLinkRequestForm');
Route::get('/password/reset/{token}','Auth\ResetPasswordController@showResetForm');
Route::post('/password/reset','Auth\ResetPasswordController@reset');
Route::post('/password/email','Auth\ForgotPasswordController@sendResetLinkEmail');

Route::get('/','PagesController@index');

/**
 * Hastane Listesi
 */
Route::get('/hastane_listesi','HospitalController@getHospitals');
Route::get('/export_sample_excel', 'ExcelController@sampleExcel')->name('sample_excel');

/**
 * Ortak Alan-Dönemsel Hastane Verileri
 */
Route::get('/tig/donemsel_hastane_verileri', array( 'uses' => 'TigController@donemselHastaneVerileri'));
Route::post('/tig/donemsel_hastane_verilerini_getir', array( 'uses' => 'TigController@donemselHastaneVerileriniGetir'));

/**
 * Klinik kodlayici verilerini getir
 */

Route::get('/tig/klinik_kodlayici_tig_verileri', array( 'uses' => 'TigController@klinikKodlayiciTigVerileri'));
Route::post('/tig/klinik_kodlayici_tig_verilerini_getir', array( 'uses' => 'TigController@klinikKodlayiciTigVerileriniGetir'));

/**
 * Branş Bazlı Hastanelerin Emek Bağıl Değerleri(Vki)
 */
Route::get('/tig/brans_bazli_hastane_emek_verileri', array( 'uses' => 'TigController@bransBazliEmekVerileri'));
Route::post('/tig/brans_bazli_hastane_emek_verilerini_getir', array( 'uses' => 'TigController@bransBazliEmekVerileriniGetir'));

Route::get('/tig/donemsel_hastane_tig_yatis_verileri', array( 'uses' => 'TigController@donemselHastaneYatisBazliTigVerileri'));
Route::post('/tig/donemsel_hastane_tig_yatis_verilerini_getir', array( 'uses' => 'TigController@donemselHastaneYatisBazliTigVerileriniGetir'));

/**
 * KURUM BAZLI KLİNİK KODLAYICI SAYILARI
 */

Route::get('/tig/kurum_bazli_klinik_kodlayici_sayilari', array( 'uses' => 'TigController@kurumBazliKlinikKodlayiciSayilari'));

/* ############################## OECD VERİLERİ - KLİNİK VERİLER#################################*/
Route::get('/tig/oecd_donemsel_hastane_bazli_veriler', array( 'uses' => 'TigController@oecdHastaneBazliKlinikVeri'));
Route::get('/tig/oecd_donemsel_hastane_bazli_verileri_getir', array( 'uses' => 'TigController@oecdHastaneBazliKlinikVerileriGetir'));

/**
 * Tıbbi gider maliyet yönetimi
 */
Route::group(['middleware' => ['auth', 'acl'],'is'=>('administrator|owner|GSMember')],
    function () {
        Route::get('/tibbi_gider_maliyet', 'TibbiGiderController@getIndex');
        Route::get('/tibbi_gider_maliyet/export_sample_excel', 'TibbiGiderController@sampleExcel');
        Route::post('/tibbi_gider_maliyet/import_data', 'TibbiGiderController@importData');
        Route::delete('/tibbi_gider_maliyet/delete/{id}', 'TibbiGiderController@destroyGider');
        Route::patch('/tibbi_gider_maliyet/update/{id}', 'TibbiGiderController@updateGider');
});

/**
 * Sut islem girisi
 */

Route::group(['middleware' => ['auth', 'acl'],'is'=>('administrator|owner')],
function () {
	Route::get('/sut_islem_bilgileri', 'SutIslemController@index');
	Route::post('/sut_islem_bilgileri/import_data', 'SutIslemController@importData');
	Route::get('/sut_islem_bilgileri/export_data/{dosya_id}', 'SutIslemController@exportData');
	Route::delete('/sut_islem_bilgileri/dosya_sil/{id}', 'SutIslemController@destroyFile');

	Route::get('/sut_islem_ortalama_index', 'SutIslemController@avgSutIndex');
	Route::get('/sut_islem_ortalama', 'SutIslemController@avgSut');

	Route::get('/sut_ek2c_islemleri', 'SutIslemController@sutEk2CIndex');
	Route::get('/sut_ek2c_islem_verileri', 'SutIslemController@getSutEk2C');
	Route::post('/sut_ek2c_islem_bilgileri/import_data', 'SutIslemController@importEk2CData');
	Route::delete('/sut_ek2c_islem_bilgileri/dosya_sil/{id}', 'SutIslemController@destroyEk2CFile');

	Route::get('/sut_ek2c_islem_ortalama_index', 'SutIslemController@avgSutEk2CIndex');
	Route::get('/sut_ek2c_islem_ortalama', 'SutIslemController@avgSutEk2C');
});

Route::group(['middleware' => ['auth', 'acl'],'is'=>('administrator|owner')], 
function () {

/**
 * Tanısal İşlemler Routing 
 */

Route::get('/tig/donemsel_turkiye_geneli_tig_verileri', array( 'uses' => 'TigController@turkiyeGeneliDonemselVeriler'));
Route::post('/tig/donemsel_turkiye_geneli_tig_verilerini_getir', array( 'uses' => 'TigController@turkiyeGeneliDonemselVerileriniGetir'));


Route::get('/tig/donemsel_turkiye_geneli_drg_bazli_veriler', array( 'uses' => 'TigController@turkiyeGeneliDonemselDrgBazliVeriler'));
Route::post('/tig/donemsel_turkiye_geneli_drg_bazli_verilerini_hesapla', array( 'uses' => 'TigController@turkiyeGeneliDonemselDrgBazliVerileriniHesapla'));

Route::get('/tig/donemsel_turkiye_geneli_vaka_sayilari', array( 'uses' => 'TigController@turkiyeGeneliDonemselVakaSayilari'));
Route::post('/tig/donemsel_turkiye_geneli_vaka_sayilarini_getir', array( 'uses' => 'TigController@turkiyeGeneliDonemselVakaSayilariniGetir'));


Route::get('/tig/donemsel_hastanebazli_drg_sayilari', array( 'uses' => 'TigController@donemselHastaneBazliDrgSayilari'));
Route::get('/tig/donemsel_hastanebazli_drg_sayilarini_getir', array( 'uses' => 'TigController@donemselHastaneBazliDrgSayilariniGetir'));




/***
 * TÜRKİYE GENELİ DÖNEMSEL KLİNİK KOD SAYILARI
 */

Route::get('/tig/donemsel_turkiye_geneli_klinik_kod_verileri', array( 'uses' => 'TigController@turkiyeGeneliDonemselKlinikKodSayilari'));
Route::post('/tig/donemsel_turkiye_geneli_klinik_kod_verileri_getir',array( 'uses' => 'TigController@turkiyeGeneliDonemselKlinikKodSayilariniGetir'));

/**
 * ANA TANI BAŞINA DÜŞEN EK TANI VE İŞLEM SAYILARI ROUTE
 */

Route::get('/tig/donemsel_anatani_basina_dusen_ektani', array( 'uses' => 'TigController@donemselAnataniBasinaDusenEktaniSayilari'));
Route::post('/tig/donemsel_anatani_basina_dusen_ektani_sayilarini_getir', array( 'uses' => 'TigController@donemselAnataniBasinaDusenEktaniSayilariniHesapla'));

Route::get('/tig/klinik_kod_listesi',array( 'uses' => 'TigController@klinikKodlar'));
/**
 * TÜRKİYE GENELİ DÖNEMSEL HASTA BAŞINA DÜŞEN İŞLEM ORANI İŞLEM/HASTA
 */
Route::get('/tig/donemsel_turkiye_geneli_islem_hasta_oranlarini_hesapla', array( 'uses' => 'TigController@donemselHastaBasinaDusenIslemSayilariniHesapla'));
Route::get('/tig/donemsel_turkiye_geneli_islem_hasta_oranlari', array( 'uses' => 'TigController@donemselHastaBasinaDusenIslemSayilari'));

/**
 * DÖNEMSEL CİNSİYET VE YAŞ FREKANSLARI
 */
Route::get('/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslarini_hesapla', array( 'uses' => 'TigController@donemselCinsiyetVeYasFrekanslariniHesapla'));
Route::get('/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslarini_toplam_hesapla', array( 'uses' => 'TigController@donemselCinsiyetVeYasFrekanslariniToplamHesapla'));
Route::get('/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslari', array( 'uses' => 'TigController@donemselCinsiyetVeYasFrekanslari'));

/**
 * DÖNEMSEL DRG BAZLI CİNSİYET VE YAŞ FREKANSLARI
 */
Route::get('/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslari', array( 'uses' => 'TigController@donemselTigBazliCinsiyetVeYasFrekanslari'));
Route::get('/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslarini_hesapla', array( 'uses' => 'TigController@donemselTigBazliCinsiyetVeYasFrekanslariniHesapla'));
Route::get('/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslarini_toplam_hesapla', array( 'uses' => 'TigController@donemselTigBazliCinsiyetVeYasFrekanslariniToplamHesapla'));


/**
 * TİG BAZLI DÖNEMSEL GÜNÜBİRLİK VE DİĞER YATIŞLARDAKİ VAKA FREKANSLARI
 */
Route::get('/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari', array( 'uses' => 'TigController@donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslari'));
Route::get('/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari_hesapla', array( 'uses' => 'TigController@donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslariHesapla'));
Route::get('/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari_toplam_hesapla', array( 'uses' => 'TigController@donemselTigBazliGunubirlikVeDigerYatislardakiVakaFrekanslariToplamHesapla'));

/**
 * YAŞ VE CİNSİYETE GÖRE TANI(ICD-EK-ANA) SAYILARI
 */
Route::get('/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilari', array( 'uses' => 'TigController@donemselTurkiyeGeneliYasveCinsiyetBazliTaniSayilari'));
Route::get('/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilarini_hesapla', array( 'uses' => 'TigController@donemselTurkiyeGeneliYasveCinsiyetBazliTaniSayilariniHesapla'));





/* ####################   DOĞUM VERİLERİ ########################## */
Route::get('/dogum/dogum_frekans_verileri', array( 'uses' => 'TigController@dogumFrekansVerileri'));
Route::get('/dogum/dogum_verileri', array( 'uses' => 'TigController@dogumVerileri'));
Route::get('/dogum/dogum_frekans_verilerini_kurum_turu_bazinda_hesapla', array( 'uses' => 'TigController@dogumFrekansVerileriniKurumTuruBazindaHesapla'));
Route::get('/dogum/dogum_frekans_verilerini_il_bazinda_hesapla', array( 'uses' => 'TigController@dogumFrekansVerileriniIlBazindaHesapla'));

Route::get('/dogum/normal_sezaryen_dogum_anatani_ektani_verileri', array( 'uses' => 'TigController@normalDgmTaniVerileri'));
Route::get('/dogum/normal_sezaryen_dogum_anatani_ektani_verilerini_hesapla', array( 'uses' => 'TigController@normalDgmTaniVerileriniHesapla'));



Route::get('/dogum/yd_kilo_verileri', array( 'uses' => 'TigController@yenidoganKiloVerileri'));
Route::get('/dogum/yd_kilo_verilerini_hesapla', array( 'uses' => 'TigController@yenidoganKiloVerileriniKurumBazindaHesapla'));


});//End  Route Authentication


/**
 * User management
 */
Route::group(
	['middleware' => ['auth', 'acl'], 'is' => ('owner|gsmember')],
	function () {
		Route::get('/kullanici_yonetimi', 'UserManagement@index');
		Route::get('/kullanici_yonetimi/kullanicilar', 'UserManagement@readUsers');
		Route::get('/kullanici_yonetimi/kullanici_olustur', 'UserManagement@createUser');
		Route::get('/kullanici_yonetimi/kullanici_guncelle', 'UserManagement@updateUser');
		Route::get('/kullanici_yonetimi/kullanici_sil/{id}', 'UserManagement@destroyUser');
	}
);


/**
 * User management
 */
Route::group(['middleware' => ['auth', 'acl'],'is'=>('owner')],
	function(){

		Route::get('/dinamik_raporlama','DinamikRaporlamaController@index');

		Route::resource('tibbi_gider_tur_kategori','TibbiGiderTurKategoriController');
		Route::resource('tibbi_gider_tur','TibbiGiderTurController');
		
		Route::get('/kurum_yonetimi','HospitalController@index');
		Route::get('/kurum_yonetimi/kurumlar','HospitalController@getHospitalsDetails');
		Route::post('/kurum_yonetimi/kurum_olustur','HospitalController@createHospital');
		Route::get('/kurum_yonetimi/kurum_guncelle','HospitalController@updateHospital');
		Route::get('/kurum_yonetimi/kurum_sil/{id}','HospitalController@destroyHospital');
});

/**
 * User Profile
 */
Route::group(['middleware' => ['auth', 'acl']],
	function(){
		Route::get('/user_profile','UserProfileController@showUserProfile');
		Route::get('/user_profile/change_email_page','UserProfileController@showChangeEmailForm');
		Route::post('/user_profile/change_email','UserProfileController@changeEmail');
		Route::get('/user_profile/change_password_page','UserProfileController@showChangePasswordForm');
		Route::post('/user_profile/change_password','UserProfileController@changePassword');
});

