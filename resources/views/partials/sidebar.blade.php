<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li class=""><a href="/"><i class="fa fa-home"></i> Anasayfa </a></li>
                   
            <li>
            <a><i class="fa fa-sitemap"></i> Dönemsel Tanı Bazlı İşlemler <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                <li><a href="/tig/donemsel_hastane_verileri">Hastane Tig Verileri</a></li>
                <li><a href="/tig/brans_bazli_hastane_emek_verileri">Hastane Branş Bazlı Emek Verileri</a></li>
                <li><a href="/tig/donemsel_hastane_tig_yatis_verileri">Hastane Yatış Bazlı Tig Verileri</a></li>
                <li><a href="/tig/klinik_kodlayici_tig_verileri">Klinik Kodlayıcı Bazlı Tig Verileri</a></li>
                 @role('administrator|owner') 
                    <li><a href="/tig/donemsel_turkiye_geneli_vaka_sayilari">Türkiye Geneli Vaka Sayıları</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_tig_verileri">Türkiye Geneli Tig Verileri</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_drg_bazli_veriler">Türkiye Geneli  Drg Bazlı Veriler</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_klinik_kod_verileri">Türkiye Geneli  Klinik Kod Bazlı Veriler</a></li>
                    <li><a href="/tig/donemsel_anatani_basina_dusen_ektani">Türkiye Geneli  Ana Tanı Başına Düşen İşlem ve Ek Tanı Sayıları</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_islem_hasta_oranlari">Türkiye Geneli  Tekil Vaka  Başına Düşen İşlem Sayıları</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslari">Türkiye Geneli Cinsiyet ve Yaş Frekansları</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslari">Türkiye Geneli Tig Bazlı Cinsiyet ve Yaş Frekansları</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilari">Türkiye Geneli Tanı Bazlı Cinsiyet ve Yaş Frekansları</a></li>
                    <li><a href="/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari">Türkiye Geneli ve Kurum Bazlı  Günübirlik Ve Diğer Yatışlardaki Tig Vaka Frekansları</a></li>
                @endrole
                    {{-- <li><a href="/tig/kurum_bazli_klinik_kodlayici_sayilari">Kurum Bazli Klinik Kodlayıcı Sayıları</a></li> --}}
                </ul>
            </li>

            @role('administrator|owner')
                <li>
                        <a><i class="fa fa-sitemap"></i> Doğum ve Yenidoğan Verileri <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/dogum/dogum_frekans_verileri">Doğum Frekansları</a></li>
                                <li><a href="/dogum/normal_sezaryen_dogum_anatani_ektani_verileri">Normal ve Sezaryen Doğum Ana Tanı ve Ek Tanı Verileri </a></li>
                                <li><a href="/dogum/yd_kilo_verileri">Yeni Doğan Kilo Verileri</a></li>
                            </ul>
                </li>
            @endrole

                <li>
                        <a><i class="fa fa-sitemap"></i> Oecd Verileri <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/tig/oecd_donemsel_hastane_bazli_veriler">Hastane Bazlı Klinik Veriler</a></li>
                            </ul>
                </li>
            
            @role('gsmember')
            <li class=""><a href="/kullanici_yonetimi"><i class="fa fa-user"></i>Kullanıcı Yönetimi</a></li>
            @endrole

            @role('owner')
            <li><a><i class="fa fa-sitemap"></i> Kds Yönetim İşlemleri <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/kullanici_yonetimi">Kullanıcı Yönetimi</a></li>
                    <li><a href="/kurum_yonetimi">Kurum Yönetimi</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-sitemap"></i>Tig Maliyet İşlemleri <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/tibbi_gider_tur_kategori">Gider Türü Kategori İşlemleri</a></li>
                    <li><a href="/tibbi_gider_tur">Gider Türü İşlemleri</a></li>
                    <li><a href="/tibbi_gider_maliyet">Gider İşlemleri</a></li>
                    <li><a href="/sut_islem_bilgileri">Sut İşlem Maliyeti</a></li>
                    <li><a href="/sut_islem_ortalama_index">Sut İşlem Türkiye Ortalamaları</a></li>
                    <li><a href="/sut_ek2c_islemleri">Sut Ek-2C İşlem Maliyet</a></li>
                    <li><a href="/sut_ek2c_islem_ortalama_index">Sut Ek-2C Türkiye Ortalamaları</a></li>
                    
                </ul>
            </li>
            @endrole

            @role('GSMember')
            <li>
                <a><i class="fa fa-sitemap"></i>Khb Tig Maliyet İşlemleri <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/tibbi_gider_maliyet">Gider İşlemleri</a></li>
                </ul>
            </li>
            @endrole

            <li>
                <a><i class="fa fa-sitemap"></i> Profil ve Kullanıcı İşlemleri <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class=""><a href="/user_profile"><i class="fa fa-user"></i> Profil </a></li>
                    <li><a href="/user_profile/change_password_page"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Şifre Güncelleme İşlemi</a></li>
                    <li><a href="/user_profile/change_email_page"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Eposta Güncelleme İşlemi</a></li>
                </ul>
            </li>

            


        </ul>
    </div>
</div>