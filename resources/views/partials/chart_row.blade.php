<div class="row">
    <div class="col-md-{{$grid_width}} col-sm-{{$grid_width}} col-xs-{{$grid_width}}">
        <div class="x_panel">
            <div class="x_title">
                <small>{{$title}}</small>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="{{$content_div_id}}" style="width: 100%;height:{{$content_div_height}}px;"></div>
            </div>
        </div>
    </div>
</div>