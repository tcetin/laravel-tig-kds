<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>



            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        @if(Auth::check())
                            {{Auth::user()->first_name}} {{Auth::user()->last_name}} 
                        @endif
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;">Profil</a></li>
                        <li><a href="/kdslogout"><i class="fa fa-sign-out pull-right"></i> Çıkış Yap</a></li>
                    </ul>
                </li>
                <li>
                    <div id="custom-search-input">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Arama Yapınız..." name="country" id="autocomplete" />
                            <span class="input-group-btn">
                               <div id="srch">
                                    <i class="glyphicon glyphicon-search"></i>
                               </div>
                            </span>
                        </div>
                    </div>
                </li>

            </ul>
        </nav>
    </div>
</div>