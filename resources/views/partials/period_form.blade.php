<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <small>Dönem Aralığı</small>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <form  data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    {{csrf_field()}}

                    <div class="form-group">
                        <div class="range_year"></div>
                        <div class="range_month"></div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-1 col-sm-1 col-xs-3 col-md-offset-11">
                            <button type="button" data-type="{{$data_type}}" class="btn btn-success btn_period">Tamam</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>