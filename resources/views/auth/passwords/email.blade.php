@extends('guest_app')

<!-- Main Content -->
@section('content')
                    <div class="row">

                        <!-- main start -->
                        <!-- ================ -->
                        <div class="main col-md-offset-3 col-md-6 col-md-offset-3">

                            <!-- page-title start -->
                            <!-- ================ -->
                            <h1 class="page-title">Şifre Yenileme İşlemi</h1>
                            <!-- page-title end -->
                                 @if (session('status'))
                                    <div class="alert alert-success" id="MessageSent">
                                        Şifre resetleme linki mailinize gönderildi.Maildeki resetleme linkine tıklayarak şifrenizi resetleyebilirsiniz.
                                    </div>
                                @else
                                   <div class="alert alert-danger {{isset($_POST['reset_password']) == false ? 'hidden' : ''}}" id="MessageNotSent">
                                        Hata oluştu.Şifre resetleme linki gönderilemedi.
                                    </div>
                                @endif
                            <div class="contact-form">
                                <form id="contact-form-with-recaptcha" role="form" method="POST" action="{{ url('/password/email') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label for="username">Kullanıcı Adı*</label>
                                        <input type="text" class="form-control" id="username" name="username" required>
                                        <i class="fa fa-user form-control-feedback"></i>
                                         @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>Böyle bir kullanıcı adı sistemde kayıtlı değildir.</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">Eposta*</label>
                                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                                        <i class="fa fa-envelope form-control-feedback"></i>
                                       @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>Böyle bir eposta adresi sistemde kayıtlı değildir.</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <input type="submit" value="Gönder" name="reset_password" class="submit-button btn btn-default">
                                </form>
                            </div>
                        </div>
                        <!-- main end -->

                    </div>
@endsection
