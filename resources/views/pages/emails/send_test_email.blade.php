@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
    	<style type="text/css">
    		.logo{
    			max-width: 100px;
    			max-height: 100px;
    		}
    	</style>
        <tr>
            <td class="title">
                Tig Karar Destek Sistemine Hoşgeldiniz!
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">
                Test epostasıdır.Dikkate almayınız.
            </td>
        </tr>
        <tr>
            <td width="100%" height="25">
            	<p>Sağlıklı Günler Dileriz!</p>
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop