@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Tig Bazlı Cinsiyet ve Yaş Frekansları</h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'tig_bazli_cinsiyet_yas_frekans'))

    @include("partials.chart_row",array('grid_width'=>'12','title'=>'Tig(Drg) Bazlı Cinsiyet ve Yaş Frekansları Grafiksel Gösterimi (En Son Dönem)','content_div_id'=>'tig_bazli_cinsiyet_yas_frekans','content_div_height'=>'500'))

        <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="tig_bazli_cinsiyet_yas_frekans_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslarini_hesapla">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="Drg" data-filter-control="input">Drg Kodu</th>
                <th data-field="Tanim" data-filter-control="input">Drg Tanım</th>
                <th data-field="Cinsiyet" data-filter-control="select">Cinsiyet</th>
                <th data-field="y_0_4">0-4</th>
                <th data-field="y_5_9">5-9</th>
                <th data-field="y_10_14">10-14</th>
                <th data-field="y_15_19">15-19</th>
                <th data-field="y_20_24">20-24</th>
                <th data-field="y_25_29">25-29</th>
                <th data-field="y_30_34">30-34</th>
                <th data-field="y_35_39">35-39</th>
                <th data-field="y_40_44">40-44</th>
                <th data-field="y_45_49">45-49</th>
                <th data-field="y_50_54">50-54</th>
                <th data-field="y_55_59">55-59</th>
                <th data-field="y_60_64">60-64</th>
                <th data-field="y_65_69">65-69</th>
                <th data-field="y_70_74">70-74</th>
                <th data-field="y_75_79">75-79</th>
                <th data-field="y_80_84">80-84</th>
                <th data-field="y_85_">85+</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>



@endsection