@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Hastane Bazlı Drg Sayıları </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'hastane_bazli_drg_say'))

    <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="drg_hospitals_table" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/tig/donemsel_hastanebazli_drg_sayilarini_getir">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="select">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane Adı</th>
                <th data-field="DRG" data-filter-control="select">Drg</th>
                <th data-field="TANIM" data-filter-control="input">Drg Tanım</th>
                <th data-field="BAGIL" data-sortable="true">Drg Bağıl</th>
                <th data-field="DrgCount" data-sortable="true">Drg Sayısı</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection