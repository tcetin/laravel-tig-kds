@extends('app')
@section('content')
<div class="row">
    {{--  <div class="col-sm-12">
            <div class="wizard-container" style="padding-top:0 !important">

                <div class="card wizard-card" data-color="azure" id="wizardProfile" >
                    <form action="" method="">
                        {{csrf_field()}}
                        <div class="wizard-header text-center">
                            <h3 class="wizard-title">Tig Dinamik Raporlama</h3>
                            <p class="category"></p>
                        </div>

                        <div class="wizard-navigation">
                            <div class="progress-with-circle">
                                 <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                            </div>
                            <ul>
                                <li>
                                   
                                    <a href="#donem" data-toggle="tab">
                                        <div class="icon-circle">
                                            <i class="ti-time"></i>
                                        </div>
                                        Dönem
                                    </a>
                                </li>
                                <li>
                                   
                                    <a href="#kurum" data-toggle="tab">
                                        <div class="icon-circle">
                                            <i class="ti-map-alt"></i>
                                        </div>
                                        Kurum
                                    </a>
                                </li>
                                <li>
                                    
                                    <a href="#vaka" data-toggle="tab">
                                        <div class="icon-circle">
                                            <i class="ti-pulse"></i>
                                        </div>
                                        Yatış Şekli
                                    </a>
                                </li>
                                <li>
                                    
                                    <a href="#brans" data-toggle="tab">
                                         <div class="icon-circle">
                                            <i class="ti-pulse"></i>
                                        </div>
                                        Branş
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane" id="donem">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <small>Dönem Aralığı</small>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                    <div class="form-group">
                                                        <div class="range_year"></div>
                                                        <div class="range_month"></div>
                                                    </div>
                                                    <div class="ln_solid"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kurum">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox" data-type="kurum_tur">
                                                <input type="checkbox" name="kurum[]" value="D">
                                                <div class="card card-checkboxes card-hover-effect">
                                                    <i class="ti-check-box"></i>
                                                    <p>Kamu</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox" data-type="kurum_tur">
                                                <input type="checkbox" name="kurum[]" value="O">
                                                <div class="card card-checkboxes card-hover-effect">
                                                    <i class="ti-check-box"></i>
                                                    <p>Özel</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox" data-type="kurum_tur">
                                                <input type="checkbox" name="kurum[]" value="U">
                                                <div class="card card-checkboxes card-hover-effect">
                                                    <i class="ti-check-box"></i>
                                                    <p>Üniversite</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin-left:227px;">
                                       <div class="input-group">
                                            <span class="input-group-addon">
                                                    <div class="choice" data-toggle="wizard-checkbox" data-type="hastane_list"  style="margin-top:0px !important">
                                                        <div class="card card-checkboxes" style="padding:0 !important;margin-bottom:0px !important;">
                                                            <i class="ti-check-box" style="font-size:20px !important;line-height:0px !important"></i>
                                                        </div>
                                                    </div>
                                            </span>
                                            <select name="dinamik_hastane" id="dinamik_hastane" class="form-control" multiple></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="vaka">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <div class="col-sm-4">
                                            <div class="choice" data-type="vaka">
                                                <input type="checkbox" name="vaka_sekli">
                                                <div class="card card-checkboxes card-hover-effect">
                                                    <i class="ti-check-box"></i>
                                                    <p>Tekil Bazlı Yatışlar</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-type="vaka">
                                                <input type="checkbox" name="vaka_sekli">
                                                <div class="card card-checkboxes card-hover-effect">
                                                    <i class="ti-check-box"></i>
                                                    <p>Tüm Yatışlar</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kurum">
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <div class="col-sm-4">
                                                <div class="choice" data-toggle="wizard-checkbox" data-type="kurum_tur">
                                                    <input type="checkbox" name="kurum[]" value="D">
                                                    <div class="card card-checkboxes card-hover-effect">
                                                        <i class="ti-check-box"></i>
                                                        <p>Kamu</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="choice" data-toggle="wizard-checkbox" data-type="kurum_tur">
                                                    <input type="checkbox" name="kurum[]" value="O">
                                                    <div class="card card-checkboxes card-hover-effect">
                                                        <i class="ti-check-box"></i>
                                                        <p>Özel</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="choice" data-toggle="wizard-checkbox" data-type="kurum_tur">
                                                    <input type="checkbox" name="kurum[]" value="U">
                                                    <div class="card card-checkboxes card-hover-effect">
                                                        <i class="ti-check-box"></i>
                                                        <p>Üniversite</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                           <div class="input-group">
                                                <span class="input-group-addon">
                                                        <div class="choice" data-toggle="wizard-checkbox" data-type="brans_list"  style="margin-top:0px !important">
                                                            <div class="card card-checkboxes" style="padding:0 !important;margin-bottom:0px !important;">
                                                                <i class="ti-check-box" style="font-size:20px !important;line-height:0px !important"></i>
                                                            </div>
                                                        </div>
                                                </span>
                                                <select name="dinamik_brans" id="dinamik_brans" class="form-control" multiple></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Sonraki' />
                                <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Tamam' />
                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Önceki' />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div> <!-- wizard container -->
    </div>  --}}

<div id="app">
    <dinamik-rapor-form></dinamik-rapor-form>
</div>
</div>

@endsection