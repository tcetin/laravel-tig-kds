@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Branş Bazlı Hastane Emek Verileri </h3>
    </div>
    <div class="clearfix"></div>
    @include("partials.period_form",array('data_type'=>'brans_bazli_hastane_emek_verileri'))


    <div class="fresh-table full-screen-table toolbar-color-azure">

        <table id="brans_emek_hastane_verileri" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="CityName" data-filter-control="select">İl</th>
                <th data-field="HospitalCode" data-sortable="true"  data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane Adı</th>
                <th data-field="Brans" data-filter-control="input">Branş</th>
                <th data-sortable="true" data-field="YatisTigCesitliligi">Yatış Tig Çeşitliliği</th>
                <th data-sortable="true" data-field="YatisTigFrekans">Yatış Tig Frekansı</th>
                <th data-sortable="true" data-field="YatisTigBagil">Emek Yatış Tig Toplam Bağılı</th>
                <th data-sortable="true" data-field="Vki">Emek Vaka Karma İndeksi</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>


@endsection