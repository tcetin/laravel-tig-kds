@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Hastane Tig Verileri </h3>
    </div>
    <div class="clearfix"></div>
    @include("partials.period_form",array('data_type'=>'donemsel_hastane_verileri'))


    <div class="fresh-table full-screen-table toolbar-color-azure">

        <table id="hastane_verileri" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="CityName" data-filter-control="select">İl</th>
                <th data-field="HospitalCode" data-sortable="true"  data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane Adı</th>
                <th data-field="HospitalStatu" data-filter-control="input">Hastane Türü</th>
                <th data-sortable="true" data-field="YatisTigCesitliligi">Yatış Tig Çeşitliliği</th>
                <th data-sortable="true" data-field="YatisTigFrekans">Yatış Tig Frekansı</th>
                <th data-sortable="true" data-field="GunuBirlikYatisTigFrekans">Günü Birlik Yatış Tig Frekansı</th>
                <th data-sortable="true" data-field="YatisTigGunuBirlikBagil">Yatış Tig Günü Birlik Toplam Bağılı</th>
                <th data-sortable="true" data-field="YatisTigBagil">Yatış Tig Toplam Bağılı</th>
                <th data-sortable="true" data-field="Vki">Vaka Karma İndeksi</th>
                <th data-sortable="true" data-field="OrtalamayatisGunu">Ortalama Yatış Gün Süresi</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>


@endsection