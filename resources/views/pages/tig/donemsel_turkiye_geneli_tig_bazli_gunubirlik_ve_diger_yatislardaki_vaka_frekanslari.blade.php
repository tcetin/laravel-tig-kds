@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Tig Bazlı Günübirlik ve Diğer Yatışlardaki Vaka Frekansları </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'tig_bazli_gunubirlik_ve_diger'))

    @include("partials.chart_row",array('grid_width'=>'12','title'=>'Tig(Drg) Bazlı Günübirlik ve Diğer Yatışlardaki Vaka Frekansları Grafiksel Gösterimi','content_div_id'=>'tig_bazli_gunubirlik_ve_diger_vaka_frekans','content_div_height'=>'500'))

        <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="tig_bazli_gunubirlik_ve_diger_yatis_frekans_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari_hesapla">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane</th>
                <th data-field="Drg" data-filter-control="input">Drg Kodu</th>
                <th data-field="Tanim" data-filter-control="input">Drg Tanım</th>
                <th data-sortable="true" data-field="GunubirlikVakaFrekans">Günübirlik Yatışlardaki Vaka Frekansı (0-1 Günler)</th>
                <th data-sortable="true" data-field="DigerVakaFrekans">Diğer Yatışlardaki Vaka Frekansı (1> Günler)</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>



@endsection