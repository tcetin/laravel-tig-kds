@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Klinik Kod Verileri </h3>
    </div>
    <div class="clearfix"></div>
    @include("partials.period_form",array('data_type'=>'donemsel_turkiye_geneli_klinik_kod_verileri'))

    <div class="fresh-table full-screen-table toolbar-color-azure">

        <table id="klinik_tbl" class="table table-bordered fresh-table"
               data-toggle="table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-show-loading="false"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="PeriodId">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-sortable="true" data-field="Code" data-filter-control="input">Kod</th>
                <th data-sortable="true" data-field="CodeType" data-filter-control="select">Kod Tipi</th>
                <th data-sortable="true" data-field="Sayi">Sayı</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection