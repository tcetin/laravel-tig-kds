@extends('app')
@section('content')
    <div class="title_left">
        <h3>Dönemsel Hastane Bazlı Klinik Veri</h3>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <small>Dönem Aralığı</small>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form  data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="range_year"></div>
                            <div class="range_month"></div>
                            <div class="x_title">
                                <small>Hastane</small>
                                <div class="clearfix"></div>
                            </div>
                            <div>
                                <select name="hospitals" id="hospitals" class="form-control">
                                    @foreach($hospitals as $hospital)
                                        <option value="{{$hospital->HospitalCode}}">
                                            {{$hospital->HospitalCode}}-{{$hospital->HospitalName}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-1 col-sm-1 col-xs-3 col-md-offset-11">
                                <button type="button" data-type="oecd_donemsel_hastane_bazli_klinik_veri" class="btn btn-success btn_period">Tamam</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="fresh-table full-screen-table toolbar-color-azure">
        <div id="toolbar">
            <div class="form-inline" role="form">
                <div class="form-group">
                    <a type="button" class="btn btn-default" href="/tig/oecd_donemsel_hastane_bazli_verileri_getir?export=true" id="export_oecd" style="margin-bottom: -43px">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Tüm Kayıtları Excel'e Aktar
                    </a>
                </div>
            </div>
        </div>
        <table id="oecd_hastane_klinik_verileri" 
        	   class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-sortable="true"
               data-minimum-count-columns="2"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-pagination="true"
               data-side-pagination="server"
               data-url="/tig/oecd_donemsel_hastane_bazli_verileri_getir">
            <thead>
            <tr>        <th data-field="DONEM_KODU" data-filter-control="input" data-sortable="true">Dönem Kodu</th>
                        <th data-field="DONEM" data-filter-control="input">Veri Dönemi</th>
                        <th data-field="HASTANE_KODU" data-sortable="true"  data-filter-control="input">Hastane Kodu</th>
                        <th data-field="HASTANE_ADI">Hastane Adı</th>
                        <th data-field="YATIS_NO" data-sortable="true" data-filter-control="input">Yatış No</th>
        				<th data-field="HASTA_YAS" data-sortable="true">Yaş</th>
        				<th data-field="CINSIYET">Cinsiyet</th>
                        <th data-field="BRANS">Branş</th>
        				<th data-field="YATIS_SEKLI">Yatış Şekli</th>
        				<th data-field="CIKIS_SEKLI">Çıkış Şekli</th>
                        <th data-field="YATIS_TARIHI">Yatış Tarihi</th>
                        <th data-field="CIKIS_TARIHI">Çıkış Tarihi</th>
                        <th data-field="YATIS_GUNU" data-sortable="true">Yatış Günü</th>
                        <th data-field="YB_YATIS_GUNU" data-sortable="true">Yoğun Bakım Yatış Günü</th>
        				<th data-field="DRG_KODU" data-filter-control="input">Drg Kodu</th>
        				<th data-field="DRG_KODU_ACIKLAMA">Drg Kodu Açıklama</th>
        				<th data-field="DRG_BAGIL" data-sortable="true">Drg Bağıl</th>
        				<th data-field="TANI_KODU" data-filter-control="input">Tanı Kodu</th>
        				<th data-field="TANI_KODU_ACIKLAMA">Tanı Kodu Açıklama</th>
        				<th data-field="TANI_KODU_SEKLI">Tanı Kodu Şekli</th>
                        <th data-field="TANI_KODU_TIPI">Tanı Kodu Tipi</th>
                        <th data-field="ISLEM_ZAMANI">İşlem Zamanı(Saat)</th>
                        <th data-field="KODLAYICI">Klinik Kodlayıcı</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection