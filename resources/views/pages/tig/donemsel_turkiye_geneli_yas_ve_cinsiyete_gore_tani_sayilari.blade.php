@extends('app')
@section('content')
     <style type="text/css">
       .dc-chart g.row text {fill: #73879C;}
     </style>
    <div class="title_left">
        <h3> Dönem Bazlı Türkiye Geneli Yaş ve Cinsiyete Bağlı Tanı Sayıları </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'tani_yas_cinsiyet'))

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="x_panel">
                  <div class="x_title">
                      <small>Tanı Tipi Bazında Vaka Sayıları <span class="badge total_say"></span></small>
                      <ul class="nav navbar-right panel_toolbox">
                         <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                         <li><a class="reset" href="javascript:taniTipChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                          <li><a href="#" class="download-chart" chart-dim="tani_tip"><i class="fa fa-download fa-2x"></i></a></li>

                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div id="tani_tip" style="width: 100%;height:500px;">
                          <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                          <div class="clearfix"></div>
                      </div>
                  </div>
              </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="x_panel">
                  <div class="x_title">
                      <small>Cinsiyet Bazında Vaka Sayıları <span class="badge total_say"></span></small>
                      <ul class="nav navbar-right panel_toolbox">
                         <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                         <li><a class="reset" href="javascript:taniCinsiyetChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                          <li><a href="#" class="download-chart" chart-dim="tani_cinsiyet"><i class="fa fa-download fa-2x"></i></a></li>

                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div id="tani_cinsiyet" style="width: 100%;height:500px;">
                          <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                          <div class="clearfix"></div>
                      </div>
                  </div>
              </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  <div class="x_title">
                      <small>Yaş Bazında Vaka Sayıları <span class="badge total_say"></span></small>
                      <ul class="nav navbar-right panel_toolbox">
                         <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                         <li><a class="reset" href="javascript:taniYasChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                          <li><a href="#" class="download-chart" chart-dim="tani_yas"><i class="fa fa-download fa-2x"></i></a></li>

                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div id="tani_yas" style="width: 100%;height:500px;">
                          <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                          <div class="clearfix"></div>
                      </div>
                  </div>
          </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  <div class="x_title">
                      <small>Tanı Bazında Vaka Sayıları <span class="badge total_say"></span></small>
                      <ul class="nav navbar-right panel_toolbox">
                         <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                         <li><a class="reset" href="javascript:taniChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                          <li><a href="#" class="download-chart" chart-dim="tani"><i class="fa fa-download fa-2x"></i></a></li>

                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div id="tani" style="width: 100%;height:500px;overflow-y: scroll">
                          <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                          <div class="clearfix"></div> 
                      </div>
                  </div>
          </div>
      </div>
    </div>

    <div class="fresh-table full-screen-table toolbar-color-azure">

        <table id="tani_yas_cinsiyet_tbl" class="table table-bordered fresh-table"
               data-toggle="table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-show-loading="false"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilarini_hesapla">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-sortable="true" data-field="TaniKod" data-filter-control="input">Kod</th>
                <th data-sortable="true" data-field="TaniTip" data-filter-control="select">Kod Tipi</th>
                <th data-sortable="true" data-field="TANIM" data-filter-control="input">Tanım</th>
                <th data-sortable="true" data-field="Cinsiyet" data-filter-control="select">Cinsiyet</th>
                <th data-sortable="true" data-field="YasAralik" data-filter-control="input">Yaş Aralığı</th>
                <th data-sortable="true" data-field="Say">Sayı</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection