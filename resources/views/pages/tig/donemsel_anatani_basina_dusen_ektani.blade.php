@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Ana Tanı Başına Düşen Ek Tanı Sayıları </h3>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <small>Dönem Aralığı</small>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form  data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="range_year"></div>
                            <div class="range_month"></div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                           <div class="class="col-md-9 col-sm-9 col-xs-9 col-md-offset-3">  
                                <input 
                                type="text" class="form-control kod-listesi" 
                                name="kod" id="klinik_kod_anatani" 
                                placeholder="ICD Kodunu Giriniz..."
                                style="min-width: 485px" />
                           </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1 col-sm-1 col-xs-3 col-md-offset-11">
                                <button type="button" data-type="ana_tani_basina_dusen_ektani" class="btn btn-success btn_period">Tamam</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="ana_tani_basina_dusen_ek_tani_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="PeriodId">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="Code" data-filter-control="input">Ana Tanı</th>
                <th data-sortable="true" data-field="EkTaniSay">Ek Tanı (ICD)</th>
                <th data-sortable="true" data-field="IslemSay">İşlem</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection