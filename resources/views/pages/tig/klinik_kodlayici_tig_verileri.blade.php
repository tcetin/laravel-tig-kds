@extends('app')
@section('content')
    <div class="title_left">
        <h3>Klinik Kodlayıcı Tig Verileri </h3>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <small>Dönem Aralığı</small>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form  data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="range_year"></div>
                            <div class="range_month"></div>
                            <div class="x_title">
                                <small>Hastane</small>
                                <div class="clearfix"></div>
                            </div>
                            <div>
                                <select name="hospitals" id="hospitals" class="form-control">
                                    @foreach($hospitals as $hospital)
                                        <option value="{{$hospital->HospitalCode}}">{{$hospital->HospitalCode}}-{{$hospital->HospitalName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-1 col-sm-1 col-xs-3 col-md-offset-11">
                                <button type="button" data-type="kodlayici_tig_verileri" class="btn btn-success btn_period">Tamam</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="fresh-table full-screen-table toolbar-color-azure">

        <table id="kodlayici_tig_verileri" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="DonemYili" data-filter-control="select">Dönem Yılı</th>
                <th data-field="HospitalCode" data-sortable="true"  data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane Adı</th>
                <th data-field="AdSoyad" data-filter-control="input">AdSoyad</th>
                <th data-sortable="true" data-field="TigFrekans">Yatış Tig Frekansı</th>
                <th data-sortable="true" data-field="TigBagilToplam">Yatış Tig Toplam Bağılı</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>


@endsection