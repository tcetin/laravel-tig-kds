@extends('app')
@section('content')
    <div class="title_left">
        <h3> Kurum Bazlı Klinik Kodlayıcı Sayıları </h3>
    </div>
    <div class="clearfix"></div>

    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="kurum_bazli_klinik_kodlayici_sayilari_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-show-pagination-switch="true"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="HastaneKodu">Hastane Kodu</th>
                <th data-field="HastaneAdi" data-filter-control="input">Hastane Adi</th>
                <th data-field="Il" data-filter-control="input">İl</th>
                <th data-sortable="true" data-field="Aktif">Aktif Kodlayıcı</th>
                <th data-sortable="true" data-field="Pasif">Pasif Kodlayıcı</th>
            </tr>
            </thead>
            <tbody>
                @foreach($datas as $data)
                    <tr>
                        <td>{{$data->HastaneKodu}}</td>
                        <td>{{$data->HastaneAdi}}</td>
                        <td>{{$data->Il}}</td>
                        <td>{{$data->Aktif}}</td>
                        <td>{{$data->Pasif}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection