@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Hastanelerdeki Yatış Bazlı Drg Verileri </h3>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <small>Dönem Aralığı</small>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form  data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="range_year"></div>
                            <div class="range_month"></div>
                            <div class="x_title">
                                <small>Hastane</small>
                                <div class="clearfix"></div>
                            </div>
                            <div>
                                <select name="hospitals" id="hospitals" class="form-control" multiple>
                                    @foreach($hospitals as $hospital)
                                        <option value="{{$hospital->HospitalCode}}">{{$hospital->HospitalCode}}-{{$hospital->HospitalName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-1 col-sm-1 col-xs-3 col-md-offset-11">
                                <button type="button" data-type="donemsel_hastane_tig_yatis_verileri" class="btn btn-success btn_period">Tamam</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="donemsel_hastane_tig_yatis_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-field="DonemAdi" data-filter-control="input">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="input">Hastane Kodu</th>
                <th data-field="Hastane" data-filter-control="input">Hastane</th>
                <th data-field="YatisNo" data-filter-control="input">Yatış No</th>
                <th data-field="YatisTarihi">Yatış Tarihi</th>
                <th data-field="TaburcuTarihi">Taburcu Tarihi</th>
                <th data-field="YbYatisGun">Yoğunbakım Yatış Gün Sayısı</th>
                <th data-field="Cinsiyet" data-filter-control="select">Cinsiyet</th>
                <th data-field="TaburcuSekli" data-filter-control="select">TaburcuSekli</th>
                <th data-field="SosyalGuvence" data-filter-control="select">Sosyal Güvence</th>
                <th data-field="Tig" data-filter-control="input">Drg Kodu</th>
                <th data-field="TigBagil" data-filter-control="input">Drg Bağıl Değeri</th>
                <th data-field="TigAciklama" data-filter-control="input">Drg Açıklama</th>
                <th data-field="MDC" data-filter-control="input">MTS</th>
                <th data-field="MTSAciklama" data-filter-control="input">MTS Açıklama</th>
                <th data-field="TakipNo" data-filter-control="input">Takip No</th>
                <th data-field="DrTcKimlik">Hekim TC No</th>
                <th data-field="KayitTarihi" data-filter-control="input">Kayıt Tarihi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>



@endsection