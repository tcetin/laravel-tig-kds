@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Tig Verileri </h3>
    </div>

    <div class="clearfix"></div>
    @include("partials.period_form",array('data_type'=>'donemsel_turkiye_geneli_tig_verileri'))

    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="genel_tig_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi">Dönemi</th>
                <th data-sortable="true" data-field="YatisTigCesitliligi">Yatış Tig Çeşitliliği</th>
                <th data-sortable="true" data-field="YatisTigFrekans">Yatış Tig Frekansı</th>
                <th data-sortable="true" data-field="YatisTigBagil">Yatış Tig Toplam Bağılı</th>
                <th data-sortable="true" data-field="OrtalamayatisGunu">Ortalama Yatış Gün Süresi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection