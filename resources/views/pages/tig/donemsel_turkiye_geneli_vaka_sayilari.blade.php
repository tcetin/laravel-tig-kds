@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Dönem Bazlı Vaka Sayıları</h3>
    </div>

    @include("partials.period_form",array('data_type'=>'tr_vaka_sayilari'))

    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="vaka_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false">
            <thead>
            <tr>
                <th data-field="DonemAdi" data-filter-control="input">Dönemi</th>
                <th data-field="Vaka">Toplam Vaka</th>
                <th data-field="TekilVaka">Toplam Tekil Vaka</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection