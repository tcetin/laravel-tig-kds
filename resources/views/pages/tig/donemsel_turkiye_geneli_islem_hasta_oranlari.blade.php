@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Tekil Vaka Başına Düşen İşlem Sayıları </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'islem_hasta_oran'))

    @include("partials.chart_row",array('grid_width'=>'12','title'=>'Tekil Vaka Başına Düşen İşlem Sayıları Grafiksel Gösterimi','content_div_id'=>'turkiye_geneli_islem_hasta_oranlari','content_div_height'=>'400'))

    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="islem_hasta_oran_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="IslemSayisi">İşlem Sayısı</th>
                <th data-field="TekilVakaSayisi">Tekil Vaka Sayısı</th>
                <th data-field="IslemHastaOrani" data-sortable="true">İşlem Hasta Oranı</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection