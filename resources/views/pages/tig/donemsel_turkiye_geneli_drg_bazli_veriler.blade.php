@extends('app')
@section('content')
    <div class="title_left">
        <h3> Dönemsel Türkiye Geneli Hastane Bazlı Dönemsel Drg Bazlı Veriler </h3>
    </div>

    <div class="clearfix"></div>
    @include("partials.period_form",array('data_type'=>'donemsel_turkiye_geneli_drg_verileri'))

    <div class="fresh-table full-screen-table toolbar-color-azure">

        <table id="drg_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true" 
               data-id-field="id"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane</th>
                <th data-field="Statu" data-filter-control="select">Hastane Statu</th>
                <th data-field="Drg" data-filter-control="select">Drg</th>
                <th data-field="Tanim" data-filter-control="input">Tanım</th>
                <th data-sortable="true" data-field="YatisTigFrekans">Yatış Tig Frekansı</th>
                <th data-sortable="true" data-field="YatisTigBagil">Yatış Tig Toplam Bağılı</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection