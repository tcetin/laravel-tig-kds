@extends('app')
@section('content')
<style type="text/css">
  .hide{
    display: none
  }
  .show{
    display: block;
  }
</style>
    <div class="title_left">
        <h3> Kurum Yönetimi </h3>
    </div>
    <div class="clearfix"></div>
    <div class="fresh-table full-screen-table toolbar-color-azure">
        <p class="toolbar">
            <a class="create_hospital btn btn-default" href="javascript:"><i class="fa fa-plus fa-2x" aria-hidden="true"></i> Yeni Kurum</a>
            <a class="update_hospital btn btn-success"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i> Güncelle</a>
            <a class="delete_hospital btn btn-success"><i class="fa fa-trash fa-2x" aria-hidden="true"></i> Sil</a>
            <span class="alert"></span>
        </p>
          @if (count($errors) > 0)
                  <div>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
          @endif
     <table id="kurum_yonetimi_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-click-to-select="true" 
               data-toolbar=".toolbar">
            <thead>
            <tr>
                <th data-checkbox="true"></th>
                <th data-field="hospitalId">Sıra</th>
                <th data-field="HospitalCode" data-filter-control="input">Kurum Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Kurum Adı</th>
                <th data-field="Il" data-filter-control="input">İl</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

      <div id="create_hospital_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Kurum Tanımlama İşlemi</h4>
                </div>
                <div class="modal-body"> 
                    <form id="create_hospital" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                      {{csrf_field()}}
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hospital_code">Kurum Kodu<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="hospital_code" name="hospital_code" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hospital_name">Kurum Adı <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="hospital_name" name="hospital_name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">İl <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="hospital_khb" id="hospital_khb" class="form-control">
                            @foreach($hospitalsKhb as $h)
                            <option value="{{$h->KurumKod}}">{{$h->KurumAd}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kurum Türü<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="hospital_type" id="hospital_type" class="form-control">
                            <option value="D">Kamu</option>
                            <option value="O">Özel</option>
                            <option value="U">Üniversite</option>
                          </select>
                        </div>
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary submit">Kaydet</button>
                    <button type="button" class="btn btn-primary hide submit_update">Güncelle</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection