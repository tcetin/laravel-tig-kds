@extends('app')
@section('content')
    <div class="title_left">
        <h3> Sut İşlemleri Türkiye Ortalamaları </h3>
    </div>
    <div class="clearfix"></div>


    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="sut_islem_ortalamalari" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/sut_islem_ortalama">
            <thead>
            <tr>
                <th data-field="sut_kodu" data-filter-control="input">Sut Kodu</th>
                <th data-field="islem_sayisi" data-sortable="true">İşlem Sayısı(Sayı)</th>
                <th data-field="ortalama_yatis_suresi_ort" data-sortable="true">Ortalama Yatış Süresi</th>
                <th data-field="tibbi_malzeme_tutari_ort" data-sortable="true">Tıbbi Malzeme Tutarı(₺)</th>
                <th data-field="ilac_tutar_ort" data-sortable="true">İlaç Tutarı(₺)</th>
                <th data-field="islem_tutari_ort" data-sortable="true">İşlem Tutarı(₺)</th>
                <th data-field="uzman_hekim_sayisi_ort" data-sortable="true">Uzman Hekim Sayısı</th>
                <th data-field="anestezi_uzmani_sayisi_ort" data-sortable="true">Anestezi Uzman Sayısı</th>
                <th data-field="ysp_sayisi_ort" data-sortable="true">Yardımcı Sağlık Personeli Sayısı</th>
                <th data-field="asistan_hekim_sayisi_ort" data-sortable="true">Asistan Hekim Sayısı</th>
                <th data-field="islem_suresi_dk_ort" data-sortable="true">İşlem Süresi(dk)</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection