@extends('app')
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session()->has('formState'))
            @if(count($errors))
                <div class="form-group">
                    <div class="alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @else
                @if(session()->get('formState')==true)
                <span class="label label-success pull-right"><i class="fa fa-check"></i> İşlem başarılı!</span>
                @endif
            @endif
        @endif
            @if(Session::has('errorMsg'))
            <div class="alert-danger"><p>{{session()->get('errorMsg')}}</p></div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <i class="fa fa-edit"></i> Sut İşlem Girişi
                    <small>
                    <a class="btn btn-default" 
                    href="{{
                        route('sample_excel',array(
                        'name'=>'Sut Örnek Veri Girişi',
                        '_headers'=>['Sut Kodu', 'Islem Adi','Islem Sayisi','Ortalama Yatis Suresi', 'Tibbi Malzeme Tutari', 'Ilac Tutar',
                        'Diger Islemler Tutari', 'Uzman Hekim Sayisi', 'Anestezi Uzmani Sayisi','Yardimci Saglik Personeli Sayisi',
                        'Asistan Hekim Sayisi', 'Islem Suresi'],
                        'path'=>'sut_kodlari.json'
                    ))}}">
                    <i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i> Örnek Excel Dosyasını İndir</a></small>

                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <form enctype="multipart/form-data" method="POST" action="/sut_islem_bilgileri/import_data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Yıl</label>
                                    <select name="yil" id="yil" class="form-control">
                                        <option value="2017" selected>2017</option>
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Hastane</label>
                                    <select name="hastane_kodu" id="hastane_kodu" class="form-control">
                                        @foreach($hospitals as $hospital)
                                         <option value="{{$hospital->HospitalCode}}">{{$hospital->HospitalCode}}--{{$hospital->HospitalName}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-4">    
                            <div class="form-group">
                                    <label>Dosya</label>
                                    <input type="file" class="form-control" name="file">
                            </div>
                        </div>
                        <div class="col-md-2" style="margin-top:5px;">
                            <div class="form-group pull-right">
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Yükle</button>
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                </form>
            </div>
        </div>
    </div>
</div>

    <div class="clearfix"></div>
    <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="sut_islemleri_girisi_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-click-to-select="true"
               data-toolbar=".toolbar">
            <thead>
            <tr>
                <th>Hastane Kodu</th>
                <th>Hastane Adı</th>
                <th>Yıl</th>
                <th>Oluşturulma Zamanı</th>
                <th>En Son Güncellenme Zamanı</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dosya as $item)
                <tr>
                    <td>{{$item->hastane_kodu}}</td>
                    <td>{{$item->hastane_adi}}</td>
                    <td>{{$item->yil}}</td>
                    <td>{{\Carbon\Carbon::parse($item->created_at,"Europe/Istanbul")->diffForHumans()}}</td>
                    <td>{{\Carbon\Carbon::parse($item->updated_at,"Europe/Istanbul")->diffForHumans()}}</td>
                    <td>
                        <a class="btn btn-danger" data-toggle="modal" data-target="#delete_islem_dosya_{{$item->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Sil</a>
                        <div id="delete_islem_dosya_{{$item->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <form class="form-horizontal" method="POST" action="/sut_islem_bilgileri/dosya_sil/{{$item->id}}">
                                    <input name="_method" type="hidden" value="DELETE">
                                    {{csrf_field()}}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sut Dosya Girişini Sil</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h2>Bu sut girişini silmek istediğinize emin misiniz?</h2>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                                            <button class="btn btn-success" type="submit">Sil</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <a class="btn btn-success" href="/sut_islem_bilgileri/export_data/{{$item->id}}"><i class="fa fa-download" aria-hidden="true"></i>İndir</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection