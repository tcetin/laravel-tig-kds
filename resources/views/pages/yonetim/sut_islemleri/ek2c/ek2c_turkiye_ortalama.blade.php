@extends('app')
@section('content')
    <div class="title_left">
        <h3> Sut Ek-2C İşlemleri Türkiye Ortalamaları </h3>
    </div>
    <div class="clearfix" id="app"></div>


    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="sut_islem_ortalamalari_ek2c" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/sut_ek2c_islem_ortalama">
            <thead>
            <tr>
                <th data-field="sut_kodu" data-filter-control="input">Sut Kodu</th>
                <th data-field="hasta_basina_tibbi_malzeme_gideri_paket_ort" data-sortable="true">Hasta Başına Tıbbi Malzeme Gideri(Paket Dahil)</th>
                <th data-field="hasta_basina_tibbi_malzeme_gideri_ort" data-sortable="true">Hasta Başına Tıbbi Malzeme Gideri(Paket Hariç)</th>
                <th data-field="hasta_basina_ilac_gideri_paket_ort" data-sortable="true">Hasta Başına İlaç Gideri(Paket Dahil)</th>
                <th data-field="hasta_basina_ilac_gideri_ort" data-sortable="true">Hasta Başına İlaç Gideri(Paket Hariç)</th>
                <th data-field="hasta_basina_lab_gideri_ort" data-sortable="true">Hasta Başına Laboratuvar Gideri</th>
                <th data-field="hasta_basina_radyoloji_gideri_ort" data-sortable="true">Hasta Başına Radyoloji Gideri</th>
                <th data-field="hasta_basina_diger_gideri_ort" data-sortable="true">Hasta Başına Diğer Gider</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection