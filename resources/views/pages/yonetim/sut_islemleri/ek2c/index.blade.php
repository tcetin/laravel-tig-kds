@extends('app')
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session()->has('formState'))
            @if(count($errors))
                <div class="form-group">
                    <div class="alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @else
                @if(session()->get('formState')==true)
                <span class="label label-success pull-right"><i class="fa fa-check"></i> İşlem başarılı!</span>
                @endif
            @endif
        @endif
            @if(Session::has('errorMsg'))
            <div class="alert-danger"><p>{{session()->get('errorMsg')}}</p></div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> Sut Ek-2C İşlem Girişi<small>
                    <a class="btn btn-default" 
                    href="{{
                        route('sample_excel',array(
                        'name'=>'Sut Ek2C Örnek Veri Girişi',
                        '_headers'=>['Sut Kodu', 'Islem Adi','Hasta Basina Tibbi Malzeme Gideri Paket','Hasta Basina Tibbi Malzeme Gideri',
                        'Hasta Basina Ilac Gideri Paket','Hasta Basina Ilac Gideri','Hasta Basina Lab Gideri','Hasta Basina Radyoloji Gideri',
                        'Hasta Basina Diger Gideri'],
                        'path'=>'sut_kodlari.json'
                    ))}}">
                        <i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i> Örnek Excel Dosyasını İndir
                    </a></small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="app">
                <sut-ek2c-form/>
            </div>
        </div>
    </div>
</div>
@endsection
