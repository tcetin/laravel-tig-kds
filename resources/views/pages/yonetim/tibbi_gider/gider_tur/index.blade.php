@extends('app')
@section('content')
    <style type="text/css">
        .hide{
            display: none
        }
        .show{
            display: block;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('formState'))
                @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @else
                    <span class="label label-success pull-right"><i class="fa fa-check"></i> İşlem başarılı!</span>
                @endif
            @endif
        </div>
    </div>
    <div class="title_left">
        <h3> Tıbbi Gider Türü Yönetimi </h3>
    </div>

    <div class="clearfix"></div>
    <div class="fresh-table full-screen-table toolbar-color-azure">
        <p class="toolbar">
            <a class="btn btn-default" data-toggle="modal" data-target="#create_tur"><i class="fa fa-plus fa-2x" aria-hidden="true"></i> Yeni Gider Türü</a>
            <span class="alert"></span>
        </p>
        <table id="gider_tur_yonetimi_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-click-to-select="true"
               data-toolbar=".toolbar">
            <thead>
            <tr>
                <th>Gider Türü</th>
                <th>Kategori</th>
                <th>Oluşturulma Zamanı</th>
                <th>En Son Güncellenme Zamanı</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            @foreach($giderTur as $tur)
                <tr>
                    <td>{{$tur->gider_tur}}</td>
                    <td>{{$tur->kategori->kategori}}</td>
                    <td>{{\Carbon\Carbon::parse($tur->created_at,"Europe/Istanbul")->diffForHumans()}}</td>
                    <td>{{\Carbon\Carbon::parse($tur->updated_at,"Europe/Istanbul")->diffForHumans()}}</td>
                    <td>
                        <a class="btn btn-danger" data-toggle="modal" data-target="#delete_tur_{{$tur->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Sil</a>
                        <div id="delete_tur_{{$tur->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <form class="form-horizontal" method="POST" action="/tibbi_gider_tur/{{$tur->id}}">
                                    <input name="_method" type="hidden" value="DELETE">
                                    {{csrf_field()}}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Tür Sil</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h2>Bu kaydı silmek istediğinize emin misiniz?</h2>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                                            <button class="btn btn-success" type="submit">Sil</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>

                        <a class="btn btn-success" data-toggle="modal" data-target="#update_tur_{{$tur->id}}"><i class="fa fa-refresh" aria-hidden="true"></i> Güncelle</a>

                        <div id="update_tur_{{$tur->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <form class="form-horizontal" method="POST" action="/tibbi_gider_tur/{{$tur->id}}">
                                    {{csrf_field()}}
                                    {{ method_field('PATCH') }}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Tür Güncelle</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Tür
                                                    <span
                                                            class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="gider_tur" name="gider_tur" required="required"
                                                           class="form-control col-md-10 col-xs-12"
                                                           value="{{$tur->gider_tur}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Tür Kategori
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="kategori_id" id="kategori_id" class="form-control">
                                                        @foreach(\App\TibbiGiderTurKategori::all() as $kategori)
                                                            <option value="{{$kategori->id}}" {{$tur->kategori->id==$kategori->id ? 'selected' : ''}}>{{$kategori->kategori}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                                            <button class="btn btn-success" type="submit">Güncelle</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- Modal -->

    <div id="create_tur" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form class="form-horizontal" method="POST" action="/tibbi_gider_tur">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tür Ekle</h4>
                    </div>
                    <div class="modal-body">

                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Tür <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="gider_tur" name="gider_tur" required="required"
                                       class="form-control col-md-10 col-xs-12" placeholder="Gider türü">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Tür Kategori
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="kategori_id" id="kategori_id" class="form-control">

                                    @foreach(\App\TibbiGiderTurKategori::all() as $kategori)
                                        <option value="{{$kategori->id}}">{{$kategori->kategori}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                        <button class="btn btn-success" type="submit">Kaydet</button>
                    </div>
            </form>
        </div>
        </div>
        </div>
@endsection