@extends('app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('formState'))
                @if(count($errors))
                    <div class="form-group">
                        <div class="alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @else
                    @if(session()->get('formState')==true)
                    <span class="label label-success pull-right"><i class="fa fa-check"></i> İşlem başarılı!</span>
                    @endif
                @endif
            @endif
                @if(Session::has('errorMsg'))
                <div class="alert-danger"><p>{{session()->get('errorMsg')}}</p></div>
            @endif
        </div>
    </div>

  <div class="row">
      <div class="col-md-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <h2>Tig Gider Veri Aktarımı<small><a class="btn btn-default" href="/tibbi_gider_maliyet/export_sample_excel"><i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i> Örnek Excel Dosyasını İndir</a></small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <br>
                  <form class="form-inline form-label-left input_mask" enctype="multipart/form-data" method="POST" action="/tibbi_gider_maliyet/import_data">
                      {{csrf_field()}}
                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Dönem</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <select name="period" id="period" class="form-control">
                                  @foreach($periods as $period)
                                  <option value="{{$period->Id}}">{{$period->DonemAdi}}</option>
                                  @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Gider Türü</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <select name="gider_tur" id="gider_tur" class="form-control">
                                  @foreach($giderTurKategori as $kategori)
                                  <optgroup label="{{$kategori->kategori}}">
                                      @foreach($kategori->giderTur as $tur)
                                          <option value="{{$tur->id}}">{{$tur->gider_tur}}</option>
                                      @endforeach
                                  </optgroup >
                                  @endforeach
                              </select>
                          </div>

                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Dosya</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="file" class="form-control" name="file">
                          </div>
                      </div>
                      <div class="form-group pull-right">
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <button type="submit" class="btn btn-success"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Yükle</button>
                          </div>
                      </div>
                      <div class="ln_solid"></div>

                  </form>
              </div>
          </div>
      </div>
  </div>

    <div class="clearfix"></div>
    <div class="fresh-table full-screen-table toolbar-color-azure">
        <p class="toolbar"></p>
        <table id="tibbi_gider_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-click-to-select="true"
               data-toolbar=".toolbar">
            <thead>
            <tr>
                <th data-field="DonemAdi" data-filter-control="select">Dönem</th>
                <th data-field="hastane_kodu" data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane Adı</th>
                <th data-field="yatis_no" data-filter-control="input">Yatış No</th>
                <th data-field="drg" data-filter-control="input">Tig Kodu</th>
                <th data-field="TANIM" data-filter-control="input">Tig Açıklama</th>
                <th data-field="gider_tur" data-filter-control="input">Gider Türü</th>
                <th data-field="miktar" data-filter-control="input">Miktar(TL)</th>
                <th>Eklenme Zamanı</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
                @foreach($gider as $item)
                    <tr>
                        <td>{{$item->donem->DonemAdi}}</td>
                        <td>{{$item->hastane_kodu}}</td>
                        <td>{{$item->hospital->HospitalName}}</td>
                        <td>{{$item->yatis_no}}</td>
                        <td>{{$item->drg}}</td>
                        <td>{{$item->tigKodu->TANIM}}</td>
                        <td>{{$item->giderTur->gider_tur}}</td>
                        <td>{{$item->miktar}}</td>
                        <td>{{\Carbon\Carbon::parse($item->created_at,"Europe/Istanbul")->diffForHumans()}}</td>
                        <td>
                            <a class="btn btn-danger" data-toggle="modal" data-target="#delete_gider_{{$item->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Sil</a>
                            <div id="delete_gider_{{$item->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <form class="form-horizontal" method="POST" action="/tibbi_gider_maliyet/delete/{{$item->id}}">
                                        <input name="_method" type="hidden" value="DELETE">
                                        {{csrf_field()}}
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Kayıt Sil</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h2>Bu kaydı silmek istediğinize emin misiniz?</h2>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                                                <button class="btn btn-success" type="submit">Sil</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                            <a class="btn btn-success" data-toggle="modal" data-target="#update_gider_{{$item->id}}"><i class="fa fa-refresh" aria-hidden="true"></i> Güncelle</a>

                            <div id="update_gider_{{$item->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <form class="form-horizontal" method="POST" action="/tibbi_gider_maliyet/update/{{$item->id}}">
                                        {{csrf_field()}}
                                        {{ method_field('PATCH') }}
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Gider Güncelle</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Gider Türü <span
                                                                class="required">*</span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select name="gider_tur" id="gider_tur" class="form-control">
                                                            @foreach($giderTurKategori as $kategori)
                                                                <optgroup label="{{$kategori->kategori}}">
                                                                    @foreach($kategori->giderTur as $tur)
                                                                        <option value="{{$tur->id}}" {{$item->gider_tur == $tur->id ? 'selected' : ''}}>{{$tur->gider_tur}}</option>
                                                                    @endforeach
                                                                </optgroup >
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Miktar <span
                                                                class="required">*</span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="miktar" id="miktar" class="form-control" value="{{$item->miktar}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                                                <button class="btn btn-success" type="submit">Güncelle</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection