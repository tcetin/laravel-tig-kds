@extends('app')
@section('content')
<style type="text/css">
  .hide{
    display: none
  }
  .show{
    display: block;
  }
</style>
    <div class="title_left">
        <h3> Kullanıcı Yönetimi </h3>
    </div>
    <div class="clearfix"></div>
    <div class="fresh-table full-screen-table toolbar-color-azure">
        <p class="toolbar">
            <a class="create btn btn-default" href="javascript:"><i class="fa fa-plus fa-2x" aria-hidden="true"></i> Yeni Kullanıcı</a>
            <a class="update btn btn-success"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i> Güncelle</a>
            <a class="delete btn btn-success"><i class="fa fa-trash fa-2x" aria-hidden="true"></i> Sil</a>
            <span class="alert"></span>
        </p>
          @if (count($errors) > 0)
                  <div>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
          @endif
     <table id="kullanici_yonetimi_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-click-to-select="true" 
               data-toolbar=".toolbar">
            <thead>
            <tr>
                <th data-checkbox="true"></th>
                <th data-field="username" data-filter-control="input">Kullanıcı Adı</th>
                <th data-field="first_name" data-filter-control="input">Ad</th>
                <th data-field="last_name" data-filter-control="input">Soyad</th>
                <th data-field="email" data-filter-control="input">Eposta</th>
                <th data-field="phone" data-filter-control="input">Telefon</th>
                <th data-field="RoleName" data-filter-control="input">Rol</th>
                <th data-field="description" data-filter-control="input">Rol Açıklama</th>
                <th data-field="userHospCode" data-filter-control="input">Kurum Kodu</th>
                <th data-field="userHospitalName" data-filter-control="input">Kurum Adı</th>
                <th data-field="created_at" data-filter-control="input">Oluşturulma Tarihi</th>
                <th data-field="updated_at" data-filter-control="input">Güncellenme Tarihi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

      <div id="create_user_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Yeni Kullanıcı Tanımlama İşlemi</h4>
                </div>
                <div class="modal-body"> 
                    <form id="create_user" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                      {{csrf_field()}}
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">TC Kimlik No <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ad <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" name="first_name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Soyad <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="last-name" name="last_name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Eposta <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="email" class="form-control col-md-7 col-xs-12" type="text" name="email">
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Telefon</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="phone" name="phone" class="form-control col-md-7 col-xs-12" type="text">
                        </div>
                      </div>
                      @role('owner')
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kullanıcı Rolü <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                          <select name="rol" id="rol" class="form-control">
                            <option value="0">--Rol Seçiniz--</option>
                            <option value="hsmember">Hastane Yöneticiliği</option>
                            <option value="gsmember">İl Kullanıcısı</option>
                            <option value="administrator">Yönetici</option>
                            <option value="owner">KDS Yöneticisi</option>
                          </select>
                        </div>
                      </div>
                      <div id="div_hst" class="form-group hide">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sağlık Tesisi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="hospitals" id="hospitals" class="form-control">
                            @foreach($hospitals as $h)
                            <option value="{{$h->HospitalCode}}">{{$h->HospitalCode}}-{{$h->HospitalName}}</option>
                            @endforeach
                          </select>
                        </div> 
                      </div>
                       <div id="div_khb" class="form-group hide">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">İl <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="hospitalsKhb" id="hospitalsKhb" class="form-control">
                            @foreach($hospitalsKhb as $h)
                            <option value="{{$h->KurumKod}}">{{$h->KurumAd}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      @endrole

                      @role('gsmember')
                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Sağlık Tesisi <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                              <select name="hospitals" id="hospitals" class="form-control">
                                @foreach($hospitals as $h)
                                <option value="{{$h->HospitalCode}}">{{$h->HospitalCode}}-{{$h->HospitalName}}</option>
                                @endforeach
                              </select>
                            </div> 
                      </div>
                      <select name="rol" id="rol" class="form-control hide">
                          <option value="hsmember" selected>Hastane Yöneticiliği</option>
                        </select>
                      @endrole


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary submit">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection