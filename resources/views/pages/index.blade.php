@extends('app')
@section('content')

   <div class="row">
      <h3 style="margin-left: 10px">{{$hospital_name or ''}}</h3>
   </div>
   <div class="row tile_count">
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
         <span class="count_top"><i class="fa fa-h-square" aria-hidden="true"></i> Toplam Kurum Sayısı</span>
         <div class="count">{{$hospitals_cnt}}</div>
         <span class="count_bottom"><i class="green">Aktif Kurum </i></span>
      </div>
      {{-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
         <span class="count_top"><i class="fa fa-user"></i> Toplam Klinik Kodlayıcı</span>
         <div class="count green">{{$users_cnt}}</div>
         <span class="count_bottom"><i class="green">Aktif Kullanıcı</i></span>
      </div> --}}
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
         <span class="count_top"><i class="fa fa-heartbeat" aria-hidden="true"></i> Toplam DRG</span>
         <div class="count">{{$drg_cnt}}</div>
         <span class="count_bottom">Sistemde Tanımlı</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
         <span class="count_top"><i class="fa fa-heartbeat" aria-hidden="true"></i> ICD Klinik Kod Sayısı</span>
         <div class="count">{{$icd_cnt}}</div>
         <span class="count_bottom">Sistemde Tanımlı</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
         <span class="count_top"><i class="fa fa-heartbeat" aria-hidden="true"></i> Achi Klinik Kod Sayısı</span>
         <div class="count">{{$achi_cnt}}</div>
         <span class="count_bottom">Sistemde Tanımlı</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
         <span class="count_top"><i class="fa fa-heartbeat" aria-hidden="true"></i> Morfoloji Klinik Kod Sayısı</span>
         <div class="count">{{$morph_cnt}}</div>
         <span class="count_bottom">Sistemde Tanımlı</span>
      </div>
   </div>
@endsection