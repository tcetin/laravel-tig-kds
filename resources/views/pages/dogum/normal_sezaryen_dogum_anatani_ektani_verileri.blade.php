@extends('app')
@section('content')
     <style type="text/css">
       .dc-chart g.row text {fill: #73879C;}
     </style>
    <div class="title_left">
        <h3> Normal ve Sezaryen Doğum Ana Tanı ve Ek Tanı Verileri </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'nsdogum_anatani_ektani'))

    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="x_panel">
                      <div class="x_title">
                          <small>Doğum Türü Sayıları</small>
                          <ul class="nav navbar-right panel_toolbox">
                             <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                             <li><a class="reset" href="javascript:dogumTurChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                              <li><a href="#" class="download-chart" chart-dim="dgm_tani_tur"><i class="fa fa-download fa-2x"></i></a></li>

                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content" style="overflow-y: true">
                          <div id="dgm_tani_tur" style="width: 100%;height:500px;">
                              <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                              <div class="clearfix"></div>
                          </div>
                      </div>
                  </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="x_panel">
                  <div class="x_title">
                      <small>Ana Tanı ve Ek Tanı Sayıları</small>
                      <ul class="nav navbar-right panel_toolbox">
                         <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                         <li><a class="reset" href="javascript:dgmTaniDurumChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                          <li><a href="#" class="download-chart" chart-dim="dgm_tani_durum"><i class="fa fa-download fa-2x"></i></a></li>

                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="overflow-y: true">
                      <div id="dgm_tani_durum" style="width: 100%;height:500px;">
                          <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                          <div class="clearfix"></div>
                      </div>
                  </div>
              </div>
      </div>
  </div>
  <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <small>Tanı Verileri</small>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                     <li><a class="reset" href="javascript:dgmTaniChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                      <li><a href="#" class="download-chart" chart-dim="dgm_tani"><i class="fa fa-download fa-2x"></i></a></li>

                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div id="dgm_tani" style="width: 100%;height: 500px;overflow-y: scroll">
                      <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
  </div>
  </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <small>İller Bazında Tanı Sayıları</small>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>

                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div id="dgm_tani_il" style="width: 100%;height:600px;"></div>
              </div>
          </div>
      </div>
  </div>

    <div class="fresh-table full-screen-table toolbar-color-azure">


        <table id="dgm_tani_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true"
               data-url="/dogum/normal_sezaryen_dogum_anatani_ektani_verilerini_hesapla">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane Adı</th>
                <th data-field="CityName" data-filter-control="select">İl</th>
                <th data-field="DogumTur" data-filter-control="select">Doğum Türü</th>
                <th data-field="Code" data-filter-control="input">Tanı Kodu</th>
                <th data-field="TANIM" data-filter-control="input">Kod Açıklama</th>
                <th data-field="TaniDurum" data-filter-control="select">Tanı Durumu</th>
                <th data-field="Say" data-sortable="true">Sayı</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@endsection