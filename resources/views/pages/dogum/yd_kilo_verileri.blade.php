@extends('app')
@section('content')
   <style type="text/css">
     .dc-chart g.row text {fill: #73879C;}
   </style>

    <div class="title_left">
        <h3> Yeni Doğan Kilo Verileri </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'yd_kilo'))

  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <small>Kilo Bazında Yenidoğan Sayıları</small>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                     <li><a class="reset" href="javascript:ydKiloChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                      <li><a href="#" class="download-chart" chart-dim="yd_kilo"><i class="fa fa-download fa-2x"></i></a></li>

                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div id="yd_kilo" style="width: 100%;height:500px;">
                      <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
  </div>

  <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-6">
          <div class="x_panel">
              <div class="x_title">
                  <small>Kurum Bazında Yenidoğan Sayıları</small>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                      <li><a class="reset" href="javascript:ydKiloKurumChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                      <li><a href="#" class="download-chart" chart-dim="kurum_statu"><i class="fa fa-download fa-2x"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div id="yd_kilo_kurum_statu" style="width: 100%;height:500px;">
                      <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>

    <div class="col-md-6 col-sm-6 col-xs-6">
          <div class="x_panel">
              <div class="x_title"> 
                  <small>Cinsiyet Bazında Yenidoğan Sayıları</small>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>
                      <li><a class="reset" href="javascript:ydKiloCinsiyetChart.filterAll();dc.redrawAll();"><i class="fa fa-filter fa-2x"></i></a></li>
                      <li><a href="#" class="download-chart" chart-dim="cinsiyet"><i class="fa fa-download fa-2x"></i></a></li>
                  </ul>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div id="yd_kilo_cinsiyet" style="width: 100%;height:500px;">
                      <span class="reset" style="display: none;">Filtre: <span class="filter"></span></span>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>





  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <small>İller Bazında Yenidoğan Sayıları</small>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="reset-all" href="javascript:dc.filterAll(); dc.renderAll();"><i class="fa fa-refresh fa-2x" aria-hidden="true"></i></a></li>

                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div id="yd_kilo_il" style="width: 100%;height:600px;"></div>
              </div>
          </div>
      </div>
  </div>


        <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="yd_kilo_frekans_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="10"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane</th>
                <th data-field="Statu" data-filter-control="select">Kurum Statu</th>
        				<th data-field="CityName" data-filter-control="input">İl</th>
                <th data-field="Cinsiyet" data-filter-control="select">Cinsiyet</th>
        				<th data-field="KiloAralik" data-filter-control="select">Kilo Aralığı</th>
        				<th data-sortable="true" data-field="KiloDeger">Kilo Değeri</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>



@endsection