@extends('app')
@section('content')
    <div class="title_left">
        <h3> Doğum Frekans Verileri </h3>
    </div>
    <div class="clearfix"></div>

    @include("partials.period_form",array('data_type'=>'dogum_frekans'))


    <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-bar-chart"></i></div>
                  <div class="count total-ndogum">{{$totalDogum[0]->ndogum ?? ''}}</div>
                  <h3>Normal Doğum</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-area-chart"></i></div>
                  <div class="count total-sdogum">{{$totalDogum[0]->sdogum ?? ''}}</div>
                  <h3>Sezaryen Doğum</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-area-chart"></i></div>
                  <div class="count total-pdogum">{{$totalDogum[0]->pdogum ?? ''}}</div>
                  <h3>Primer Sezaryen Doğum</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-line-chart"></i></div>
                  <div class="count total-mdogum">{{$totalDogum[0]->mdogum ?? ''}}</div>
                  <h3>Müdehaleli Doğum</h3>
                </div>
              </div>
    </div>



      @include("partials.chart_row",array('grid_width'=>'12','title'=>'Doğum Sayılarının Kurum Türü Bazında Dağılımı','content_div_id'=>'dogum_turleri_devlet_ozel_universite','content_div_height'=>'600'))

		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		        <div class="x_panel">
		            <div class="x_title">
		                <small>İl Bazında Doğum Frekanslarının Dağılımı</small>
		                <ul class="nav navbar-right panel_toolbox">
		                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		                    <li class="dropdown">
		                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
		                    	<ul class="dropdown-menu" role="menu">
			                    	<li><a class="filter_dgm" fiter-data="ndogum">Normal Doğum</a></li>
			                    	<li><a class="filter_dgm" fiter-data="mdogum">Müdehaleli Doğum</a></li>
			                    	<li><a class="filter_dgm" fiter-data="sdogum">Sezaryen Doğum</a></li>
			                    	<li><a class="filter_dgm" fiter-data="pdogum">Primer Sezaryen Doğum</a></li>
			                    	<li><a class="filter_dgm" fiter-data="tumu">Tümü</a></li>
                            <li><a class="remove_filter" fiter-data="tumu">Filtreyi Kaldır</a></li>
		                    	</ul>
		                    </li>
		                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
		                </ul>
		                <div class="clearfix"></div>
		            </div>
		            <div class="x_content">
		                <div id="il_bazinda_dogum_say_dagilim" style="width: 100%;height:600px;"></div>
		            </div>
		        </div>
		    </div>
		</div>


        <div class="fresh-table full-screen-table toolbar-color-azure">
        <table id="dogum_frekans_tbl" class="table table-bordered fresh-table"
               data-toolbar="#toolbar"
               data-search="true"
               data-show-refresh="true"
               data-show-toggle="true"
               data-show-columns="true"
               data-show-export="true"
               data-sortable="true"
               data-show-pagination-switch="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-size="100"
               data-page-list="[10, 25, 50, 100, ALL]"
               data-show-footer="false"
               data-filter-control="true"
               data-filter-show-clear="true">
            <thead>
            <tr>
                <th data-sortable="true" data-field="DonemKodu">Dönem Kodu</th>
                <th data-field="DonemAdi" data-filter-control="select">Dönemi</th>
                <th data-field="HospitalCode" data-filter-control="input">Hastane Kodu</th>
                <th data-field="HospitalName" data-filter-control="input">Hastane</th>
        				<th data-field="Statu" data-filter-control="select">Kurum Türü</th>
        				<th data-field="HospitalType" data-filter-control="input">Kurum Türü</th>
        				<th data-field="CityName" data-filter-control="input">İl</th>
        				<th data-field="NormalDogum" data-sortable="true">Normal Doğum</th>
        				<th data-field="SezeryanDogum" data-sortable="true">Sezaryen Doğum</th>
        				<th data-field="PrimerSezaryen" data-sortable="true">Primer Sezaryen Doğum</th>
        				<th data-field="MudehaleliDogum" data-sortable="true">Müdehaleli Doğum</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>



@endsection