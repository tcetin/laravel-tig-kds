
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Tig Karar Destek Sistemi</title>
		<meta name="description" content="Tig Karar Destek Sistemi">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="login/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="login/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="login/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- iDea core CSS file -->
		<link href="login/css/style.css" rel="stylesheet">

		<!-- Color Scheme (In order to change the color scheme, replace the red.css with the color scheme that you prefer)-->
		<link href="login/skins/red.css" rel="stylesheet">

		<!-- Custom css -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<!-- body classes: 
			"boxed": boxed layout mode e.g. <body class="boxed">
			"pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> 
	-->
	<body class="no-trans">

		<!-- background image -->
		<div class="fullscreen-bg"></div>

		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container light-translucent-bg">
				
				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-8 col-md-offset-2">

							<!-- logo -->
							<div class="logo">
								<a href="http://sb.gov.tr/"><img src="images/sb.jpg"  style="max-height: 100px;max-width: 100px;"/></a>
							</div>

							<!-- name-and-slogan -->
							<div class="site-slogan">
								TEŞHİS İLİŞKİLİ GRUPLAR (TİG)<br>
								Karar Destek Sistemi
							</div>
		
							<div class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="300">
								<div class="form-block center-block">
									<h2 class="title">Kullanıcı Girişi</h2>
									<hr>
									<form class="form-horizontal" method="post" role="form" action="{{ url('/kdslogincheck') }}" >
										{{ csrf_field() }}
										<div class="form-group has-feedback">
											<label for="inputUserName" class="col-sm-3 control-label">Kullanıcı Adı</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="username" name="username" placeholder="Kullanıcı Adı" required>
												<i class="fa fa-user form-control-feedback"></i>
											</div>
												@if ($errors->has('username'))
				                                    <span class="help-block">
				                                        <p>{{ $errors->first('username') }}</p>
				                                    </span>
				                                 @endif
										</div>
										<div class="form-group has-feedback">
											<label for="inputPassword" class="col-sm-3 control-label">Şifre</label>
											<div class="col-sm-8">
												<input type="password" class="form-control" id="password" name="password" placeholder="Şifre" required>
												<i class="fa fa-lock form-control-feedback"></i>
											</div>
											@if ($errors->has('password'))
				                                    <span class="help-block">
				                                        <p>{{ $errors->first('password') }}</p>
				                                    </span>
				                           @endif
										</div>
										<div class="form-group">
											<div class="col-sm-offset-3 col-sm-8">											
												<button type="submit" class="btn btn-group btn-default btn-sm" id="gonder" name="gonder">Giriş</button>
												<ul>
													<li><a href="/password/reset" target="_blank">Şifremi/Kullanıcı Adımı Unuttum?</a></li>
												</ul>
											</div>
										</div>
									</form>
								</div>
							</div>

						</div>
						<!-- main end -->

					</div>
				</div>

			</section>
			<!-- main-container end -->

		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="login/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="login/js/modernizr.js"></script>

		<!-- Appear javascript -->
		<script type="text/javascript" src="login/js/jquery.appear.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="login/js/template.js"></script>


		<footer id="footer" class="light">
				<!-- ================ -->
				<div class="subfooter">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<p><a href="http://www.sb.gov.tr/" target="_blank">T.C. Sağlık Bakanlığı | Sağlık Hizmetleri Genel Müdürlüğü</a></p>
						   </div> 
						</div>
					</div>
				</div>

			</footer>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

	</body>
</html>

