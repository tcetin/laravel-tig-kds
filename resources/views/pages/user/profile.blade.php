@extends('app')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Kullanıcı Profili</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<table class="table">
					<thead>
						<tr>
							<th>Kullanıcı Adı</th>
							<th>Ad</th>
							<th>Soyad</th>
							<th>Rol</th>
							<th>Eposta</th>
							<th>Telefon</th>
						</tr>
					</thead>
					<tbody>
						<td>{{Auth::user()->username}}</td>
						<td>{{Auth::user()->first_name}}</td>
						<td>{{Auth::user()->last_name}}</td>
						<td>{{Auth::user()->roles->first()->name}}</td>
						<td>{{Auth::user()->email}}  
						 <a href="/user_profile/change_email_page">Güncelle</a>
						</td>
						<td>{{Auth::user()->phone}}</td>
					</tbody>
				</table>

			</div> 
		</div>
	</div>
</div>
@endsection