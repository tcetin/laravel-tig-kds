@extends('app')
@section('content')
   <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><strong><i class="fa fa-key"></i> Eposta Güncelle</strong></div>

                <div class="panel-body">

  
                    <div class="alert alert-success {{session('statu')==true ? '':'hidden'}}">İşlem Başarılı.</div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user_profile/change_email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Yeni Eposta</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Güncelle
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection