
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
});

var $user_table = $('table#kullanici_yonetimi_tbl').bootstrapTable({
    url: '/kullanici_yonetimi/kullanicilar'
}),
    $user_mdl = $('#create_user_modal').modal({ show: false }),
    $alert = $('.alert').hide();

$(function () {

    $("select#hospitals,select#hospitalsKhb,select#hastane_kodu").select2({ theme: 'bootstrap', width: '90%' });

    $(document).on('change', 'select#rol', function () {
        //hsmember hastane
        var rol = $(this).val();

        if (rol == 'hsmember') {
            $('div#div_hst').removeClass("hide");
            $('div#div_khb').addClass("hide");
        } else if (rol == 'gsmember') {
            $('div#div_hst').addClass("hide");
            $('div#div_khb').removeClass("hide");
        } else {
            $('div#div_hst').addClass("hide");
            $('div#div_khb').addClass("hide");
        }
    });

    // create event
    $('.create').click(function () {
        showMdl($(this).text());
    });
    $user_mdl.find('.submit').click(function () {
        var row = {};
        $user_mdl.find('input[name]').each(function () {
            row[$(this).attr('name')] = $(this).val();
        });
        $.ajax({
            url: '/kullanici_yonetimi/kullanici_olustur',
            type: 'get',
            data: {
                'username': $('input#username').val(),
                'first_name': $('input#first-name').val(),
                'last_name': $('input#last-name').val(),
                'email': $('input#email').val(),
                'phone': $('input#phone').val(),
                'rol': $('select#rol option:selected').val(),
                'hospitals': $('select#hospitals option:selected').val(),
                'hospitalsKhb':$('select#hospitalsKhb option:selected').val()
            },
            success: function () {
                $user_mdl.modal('hide');
                $user_table.bootstrapTable('refresh');
                showAlert(' İşlem Başarılı!', 'success');
                $('input#username').val("");
                $('input#first-name').val("");
                $('input#last-name').val("");
                $('input#email').val("");
                $('select#rol option:selected').remove();
                $('select#rol option:selected').val("");
                $('select#hospitals option:selected').remove();
                $('select#hospitals option:selected').val("");
                $('select#hospitalsKhb option:selected').remove();
                $('select#hospitalsKhb option:selected').val("");

            },
            error: function () {
                $user_mdl.modal('hide');
                showAlert(' Hata Oluştu!', 'danger');
            }
        });
    });

    $(document).on('click', 'a.update', function () {
        var user = JSON.stringify($user_table.bootstrapTable('getSelections'));
        var jsonData = JSON.parse(user);

        if (jsonData.length == 1) {

            var userId = jsonData[0].id;

            $('input#username').val(jsonData[0].username);
            $('input#first-name').val(jsonData[0].first_name);
            $('input#last-name').val(jsonData[0].last_name);
            $('input#email').val(jsonData[0].email);
            $('input#phone').val(jsonData[0].phone);
            $user_mdl.modal('show');
            $user_mdl.find('.submit').click(function () {
                if (userId != undefined) {
                    $.ajax({
                        url: '/kullanici_yonetimi/kullanici_guncelle',
                        type: 'get',
                        data: {
                            'id': userId, 'username': $('input#username').val(),
                            'first_name': $('input#first-name').val(),
                            'last_name': $('input#last-name').val(),
                            'email': $('input#email').val(),
                            'phone': $('input#phone').val(),
                            'rol': $('select#rol option:selected').val(),
                            'hospitals': $('select#hospitals option:selected').val(),
                            'hospitalsKhb':$('select#hospitalsKhb option:selected').val()
                        },
                        success: function () {
                            $user_mdl.modal('hide');
                            $user_table.bootstrapTable('refresh');
                            showAlert(' İşlem Başarılı!', 'success');
                            $('input#username').val("");
                            $('input#first-name').val("");
                            $('input#last-name').val("");
                            $('input#email').val("");
                            $('select#rol option:selected').remove();
                            $('select#rol option:selected').val("");
                            $('select#hospitals option:selected').remove();
                            $('select#hospitals option:selected').val("");
                            $('select#hospitalsKhb option:selected').remove();
                            $('select#hospitalsKhb option:selected').val("");
                        },
                        error: function () {
                            $user_mdl.modal('hide');
                            showAlert(' Hata Oluştu!', 'danger');
                        }

                    });
                }
            });
        } else {
            alert("Güncelleme işlemi için tek kayıt seçmelisiniz.");
        }
    });

    $(document).on('click', 'a.delete', function () {
        var user = JSON.stringify($user_table.bootstrapTable('getSelections'));
        var jsonData = JSON.parse(user);
        //debugger;
        if (jsonData.length == 1) {
            var userId = jsonData[0].id;
            if (userId != undefined) {
                $.ajax({
                    url: '/kullanici_yonetimi/kullanici_sil/' + userId,
                    type: 'get',
                    success: function () {
                        $user_mdl.modal('hide');
                        $user_table.bootstrapTable('refresh');
                        showAlert(' İşlem Başarılı!', 'success');
                    },
                    error: function () {
                        $user_mdl.modal('hide');
                        showAlert(' Hata Oluştu!', 'danger');
                    }

                });
            }


        } else {
            alert("Silme işlemi için tek kayıt seçmelisiniz.");
        }
    });
});


    function showMdl(title, row) {
        row = row || {
            id: '',
            name: '',
            stargazers_count: 0,
            forks_count: 0,
            description: ''
        }; // default row value
        $user_mdl.data('id', row.id);
        $user_mdl.find('.modal-title').text(title);
        for (var name in row) {
            $user_mdl.find('input[name="' + name + '"]').val(row[name]);
        }
        $user_mdl.modal('show');
    }

    function showAlert(title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
              .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 3000);
    }