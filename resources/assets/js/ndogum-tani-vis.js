
var path = window.location.pathname;

if(path=='/dogum/normal_sezaryen_dogum_anatani_ektani_verileri'){

//tig-custom.js
if(dataUrl=='' || dataUrl===undefined){
        dataUrl='/dogum/normal_sezaryen_dogum_anatani_ektani_verilerini_hesapla';
        makeDgmTaniVis(dataUrl);
}     


var dgmTaniDurumChart = dc.pieChart('#dgm_tani_durum'),
    dgmTaniChart = dc.rowChart('#dgm_tani'),
    dogumTurChart = dc.pieChart('#dgm_tani_tur'),
    dgmMap='',
    mapData='',
    dctable = $('table#dgm_tani_tbl');

  function  makeDgmTaniVis(dataUrl){ 
    d3.json(dataUrl, function(data) {

    	dctable.bootstrapTable('load',data);

    	/*------------------------------
         T O O L T İ P S 
         ------------------------------*/
        //row tooltip
        var tip = d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(function(d) {
            return "<span style='color: #fff;text-shadow: 0 0 3px rgba(255,255,255,0.4);'>" + d.key+"-"+d.value.desc+ "</span> : " + d.value.total;
        });
        // tooltips for pie chart
        var pieTip = d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(function(d) {
             return "<span style='color: #fff;text-shadow: 0 0 3px rgba(255,255,255,0.4);'>" + d.data.key + "</span> : " + d.value;
        });

    	var ndx = crossfilter(data);
        var all = ndx.groupAll();



        var totalSum = all.reduceSum(function(d) {
            return d.Say;
        });

        var taniDurumDim = ndx.dimension(function(d) {
            return d.TaniDurum
        }),taniDurumGroup = taniDurumDim.group().reduceSum(function(d) {
            return d.Say;
        }),dogumTurDim = ndx.dimension(function(d) {
            return d.DogumTur
        }),dogumTurGroup = dogumTurDim.group().reduceSum(function(d) {
            return d.Say;
        }),taniDim = ndx.dimension(function(d) {
            return d.Code;
        }),taniGroup = taniDim.group().reduce(
            // add
            function(p,v) {
                p.code = v.Code;
                p.desc = v.TANIM;
                p.total += v.Say;
                return p;    
            },
            // remove
            function(p,v) {
                p.code = v.Code;
                p.desc = v.TANIM;
                p.total -= v.Say;
                return p; 
            },
            // init
            function(p,v) {
                return { code:'',desc:'',total:0}
            }
        );

        //İller
        var ilDim = ndx.dimension(function(d) { 
            return d.id //"TR-"
        });
        var ilGroup = ilDim.group().reduceSum(function(d) {
            return d.Say
        });





        //Charts
        dogumTurChart
            .width(500).height(500).slicesCap(4).innerRadius(100)
            .group(dogumTurGroup) // set group
            .dimension(dogumTurDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', dogumTurDim.top(Infinity));
                dgmMap.dataProvider.areas = mapData;
                dgmMap.validateData();
            })
            .valueAccessor(function(p) {
                return p.value;
            }).renderLabel(true).label(function(d) {
                return d.key + ':' + d.value + ' (%' + ((d.value / totalSum.value()) * 100).toFixed(2) + ')';
            }).title(function(d) {
                return d.key + ": " + d.value;
            }).legend(dc.legend()).renderTitle(true);

       dgmTaniDurumChart
			.width(500).height(500).slicesCap(4).innerRadius(100)
            .group(taniDurumGroup) // set group
            .dimension(taniDurumDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniDurumDim.top(Infinity));
                dgmMap.dataProvider.areas = mapData;
                dgmMap.validateData();
            })
            .valueAccessor(function(p) {
                return p.value;
            }).renderLabel(true).label(function(d) {
                return d.key + ':' + d.value + ' (%' + ((d.value / totalSum.value()) * 100).toFixed(2) + ')';
            }).title(function(d) {
                return d.key + ": " + d.value;
            }).legend(dc.legend()).renderTitle(true);

        dgmTaniChart.width(1200) // (optional) define chart width, :default = 200
            .height(30000) // (optional) define chart height, :default = 200  
            .group(taniGroup) // set group
            .dimension(taniDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniDim.top(Infinity));
                dgmMap.dataProvider.areas = mapData;
                dgmMap.validateData();
            })
            .valueAccessor(function(p) { 
                return p.value.total; 
            }) 
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniDim.top(Infinity));
            }).renderLabel(true).title(function(d) {
                return d.value.code+"-"+d.value.desc+ ":" + d.value.total;
            }).ordering(function(d) {
                return -d.value.total;
            }).xAxis().ticks(20);


        //Export AS CSV 
        $(document).on("click", "a.download-chart", function() {
            if ($(this).attr("chart-dim") == "dgm_tani_durum") {
                var blob = new Blob([d3.csv.format(taniDurumGroup.top(Infinity))], {
                    type: "text/csv;charset=utf-8"
                });
                saveAs(blob, 'dgm_tani_durum.csv');
            } else if ($(this).attr("chart-dim") == "dgm_tani") {
                var blob = new Blob([d3.csv.format(taniGroup.top(Infinity))], {
                    type: "text/csv;charset=utf-8"
                });
                saveAs(blob, 'dgm_tani.csv');
            }
        });

        mapData= ilGroup.top(Infinity);
        //change key to id
        for (var i = mapData.length - 1; i >= 0; i--) {
             mapData[i].id = mapData[i]['key'];
             delete mapData[i].key;
        }

       /*İllerdeki YeniDoğan Sayıları*/
        dgmMap = AmCharts.makeChart( "dgm_tani_il", {
                  "type": "map",
                  "theme": "light",
                  "colorSteps": 10,
                  "dataProvider": {
                        "map": "turkeyLow",
                        "areas": mapData
                   },
                  "areasSettings": {
                    "autoZoom": false,
                    "selectable":true
                  },

                  "valueLegend": {
                    "right": 10,
                    "minValue": "En Az",
                    "maxValue": "En Çok"
                  },
                  "imagesSettings": {
                    "labelPosition": "middle",
                    "labelFontSize": 8
                  },

                  "export": {
                    "enabled": true,
                    "fileName":"İl Bazında Yenidoğan Sayıları"
                  },
                   "listeners": [{
                    "event": "clickMapObject",
                    "method": function(event) {
                        var cityCode = event.mapObject.id;
                        ilDim.filter(cityCode);
                        dctable.bootstrapTable('load', ilDim.top(Infinity));
                        dc.redrawAll();
                    }
                  }]

                } );

        dgmMap.balloonLabelFunction = function (area, map) {      
                var valueText = "<strong><u><h6>"+area.title+"</h6></u></strong>"+"<br/>"+
                                "<p><strong>Tanı Sayısı:</strong>"+area.value+"</p>";
                return valueText;                
            }

       $(document).on("click","a.reset-all",function(){
            ilDim.filterAll();

        });   
             

     	dc.renderAll();

       /*----------------------------------
         C A L L  T O O L T I P S
         ----------------------------------*/
        //ROW TOOL TIP
        d3.selectAll("g.row").call(tip);
        d3.selectAll("g.row").on('mouseover', tip.show).on('mouseout', tip.hide);
        //PIE TOOL TIP
        d3.selectAll(".pie-slice").call(pieTip);
        d3.selectAll(".pie-slice").on('mouseover', pieTip.show)
               .on('mouseout', pieTip.hide);
    });
}//end function
}//end if