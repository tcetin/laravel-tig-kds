
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });

    var $table = $('table#kurum_yonetimi_tbl').bootstrapTable({
        url: '/kurum_yonetimi/kurumlar'
    }), 
        $modal = $('#create_hospital_modal').modal({show: false}),
        $alert = $('.alert').hide();

    $(function () {

        $("select#hospitalsKhb").select2({theme:'bootstrap',width:'90%' });

        // create event
        $('.create_hospital').click(function () {
            showModal($(this).text());
        });
        $modal.find('.submit').click(function () {
            var row = {};
            $modal.find('input[name]').each(function () {
                row[$(this).attr('name')] = $(this).val();
            });
            $.ajax({
                url: '/kurum_yonetimi/kurum_olustur',
                type: 'post',
                data: {'hospital_code':$('input#hospital_code').val(),
                       'hospital_name':$('input#hospital_name').val(),
                       'hospital_khb':$('select#hospital_khb option:selected').val(),
                       'hospital_type':$('select#hospital_type option:selected').val(),
                    },
                success: function (response) {
                    console.log(response);
                    $modal.modal('hide');
                    $table.bootstrapTable('refresh');
                    showAlert(' İşlem Başarılı!', 'success');
                    $('input#hospital_code').val("");
                    $('input#hospital_name').val("");
                    $('select#hospital_khb option:selected').remove();
                    $('select#hospital_khb option:selected').val("");
                    $('select#hospital_city option:selected').remove();
                    $('select#hospital_city option:selected').val("");
                },
                error: function (error) {
                    console.log(error);
                    debugger;
                    $modal.modal('hide');
                    showAlert(' Hata Oluştu!', 'danger');
                }
            });
        });

        $(document).on('click','a.update_hospital',function(){
            $('button.submit_update').removeClass("hide");
            $('button.submit').addClass("hide");
            var user = JSON.stringify($table.bootstrapTable('getSelections'));
            var jsonData = JSON.parse(user);

            if(jsonData.length==1){
             var hospitalId= jsonData[0].hospitalId;
             $('input#hospital_code').val(jsonData[0].HospitalCode);  
             $('input#hospital_name').val(jsonData[0].HospitalName);

            $modal.modal('show');
            $modal.find('.submit_update').click(function () {
                 if(hospitalId!=undefined){
                    $.ajax({ 
                        url: '/kurum_yonetimi/kurum_guncelle',
                        type: 'get',
                        data:{
                           'id':hospitalId,
                           'hospital_code':$('input#hospital_code').val(),
                           'hospital_name':$('input#hospital_name').val(),
                           'hospital_khb':$('select#hospital_khb option:selected').val(),
                           'hospital_type':$('select#hospital_type option:selected').val()},
                        success:function(){
                            $modal.modal('hide');
                            $table.bootstrapTable('refresh');
                            showAlert(' İşlem Başarılı!', 'success');
                            $('input#hospital_code').val("");
                            $('input#hospital_name').val("");
                            $('select#hospital_khb option:selected').remove();
                            $('select#hospital_khb option:selected').val("");
                            $('select#hospital_city option:selected').remove();
                            $('select#hospital_city option:selected').val("");
                            $('button.submit_update').addClass("hide"); 
                            $('button.submit').removeClass("hide");
                        },
                        error:function(){
                            $modal.modal('hide');
                            showAlert(' Hata Oluştu!', 'danger');
                        }

                     });
                    }
                });
            }else{
                alert("Güncelleme işlemi için tek kayıt seçmelisiniz.");
            }
        });

        $(document).on('click','a.delete_hospital',function(){
            var user = JSON.stringify($table.bootstrapTable('getSelections'));
            var jsonData = JSON.parse(user);
            //debugger;
            if(jsonData.length==1){
            var id = jsonData[0].hospitalId;
             if(id!=undefined){
                $.ajax({
                    url: '/kurum_yonetimi/kurum_sil/'+id,
                    type: 'get', 
                    success:function(){
                        $modal.modal('hide');
                        $table.bootstrapTable('refresh');
                        showAlert(' İşlem Başarılı!', 'success');
                    },
                    error:function(){
                        $modal.modal('hide');
                        showAlert(' Hata Oluştu!', 'danger');
                    }

                 });
                }

             
            }else{
                alert("Silme işlemi için tek kayıt seçmelisiniz.");
            }
        });
    });


    function showModal(title, row) {
        row = row || {
            id: '',
            name: '',
            stargazers_count: 0,
            forks_count: 0,
            description: ''
        }; // default row value
        $modal.data('id', row.id);
        $modal.find('.modal-title').text(title);
        for (var name in row) {
            $modal.find('input[name="' + name + '"]').val(row[name]);
        }
        $modal.modal('show');
    }

    function showAlert(title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
              .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 3000);
    }