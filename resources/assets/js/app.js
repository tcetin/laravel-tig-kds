
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('successalert', require('./components/SuccessAlert.vue'));
Vue.component('vue-form', require('./components/VueForm.vue'));
Vue.component('select2', require('./components/Select2.vue'));
Vue.component('sut-ek2c-form', require('./components/Forms/SutEk2C.vue'));
Vue.component('vue-grid', require('./components/VueGrid.vue'));

Vue.component('period', require('./components/Period.vue'));
Vue.component('wizard', require('./components/wizard/Wizard.vue'));
Vue.component('w-header', require('./components/wizard/Header.vue'));
Vue.component('w-footer', require('./components/wizard/Footer.vue'));
Vue.component('w-content', require('./components/wizard/Content.vue'));
Vue.component('w-tab-pane', require('./components/wizard/TabPane.vue'));
Vue.component('w-nav', require('./components/wizard/Navigation.vue'));
Vue.component('w-tab', require('./components/wizard/Tab.vue'));
Vue.component('dinamik-rapor-form', require('./components/Forms/DinamikRapor.vue'));


const app = new Vue({
    el: '#app'
});
