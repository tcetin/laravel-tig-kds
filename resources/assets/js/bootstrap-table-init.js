window.onload=function(){

var $table = $('table#klinik_tbl,' +
               'table#hastane_verileri,' +
               'table#vaka_tbl,table#genel_tig_tbl,' +
               'table#drg_tbl,' +
               'table#ana_tani_basina_dusen_ek_tani_tbl,' +
               'table#islem_hasta_oran_tbl,' +
               'table#cinsiyet_yas_frekans_tbl,' +
               'table#tig_bazli_cinsiyet_yas_frekans_tbl,' +
               'table#tani_yas_cinsiyet_tbl,'+
               'table#tig_bazli_gunubirlik_ve_diger_yatis_frekans_tbl,'+
               'table#kurum_bazli_klinik_kodlayici_sayilari_tbl,'+
               'table#dogum_frekans_tbl,'+ 
               'table#yd_kilo_frekans_tbl,'+
               'table#dgm_tani_tbl,'+
               'table#kullanici_yonetimi_tbl,'+
               'table#drg_hospitals_table,table#donemsel_hastane_tig_yatis_tbl,'+
               'table#gider_tur_kategori_yonetimi_tbl,table#gider_tur_yonetimi_tbl,table#tibbi_gider_tbl,table#oecd_hastane_klinik_verileri,'+
               'table#brans_emek_hastane_verileri,table#sut_islemleri_girisi_tbl,table#kodlayici_tig_verileri,'+
               'table#sut_islem_ortalamalari,table#sut_islem_ortalamalari_ek2c'),
                ydKiloUrl, 
                dataUrl, 
                full_screen = true,
                table_height,
                window_height; 

$().ready(function () {

    window_height = $(window).height();
    table_height = window_height - 20;


    $table.bootstrapTable({
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            //do nothing here, we don't want to show the text "showing x of y from..."
        }, 
        formatRecordsPerPage: function (pageNumber) {
            return pageNumber + " Satır";
        },
        icons: {
            export: 'fa fa-download',
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle',
            clear: 'glyphicon-trash icon-clear',
            paginationSwitchDown: 'glyphicon-collapse-down icon-chevron-down',
            paginationSwitchUp: 'glyphicon-collapse-up icon-chevron-up'
        }
    });


});

}