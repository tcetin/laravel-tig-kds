var preloader,ydKiloUrl,dataUrl,klinikKodlar;

$().ready(function(){

    /**
     * SELECT2
     */

    $('select#klinik_kod_anatani').select2({width:'90%'});

    /**
     * ############### JQUERY AUTOCOMPLETE SEARCH ###########
     *
     */

     var menulinks = [
            { value: 'Hastane Tig Verileri', link: '/tig/donemsel_hastane_verileri' },
            { value: 'Hastane Branş Bazlı Emek Verileri', link: '/tig/brans_bazli_hastane_emek_verileri' },
            { value: 'Türkiye Geneli Vaka Sayıları', link: '/tig/donemsel_turkiye_geneli_vaka_sayilari' },
            { value: 'Türkiye Geneli Tig Verileri', link: '/tig/donemsel_turkiye_geneli_tig_verileri' },
            { value: 'Türkiye Geneli  Drg Bazlı Veriler', link: '/tig/donemsel_turkiye_geneli_drg_bazli_veriler' },
            { value: 'Hastane Bazlı Drg Sayıları', link: '/tig/donemsel_hastanebazli_drg_sayilari' },
            { value: 'Türkiye Geneli  Klinik Kod Bazlı Veriler', link: '/tig/donemsel_turkiye_geneli_klinik_kod_verileri' },
            { value: 'Türkiye Geneli  Ana Tanı Başına Düşen İşlem ve Ek Tanı Sayıları', link: '/tig/donemsel_anatani_basina_dusen_ektani' },
            { value: 'Türkiye Geneli  Tekil Vaka  Başına Düşen İşlem Sayıları', link: '/tig/donemsel_turkiye_geneli_islem_hasta_oranlari' },
            { value: 'Türkiye Geneli Cinsiyet ve Yaş Frekansları', link: 'tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslari' },
            { value: 'Türkiye Geneli Tig Bazlı Cinsiyet ve Yaş Frekansları', link: '/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslari' },
            { value: 'Türkiye Geneli ve Kurum Bazlı  Günübirlik Ve Diğer Yatışlardaki Tig Vaka Frekansları', link: '/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari' },
            { value: 'Kurum Bazli Klinik Kodlayıcı Sayıları', link: '/tig/kurum_bazli_klinik_kodlayici_sayilari' },
            { value: 'Doğum Frekans Verileri', link: '/dogum/dogum_frekans_verileri' },
            { value: 'Normal ve Sezaryen Doğum Ana Tanı ve Ek Tanı Verileri', link: '/dogum/normal_sezaryen_dogum_anatani_ektani_verileri' },
            { value: 'Yenidoğan Kilo Verileri', link: '/dogum/yd_kilo_verileri' },
            { value: 'Türkiye Geneli Tanı Bazlı Yaş ve Cinsiyet Sayıları',link:'/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilari'},
            { value: 'Sut İşlem Maliyetleri Türkiye Ortalamaları',link:'/sut_islem_ortalama_index'},
            { value: 'Sut İşlem Maliyet Girişi',link:'/sut_islem_bilgileri'},
        ];

    $('#autocomplete').autocomplete({
        lookup: menulinks,
        onSelect: function (suggestion) {
            location.href=suggestion.link;
        }
    });


        /*** KLİNİK KOD LİSTESİNİ GETİR ***/
        // axios.get('/tig/klinik_kod_listesi')
        //     .then(function (response) {
        //         $('input.kod-listesi').autocomplete({
        //             lookup: response.data,
        //             onSelect:function (msg) {
        //                 $('input.kod-listesi').val(msg.KOD2);
        //             }
        //         });
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });







    /**
     * ############### PRELOADER ###########
     *
     */


    preloader = new GSPreloader({
        radius:42,
        dotSize:15,
        dotCount:10,
        colors:["#61AC27","#555","purple","#FF6600"], //have as many or as few colors as you want.
        boxOpacity:0.2,
        boxBorder:"1px solid #AAA",
        animationOffset: 1.8, //jump 1.8 seconds into the animation for a more active part of the spinning initially (just looks a bit better in my opinion)
    });

    preloader.active(false);

    //this is the whole preloader class-function
    function GSPreloader(options) {
        options = options || {};
        var parent = options.parent || document.body,
            element = this.element = document.createElement("div"),
            radius = options.radius || 42,
            dotSize = options.dotSize || 15,
            animationOffset = options.animationOffset || 1.8, //jumps to a more active part of the animation initially (just looks cooler especially when the preloader isn't displayed for very long)
            createDot = function (rotation) {
                var dot = document.createElement("div");
                element.appendChild(dot);
                TweenLite.set(dot, {
                    width: dotSize,
                    height: dotSize,
                    transformOrigin: (-radius + "px 0px"),
                    x: radius,
                    backgroundColor: colors[colors.length - 1],
                    borderRadius: "50%",
                    force3D: true,
                    position: "absolute",
                    rotation: rotation
                });
                dot.className = options.dotClass || "preloader-dot";
                return dot;
            },
            i = options.dotCount || 10,
            rotationIncrement = 360 / i,
            colors = options.colors || ["#61AC27", "black"],
            animation = new TimelineLite({paused: true}),
            dots = [],
            isActive = false,
            box = document.createElement("div"),
            tl, dot, closingAnimation, j;
        colors.push(colors.shift());

        //setup background box
        TweenLite.set(box, {
            width: radius * 2 + 70,
            height: radius * 2 + 70,
            borderRadius: "14px",
            backgroundColor: options.boxColor || "white",
            border: options.boxBorder || "1px solid #AAA",
            position: "absolute",
            xPercent: -50,
            yPercent: -50,
            opacity: ((options.boxOpacity != null) ? options.boxOpacity : 0.3)
        });
        box.className = options.boxClass || "preloader-box";
        element.appendChild(box);

        parent.appendChild(element);
        TweenLite.set(element, {
            position: "fixed",
            top: "45%",
            left: "50%",
            perspective: 600,
            overflow: "visible",
            zIndex: 2000
        });
        animation.from(box, 0.1, {opacity: 0, scale: 0.1, ease: Power1.easeOut}, animationOffset);
        while (--i > -1) {
            dot = createDot(i * rotationIncrement);
            dots.unshift(dot);
            animation.from(dot, 0.1, {scale: 0.01, opacity: 0, ease: Power1.easeOut}, animationOffset);
            //tuck the repeating parts of the animation into a nested TimelineMax (the intro shouldn't be repeated)
            tl = new TimelineMax({repeat: -1, repeatDelay: 0.25});
            for (j = 0; j < colors.length; j++) {
                tl.to(dot, 2.5, {rotation: "-=360", ease: Power2.easeInOut}, j * 2.9)
                    .to(dot, 1.2, {skewX: "+=360", backgroundColor: colors[j], ease: Power2.easeInOut}, 1.6 + 2.9 * j);
            }
            //stagger its placement into the master timeline
            animation.add(tl, i * 0.07);
        }
        if (TweenLite.render) {
            TweenLite.render(); //trigger the from() tweens' lazy-rendering (otherwise it'd take one tick to render everything in the beginning state, thus things may flash on the screen for a moment initially). There are other ways around this, but TweenLite.render() is probably the simplest in this case.
        }

        //call preloader.active(true) to open the preloader, preloader.active(false) to close it, or preloader.active() to get the current state.
        this.active = function (show) {
            if (!arguments.length) {
                return isActive;
            }
            if (isActive != show) {
                isActive = show;
                if (closingAnimation) {
                    closingAnimation.kill(); //in case the preloader is made active/inactive/active/inactive really fast and there's still a closing animation running, kill it.
                }
                if (isActive) {
                    element.style.visibility = "visible";
                    TweenLite.set([element, box], {rotation: 0});
                    animation.play(animationOffset);
                } else {
                    closingAnimation = new TimelineLite();
                    if (animation.time() < animationOffset + 0.3) {
                        animation.pause();
                        closingAnimation.to(element, 1, {rotation: -360, ease: Power1.easeInOut}).to(box, 1, {
                            rotation: 360,
                            ease: Power1.easeInOut
                        }, 0);
                    }
                    closingAnimation.staggerTo(dots, 0.3, {
                        scale: 0.01,
                        opacity: 0,
                        ease: Power1.easeIn,
                        overwrite: false
                    }, 0.05, 0).to(box, 0.4, {
                        opacity: 0,
                        scale: 0.2,
                        ease: Power2.easeIn,
                        overwrite: false
                    }, 0).call(function () {
                        animation.pause();
                        closingAnimation = null;
                    }).set(element, {visibility: "hidden"});
                }
            }
            return this;
        };
    }
});


/**
 * ##################### Bootstrap Table ##############
 */




/**
 * ##################### Period Range Slider ##############
 */


var year_period,
    month_period_from,
    month_period_to,
    period_from,
    period_to,
    _data,
    $range_month = $(".range_month"),
    $range_year = $(".range_year"),
    islem_hasta_oranlari_chart,
    cinsiyet_yas_chart,
    tig_bazli_cinsiyet_yas_chart,
    tig_bazli_gunubirlik_ve_diger_vaka_frekans_chart;


    /* Doğum */

var dogum_turleri_devlet_ozel_universite_chart,
    dogum_map_filter_category=[],
    dogum_map,mapData;


$().ready(function () {



    $range_year.ionRangeSlider({
        grid: true,
        type:"single",
        from: 7,
        values: [
            "2010", "2011", "2012",
            "2013", "2014", "2015",
            "2016", "2017","2018"
        ],
        onStart: function (data) {
            year_period = data.from;
        },
        onChange: function (data) {
            year_period = data.from;
        },
        onUpdate: function (data) {
            year_period = data.from;
        },
        onFinish: function (data) {
            year_period = data.from;
        }
    });

    $range_month.ionRangeSlider({
        grid: true,
        type: "double",
        min: 0,
        max: 11,
        from: 0,
        to: 11,
        values: [
            "Ocak", "Şubat", "Mart",
            "Nisan", "Mayıs", "Haziran",
            "Temmuz", "Ağustos", "Eylül",
            "Ekim", "Kasım", "Aralık"
        ],
        onStart: function (obj) {
            month_period_from = obj.from;
            month_period_to = obj.to;
        },
        onUpdate: function (obj) {
            month_period_from = obj.from;
            month_period_to = obj.to;
        },
        onChange: function (obj) {
            month_period_from = obj.from;
            month_period_to = obj.to;
        },
        onFinish: function (obj) {
            month_period_from = obj.from;
            month_period_to = obj.to;
        }
    });

    $.getJSON("/js/period-map.json", function (data) {
        _data = data;
    });


    /**
     *  ########################### CHARTS #######################
     */


    // Translate


   function setDataToChart(dataset_url,chartType) {
        AmCharts.loadFile(dataset_url, {}, function(data) {
            chartType.dataProvider = AmCharts.parseJSON(data);
            chartType.validateData();
        });
    }

    /*****  turkiye_geneli_islem_hasta_oranlari ******/

    islem_hasta_oranlari_chart = AmCharts.makeChart("turkiye_geneli_islem_hasta_oranlari", {
        "language":"tr",
        "type": "serial",
        "theme": "light",
        "dataLoader": {
            "url": "/tig/donemsel_turkiye_geneli_islem_hasta_oranlarini_hesapla"
        },
        "valueAxes": [{
            "gridColor": "#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "line",
            "valueField": "IslemHastaOrani"
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "DonemAdi",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "tickPosition": "start",
            "tickLength": 20
        },
        "export":{
            "enabled":true
        }
    });

    /***** end turkiye_geneli_islem_hasta_oranlari ******/


    /**** türkiye geneli cinsiyet ve yaş frekansları ****/

    cinsiyet_yas_chart = AmCharts.makeChart("cinsiyet_yas_frekans", { 
         "language":"tr",
         "type": "serial",
          "theme": "light",
          "rotate": true,
          "marginBottom": 50,
          "dataLoader": {
            "url": "/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslarini_toplam_hesapla"
           },
          "pathToImages":"/images/amcharts/", 
          "startDuration": 1,
          "graphs": [{
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "male",
            "title": "Erkek",
            "labelText": "[[value]]",
            "clustered": false,
            "labelFunction": function(item) {
              return Math.abs(item.values.value);
            },
            "balloonFunction": function(item) {
              return item.category + ": " + Math.abs(item.values.value);
            }
          }, {
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "female",
            "title": "Kadın",
            "labelText": "[[value]]",
            "clustered": false,
            "labelFunction": function(item) {
              return Math.abs(item.values.value);
            },
            "balloonFunction": function(item) {
              return item.category + ": " + Math.abs(item.values.value);
            }
          }],
          "categoryField": "age",
          "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0.2,
            "axisAlpha": 0
          },
          "valueAxes": [{
            "gridAlpha": 0,
            "ignoreAxisWidth": true,
            "labelFunction": function(value) {
              return Math.abs(value) + '%';
            },
            "guides": [{
              "value": 0,
              "lineAlpha": 0.2
            }]
          }],
          "balloon": {
            "fixedPosition": true
          },
          "chartCursor": {
            "valueBalloonsEnabled": false,
            "cursorAlpha": 0.05,
            "fullWidth": true
          },
          "allLabels": [{
            "text": "Erkek",
            "x": "28%",
            "y": "97%",
            "bold": true,
            "align": "middle"
          }, {
            "text": "Kadın",
            "x": "75%",
            "y": "97%",
            "bold": true,
            "align": "middle"
          }],
         "export": {
            "enabled": true
          }


    });



    /**** end türkiye geneli cinsiyet ve yaş frekansları ****/


    /*** Türkiye Geneli Tig Bazlı Yaş ve Cinsiyet Frekansları **/

    tig_bazli_cinsiyet_yas_chart = AmCharts.makeChart("tig_bazli_cinsiyet_yas_frekans", {
        "language":"tr",
        "type": "xy",
        "theme": "light",
        "marginRight": 50,
        "marginTop": 10,
        "pathToImages":"/images/amcharts/",
        "dataLoader": {
            "url": "/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslarini_toplam_hesapla"
           },
        "valueAxes": [{
            "position": "bottom",
            "axisAlpha": 0
        }, {
            "minMaxMultiplier": 1.2,
            "axisAlpha": 0,
            "position": "left"
        }],
        "startDuration": 1.5,
        "graphs": [{
            "balloonText": "Drg:<b>[[Drg]]</b> <br/> Erkek:<b>[[Erkek]]</b> <br/> Kadın:<b>[[Kadin]]</b><br>Toplam:<b>[[Toplam]]</b>",
            "bullet": "bubble",
            "lineAlpha": 0,
            "valueField": "Toplam",
            "xField": "Erkek",
            "yField": "Kadin",
            "fillAlphas": 0,
            "bulletBorderAlpha": 0.2,
            "maxBulletSize": 120

        }],
        "marginLeft": 46,
        "marginBottom": 35,
        "chartScrollbar": {},
        "chartCursor": {},
        "balloon":{
            "fixedPosition":true
        },
        "export": {
            "enabled": true
        }
    });


    /*** end Türkiye Geneli Tig Bazlı Yaş ve Cinsiyet Frekansları ***/

    /*** Türkiye Geneli Günübirlik ve Diğer Yatışlardaki Vaka Frekansları ***/

    tig_bazli_gunubirlik_ve_diger_vaka_frekans_chart = AmCharts.makeChart("tig_bazli_gunubirlik_ve_diger_vaka_frekans", {
        "language":"tr",
        "type": "xy",
        "theme": "light",
        "marginRight": 50,
        "marginTop": 10,
        "pathToImages":"/images/amcharts/",
        "dataLoader": {
            "url": "/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari_toplam_hesapla"
           },
        "valueAxes": [{
            "position": "bottom",
            "axisAlpha": 0
        }, {
            "minMaxMultiplier": 1.2,
            "axisAlpha": 0,
            "position": "left"
        }],
        "startDuration": 1.5,
        "graphs": [{
            "balloonText": "Drg:<b>[[Drg]]</b> <br/> Günübirlik:<b>[[GunubirlikVakaFrekans]]</b> <br/> Diğer:<b>[[DigerVakaFrekans]]</b><br>Toplam:<b>[[Say]]</b>",
            "bullet": "bubble",
            "lineAlpha": 0,
            "valueField": "Say",
            "xField": "GunubirlikVakaFrekans",
            "yField": "DigerVakaFrekans",
            "fillAlphas": 0,
            "bulletBorderAlpha": 0.2,
            "maxBulletSize": 120

        }],
        "marginLeft": 46,
        "marginBottom": 35,
        "chartScrollbar": {},
        "chartCursor": {},
        "balloon":{
            "fixedPosition":true
        },
        "export": {
            "enabled": true
        }
    });

    /*** end Türkiye Geneli Günübirlik ve Diğer Yatışlardaki Vaka Frekansları ***/


    /*** #################  DOĞUM #################  ***/

    /*** Total Dogumlar ****/
    /*$.getJSON("/dogum/dogum_frekans_verilerini_hesapla", function (data) {
            total_ndogum = $("div.total-ndogum");
            total_ndogum.text("123");

            total_mdogum = $("div.total-mdogum");
            total_mdogum.text("123");

            total_sdogum = $("div.total-sdogum");
            total_sdogum.text("123");

            total_pdogum = $("div.total-pdogum");
            total_pdogum.text("123");
    });*/

    /*** end Total Dogumlar ****/

    /*** Özel-Ün-Devlet Doğum Türüne Göre ***/

    dogum_turleri_devlet_ozel_universite_chart = AmCharts.makeChart( "dogum_turleri_devlet_ozel_universite", {
                  "language":"tr",  
                  "type": "serial",
                  "addClassNames": true,
                  "theme": "light",
                  "autoMargins": false,
                  "marginLeft": 60,
                  "marginRight": 8,
                  "marginTop": 30,
                  "marginBottom": 26,
                  "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                  },
                  "dataLoader": {
                        "url": "/dogum/dogum_frekans_verilerini_kurum_turu_bazinda_hesapla"
                       },
                  "valueAxes": [ {
                    "axisAlpha": 0,
                    "position": "left"
                  } ],
                  "startDuration": 1,
                  "graphs": [ {
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[category]] [[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "Doğum Sayısı",
                    "type": "column",
                    "valueField": "toplam",
                    "dashLengthField": "dashLengthColumn"
                  }, {
                    "id": "graph2",
                    "balloonText": "<span style='font-size:12px;'> [[category]] ([[title]]):<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "bullet": "round",
                    "lineThickness": 3,
                    "bulletSize": 7,
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "useLineColorForBulletBorder": true,
                    "bulletBorderThickness": 3,
                    "fillAlphas": 0,
                    "lineAlpha": 1,
                    "title": "Devlet",
                    "valueField": "devlet",
                    "dashLengthField": "dashLengthLine"
                  } ,
                  {
                    "id": "graph3",
                    "balloonText": "<span style='font-size:12px;'>[[category]] ([[title]]):<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "bullet": "round",
                    "lineThickness": 3,
                    "bulletSize": 7,
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "useLineColorForBulletBorder": true,
                    "bulletBorderThickness": 3,
                    "fillAlphas": 0,
                    "lineAlpha": 1,
                    "title": "Özel",
                    "valueField": "ozel",
                    "dashLengthField": "dashLengthLine"
                  } ,
                  {
                    "id": "graph4",
                    "balloonText": "<span style='font-size:12px;'>[[category]] ([[title]]):<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "bullet": "round",
                    "lineThickness": 3,
                    "bulletSize": 7,
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "useLineColorForBulletBorder": true,
                    "bulletBorderThickness": 3,
                    "fillAlphas": 0,
                    "lineAlpha": 1,
                    "title": "Üniversite",
                    "valueField": "universite",
                    "dashLengthField": "dashLengthLine"
                  } ],
                  "categoryField": "dogum_tur",
                  "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0
                  },
                  "export": {
                    "enabled": true,
                    "fileName":"Kurum Türü Bazında Doğum Sayıları"
                  },
                  "legend": {
                    "useGraphSettings": true
                  },
                 "listeners": [{
                        "event": "clickGraphItem",
                        "method": function(e) { 
                            if(e.item.dataContext[e.graph.alphaField]==0.5){    
                                e.item.dataContext[e.graph.alphaField] = 1;
                                console.log(e.item.category);
                                //filtreyi kaldır.
                                var removeFilterIndex = dogum_map_filter_category.indexOf(e.item.category);
                                if(removeFilterIndex != -1) {
                                    dogum_map_filter_category.splice(removeFilterIndex, 1);
                                }

                            }else{
                                e.item.dataContext[e.graph.alphaField] = 0.5;
                                //filtrele
                                dogum_map_filter_category.push(e.item.category);
                            }

                            dogum_map_filter_category = $.unique(dogum_map_filter_category);

                            e.chart.validateData();    
                        }
                      }]
                });


    /*İllerdeki Doğum Sayıları*/
dogum_map = AmCharts.makeChart( "il_bazinda_dogum_say_dagilim", {
              "type": "map",
              "theme": "light",
              "colorSteps": 10,
              "dataLoader":{
                "url":"/dogum/dogum_frekans_verilerini_il_bazinda_hesapla"
              },
              "areasSettings": {
                "autoZoom": false,
                "selectable":true
              },

              "valueLegend": {
                "right": 10,
                "minValue": "En Az",
                "maxValue": "En Çok"
              },
              "imagesSettings": {
                "labelPosition": "middle",
                "labelFontSize": 8
              },

              "export": {
                "enabled": true,
                "fileName":"İl Bazında Doğum Sayıları"
              },
            "listeners": [{
                "event": "clickMapObject",
                "method": function(event) {
                    var cityCode = event.mapObject.CityCode;

                    var url = "/dogum/dogum_frekans_verilerini_kurum_turu_bazinda_hesapla?city="+cityCode;
                    var tableUrl = "/dogum/dogum_verileri?city="+cityCode;

                    if(typeof(period_to)!="undefined" && typeof(period_from)!="undefined"){
                        url = url+"&from="+period_from+"&to="+period_to;
                        tableUrl = tableUrl+"&from="+period_from+"&to="+period_to; 
                    }

                    setDataToChart(url,dogum_turleri_devlet_ozel_universite_chart);

                    // $table.bootstrapTable('refreshOptions', {url: tableUrl});
                    // $table.bootstrapTable('refresh', {url: tableUrl});

                    d3.json(tableUrl,function(data){
                         var arrayData = $.map(data, function(value, index) {
                                return [value];
                            });

                         $('table#dogum_frekans_tbl').bootstrapTable('load', arrayData); 
                     });

                    $.getJSON(url,function(res){
                        $.each(res, function( index, value ) {
                                if(res[index].dogum_tur=='Normal Doğum'){
                                    $('div.total-ndogum').text(res[index].toplam);
                                }else if(res[index].dogum_tur=='Sezaryan Doğum'){
                                    $('div.total-sdogum').text(res[index].toplam);
                                }else if(res[index].dogum_tur=='Primer Sezaryan Doğum'){
                                    $('div.total-pdogum').text(res[index].toplam);
                                }else if(res[index].dogum_tur=='Müdehaleli Doğum'){
                                    $('div.total-mdogum').text(res[index].toplam);
                                }
                            });
                    });


                }
              }]

            } );

        dogum_map.balloonLabelFunction = function (area, map) {      
            var valueText = "<strong><u><h6>"+area.title+"</h6></u></strong>"+"<br/>"+
                            "<p><strong>Normal Doğum:</strong>"+area.ndogum+"</p>"+
                            "<p><strong>Sezaryen Doğum:</strong>"+area.sdogum+"</p>"+
                            "<p><strong>Primer Sezaryen Doğum:</strong>"+area.pdogum+"</p>"+
                            "<p><strong>Müdehaleli Doğum:</strong>"+area.mdogum+"</p>"+
                            "<p><strong>Toplam Doğum:</strong>"+(area.ndogum+area.sdogum+area.pdogum+area.mdogum)+"</p>";
            return valueText;                
        }

        // dogum map enson hali
        var mapTemp = dogum_map.dataProvider;

        // dogum map change value dogum türüne göre
        $(document).on("click","a.filter_dgm",function(){
            var dtype = $(this).attr("fiter-data");
            switch(dtype) {
                case "ndogum":
                    dogum_map.dataProvider.images = [];
                    for (var i = dogum_map.dataProvider.areas.length - 1; i >= 0; i--) {
                        dogum_map.dataProvider.areas[i]['value'] = dogum_map.dataProvider.areas[i]['ndogum'];
                    }

                    dogum_map.validateData();
                    alert("İllerdeki dağılım Normal Doğum Sayılarına göre oluşturuldu.");
                    break;
               case "sdogum":
                    for (var i = dogum_map.dataProvider.areas.length - 1; i >= 0; i--) {
                        dogum_map.dataProvider.areas[i]['value'] = dogum_map.dataProvider.areas[i]['sdogum'];
                    }
                    dogum_map.validateData();
                    alert("İllerdeki dağılım Sezaryen Doğum Sayılarına göre oluşturuldu.");
                    break;
               case "pdogum":
                    for (var i = dogum_map.dataProvider.areas.length - 1; i >= 0; i--) {
                        dogum_map.dataProvider.areas[i]['value'] = dogum_map.dataProvider.areas[i]['pdogum'];
                    }
                    dogum_map.validateData();
                    alert("İllerdeki dağılım Primer Sezaryen Doğum Sayılarına göre oluşturuldu.");
                    break;
                case "mdogum":
                    for (var i = dogum_map.dataProvider.areas.length - 1; i >= 0; i--) {
                        dogum_map.dataProvider.areas[i]['value'] = dogum_map.dataProvider.areas[i]['mdogum'];
                    }
                    dogum_map.validateData();
                    alert("İllerdeki dağılım Müdehaleli Doğum Sayılarına göre oluşturuldu.");
                    break; 
                case "tumu":
                    for (var i = dogum_map.dataProvider.areas.length - 1; i >= 0; i--) {
                        var total = dogum_map.dataProvider.areas[i]['mdogum']+dogum_map.dataProvider.areas[i]['sdogum']+dogum_map.dataProvider.areas[i]['pdogum']+dogum_map.dataProvider.areas[i]['ndogum'];
                        dogum_map.dataProvider.areas[i]['value'] = total;
                    }
                    dogum_map.validateData();
                    alert("İllerdeki dağılım Tüm Doğum Sayılarına göre oluşturuldu.");
                    break; 
            }
        });
        //remove all filter
        $(document).on('click','a.remove_filter',function(){
            setDataToChart('/dogum/dogum_frekans_verilerini_kurum_turu_bazinda_hesapla',dogum_turleri_devlet_ozel_universite_chart);
            $.getJSON('/dogum/dogum_frekans_verilerini_kurum_turu_bazinda_hesapla',function(res){
                        $.each(res, function( index, value ) {
                                if(res[index].dogum_tur=='Normal Doğum'){
                                    $('div.total-ndogum').text(res[index].toplam);
                                }else if(res[index].dogum_tur=='Sezaryan Doğum'){
                                    $('div.total-sdogum').text(res[index].toplam);
                                }else if(res[index].dogum_tur=='Primer Sezaryan Doğum'){
                                    $('div.total-pdogum').text(res[index].toplam);
                                }else if(res[index].dogum_tur=='Müdehaleli Doğum'){
                                    $('div.total-mdogum').text(res[index].toplam);
                                }
                            });
            });
            //$('table#dogum_frekans_tbl').bootstrapTable('refresh', {url: '/dogum/dogum_verileri'});
        });



    /* end İllerdeki Doğum Sayıları*/

    /*** end Özel-Ün-Devlet Doğum Türüne Göre ***/ 

    /*** END DOĞUM ***/

    /*** YD KİLO ***/

    /*** /END YD KİLO ***/

/****  END CHARTS *****/

    $(document).on("click", "button.btn_period", function () {

        var dataType = $(this).attr("data-type");

        preloader.active(true);

        for(var key in _data){
            if (_data[key].year_map == year_period && _data[key].month == month_period_from) {
                period_from = key;
                break;
            }
        }

        for (var key in _data) {
            if (_data[key].year_map == year_period && _data[key].month == month_period_to) {
                period_to = key;
                break;
            }
        }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').attr("value")
            }
        });


        if (dataType == 'klinik_kod') {

            /**
             * Türkiye Geneli Klinik Kod Verileri AJAX
             * **/

            $.ajax({
                async:false,
                type: "GET",
                url: "/tig/donemsel_turkiye_geneli_klinik_kod_verileri_getir",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    $('table#klinik_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType == 'ana_tani_basina_dusen_ektani') {

            

            var kod = $("input.kod-listesi").val();

            /**
             * Türkiye Geneli Ana Tanı Başına Düşen Ek Tanı Sayısı AJAX
             * **/

            $.ajax({
                async:false,
                type: "POST",
                url: "/tig/donemsel_anatani_basina_dusen_ektani_sayilarini_getir",
                dataType: 'json',
                data: {from: period_from, to: period_to,kod:kod},
                success: function (res) {

                    preloader.active(false);

                    $('table#ana_tani_basina_dusen_ek_tani_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='islem_hasta_oran'){
            /**
             * Türkiye Geneli Hasta Başına Düşen İşlem Sayısı
             * **/

            $.ajax({
                async:false,
                type: "GET",
                url: "/tig/donemsel_turkiye_geneli_islem_hasta_oranlarini_hesapla",
                async:false,
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    setDataToChart(this.url,islem_hasta_oranlari_chart);

                    $('table#islem_hasta_oran_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='cinsiyet_yas_frekans'){
            /**
             * Türkiye Geneli Cinsiyet ve Yaş Frekansları
             * **/

            $.ajax({
                async:false,
                type: "GET",
                url: "/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslarini_hesapla",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    var chartUrl = '/tig/donemsel_turkiye_geneli_cinsiyet_yas_frekanslarini_toplam_hesapla?from='+period_from+'&to='+period_to;

                    setDataToChart(chartUrl,cinsiyet_yas_chart);

                    $('table#cinsiyet_yas_frekans_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='tig_bazli_cinsiyet_yas_frekans'){
            /**
             * Türkiye Geneli Tig Bazlı Cinsiyet ve Yaş Frekansları
             * **/

            $.ajax({
                async:false,
                type: "GET",
                url: "/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslarini_hesapla",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    var chartUrl = '/tig/donemsel_turkiye_geneli_tig_bazli_cinsiyet_yas_frekanslarini_toplam_hesapla?from='+period_from+'&to='+period_to;

                    setDataToChart(chartUrl,tig_bazli_cinsiyet_yas_chart);

                    $('table#tig_bazli_cinsiyet_yas_frekans_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='tig_bazli_gunubirlik_ve_diger'){
            /**
             * Türkiye Geneli Tig Bazlı Günübirlik ve Diğer Yatışlardaki Vaka Frekansları
             * **/

            $.ajax({
                async:false,
                type: "GET",
                url: "/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari_hesapla",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    var chartUrl = '/tig/donemsel_turkiye_geneli_tig_bazli_gunubirlik_ve_diger_yatislardaki_vaka_frekanslari_toplam_hesapla?from='+period_from+'&to='+period_to;

                    setDataToChart(chartUrl,tig_bazli_gunubirlik_ve_diger_vaka_frekans_chart);

                    $('table#tig_bazli_gunubirlik_ve_diger_yatis_frekans_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='dogum_frekans'){
            /**
             * Türkiye Geneli Kurum Türü Bazında Doğum Sayıları
             * **/

             
            $.ajax({
                async:false,
                type: "GET",
                url: "/dogum/dogum_frekans_verilerini_kurum_turu_bazinda_hesapla",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);
                    
                    setDataToChart(this.url,dogum_turleri_devlet_ozel_universite_chart);

                    $.each(res, function( index, value ) {
                      if(res[index].dogum_tur=='Normal Doğum'){
                        $('div.total-ndogum').text(res[index].toplam);
                      }else if(res[index].dogum_tur=='Sezaryan Doğum'){
                        $('div.total-sdogum').text(res[index].toplam);
                      }else if(res[index].dogum_tur=='Primer Sezaryan Doğum'){
                        $('div.total-pdogum').text(res[index].toplam);
                      }else if(res[index].dogum_tur=='Müdehaleli Doğum'){
                        $('div.total-mdogum').text(res[index].toplam);
                      }
                    });

                    //$('table#tig_bazli_gunubirlik_ve_diger_yatis_frekans_tbl').bootstrapTable('load', res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='yd_kilo'){ 
           ydKiloUrl = '/dogum/yd_kilo_verilerini_hesapla?from='+period_from+'&to='+period_to;
           makeYDKiloVis(ydKiloUrl);
           setTimeout(function(){preloader.active(false)}, 2000);
        }else if(dataType=='nsdogum_anatani_ektani'){
           dataUrl = '/dogum/normal_sezaryen_dogum_anatani_ektani_verilerini_hesapla?from='+period_from+'&to='+period_to;
           makeDgmTaniVis(dataUrl);
           setTimeout(function(){preloader.active(false)}, 2000);
        }else if(dataType=='tani_yas_cinsiyet'){
           dataUrl = '/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilarini_hesapla?from='+period_from+'&to='+period_to;
           makeTaniYasCinsiyetVis(dataUrl);
           setTimeout(function(){preloader.active(false)}, 2000);
        }else if(dataType=='hastane_bazli_drg_say'){
                var url = '/tig/donemsel_hastanebazli_drg_sayilarini_getir?from='+period_from+'&to='+period_to;
                $('table#drg_hospitals_table').bootstrapTable('refresh',{url: url});
                setTimeout(function(){preloader.active(false)}, 3000);
        } else if(dataType=='donemsel_hastane_tig_yatis_verileri'){

            /**
             * Hastane Yatış Bazlı Tig verileri.Kodlayıcıların isimlerini de içerir.
             */
            var hospitals = $("select#hospitals").val();

            if(hospitals!='undefined' && hospitals!=null) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/tig/donemsel_hastane_tig_yatis_verilerini_getir",
                    dataType: 'json',
                    data: {from: period_from, to: period_to, hospitals: hospitals},
                    success: function (res) {

                        $('table#donemsel_hastane_tig_yatis_tbl').bootstrapTable('load', res);

                        preloader.active(false);
                    },
                    error: function (res) {
                        console.log("Error: " + res)
                    }
                });
            }else{
                preloader.active(false);
                alert('Hastane seçilmedi!');
            }
        } else if(dataType=='tr_vaka_sayilari'){
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/tig/donemsel_turkiye_geneli_vaka_sayilarini_getir",
                    dataType: 'json',
                    data: {from: period_from, to: period_to},
                    success: function (res) {

                        $('table#vaka_tbl').bootstrapTable('load', res);

                        preloader.active(false);
                    },
                    error: function (res) {
                        console.log("Error: " + res)
                    }
                });
        }else if(dataType=='donemsel_hastane_verileri'){
            $.ajax({
                async: false,
                type: "POST",
                url: "/tig/donemsel_hastane_verilerini_getir",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    $('table#hastane_verileri').bootstrapTable('load',res);


                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='donemsel_turkiye_geneli_tig_verileri'){
            $.ajax({
                async: false,
                type: "POST",
                url: "/tig/donemsel_turkiye_geneli_tig_verilerini_getir",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    $('table#genel_tig_tbl').bootstrapTable('load',res);


                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='donemsel_turkiye_geneli_drg_verileri'){
            $.ajax({
                async: false,
                type: "POST",
                url: "/tig/donemsel_turkiye_geneli_drg_bazli_verilerini_hesapla",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {

                    preloader.active(false);

                    $('table#drg_tbl').bootstrapTable('load',res);


                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='donemsel_turkiye_geneli_klinik_kod_verileri'){
            $.ajax({
                async: false,
                type: "POST",
                url: "/tig/donemsel_turkiye_geneli_klinik_kod_verileri_getir",
                dataType: 'json',
                data: {from: period_from, to: period_to},
                success: function (res) {
                    preloader.active(false);
                    $('table#klinik_tbl').bootstrapTable('load',res);
                },
                error: function (res) {
                    console.log("Error: " + res)
                }
            });
        }else if(dataType=='oecd_donemsel_hastane_bazli_klinik_veri'){
            var hospital = $('select#hospitals').val();
            var url = '/tig/oecd_donemsel_hastane_bazli_verileri_getir?from='+period_from+'&to='+period_to+'&hospital='+hospital;
            var export_url ='/tig/oecd_donemsel_hastane_bazli_verileri_getir?export=true&from='+period_from+'&to='+period_to+'&hospital='+hospital; 


            $('table#oecd_hastane_klinik_verileri').bootstrapTable('destroy');
           
            $('table#oecd_hastane_klinik_verileri').bootstrapTable({
                        url:url,
                        pagination: true,
                        sidePagination: "server"
            
            });
            
            preloader.active(false);

            $('a#export_oecd').attr("href",export_url);
        }else if(dataType=='brans_bazli_hastane_emek_verileri'){

            axios.post('/tig/brans_bazli_hastane_emek_verilerini_getir', {
                from: period_from, 
                to: period_to
             })
              .then(function (response) {
                preloader.active(false);
                $('table#brans_emek_hastane_verileri').bootstrapTable('load',response);
              })
              .catch(function (error) {
                console.log(error);
              });
        }else if(dataType=='kodlayici_tig_verileri'){
            var hospital = $('select#hospitals').val();
            //klinik_kodlayici_bazli_tig_verileri
            axios.post('/tig/klinik_kodlayici_tig_verilerini_getir', {
                from: period_from, 
                to: period_to,
                hospital:hospital
             })
              .then(function (response) {
                preloader.active(false);
                $('table#kodlayici_tig_verileri').bootstrapTable('load',response);
              })
              .catch(function (error) {
                console.log(error);
              });
        }





    });




});