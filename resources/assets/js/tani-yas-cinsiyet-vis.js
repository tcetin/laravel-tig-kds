var path = window.location.pathname;

if(path=='/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilari'){

//tig-custom.js
if(dataUrl=='' || dataUrl===undefined){
        dataUrl='/tig/donemsel_turkiye_geneli_yas_ve_cinsiyete_gore_tani_sayilarini_hesapla';
        makeTaniYasCinsiyetVis(dataUrl);
}     


var taniTipChart = dc.pieChart('#tani_tip'),
    taniCinsiyetChart = dc.pieChart('#tani_cinsiyet'),
    taniYasChart = dc.rowChart('#tani_yas'),
    taniChart = dc.rowChart('#tani'),
    dctable = $('table#tani_yas_cinsiyet_tbl');

function makeTaniYasCinsiyetVis(dataUrl){ 
    d3.json(dataUrl, function(data) {

    	dctable.bootstrapTable('load',data);

    	/*------------------------------
         T O O L T İ P S 
         ------------------------------*/
        //row tooltip

	    var tip = d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(function(d) {
				return "<span style='color: #fff;text-shadow: 0 0 3px rgba(255,255,255,0.4);'>" + d.key+"-"+(d.value.desc == 'undefined' ? '' :d.value.desc)+ "</span> : " + (d.value.total=='undefined' ? d.value:d.value.total);
	        });

        // tooltips for pie chart
        var pieTip = d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(function(d) {
             return "<span style='color: #fff;text-shadow: 0 0 3px rgba(255,255,255,0.4);'>" + d.data.key + "</span> : " + d.value;
        });

    	var ndx = crossfilter(data);
        var all = ndx.groupAll();

        var totalSum = all.reduceSum(function(d) {
            return d.Say;
        });

        var taniTipDim = ndx.dimension(function(d) {
            return d.TaniTip
        }),taniTipGroup = taniTipDim.group().reduceSum(function(d) {
            return d.Say;
        }),taniCinsiyetDim = ndx.dimension(function(d) {
            return d.Cinsiyet
        }),taniCinsiyetGroup = taniCinsiyetDim.group().reduceSum(function(d) {
            return d.Say;
        }),taniYasDim = ndx.dimension(function(d) {
            return d.YasAralik
        }),taniYasGroup = taniYasDim.group().reduceSum(function(d) {
            return d.Say;
        }),taniDim = ndx.dimension(function(d) {
            return d.TaniKod;
        }),taniGroup = taniDim.group().reduce(
            // add
            function(p,v) {
                p.code = v.TaniKod;
                p.desc = v.TANIM;
                p.total += v.Say;
                return p;    
            },
            // remove
            function(p,v) {
                p.code = v.TaniKod;
                p.desc = v.TANIM;
                p.total -= v.Say; 
                return p;   
            },
            // init
            function(p,v) {
                return { code:'',desc:'',total:0}
            }
        );

        taniTipChart
            .width(500).height(500).slicesCap(4).innerRadius(100)
            .group(taniTipGroup) // set group
            .dimension(taniTipDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniTipDim.top(Infinity));
                $("span.total_say").text(totalSum.value());
            })
            .valueAccessor(function(p) {
                return p.value;
            }).renderLabel(true).label(function(d) {
                return d.key + ':' + d.value + ' (%' + ((d.value / totalSum.value()) * 100).toFixed(2) + ')';
            }).title(function(d) {
                return d.key + ": " + d.value;
            }).legend(dc.legend()).renderTitle(true);

         taniCinsiyetChart
            .width(500).height(500).slicesCap(4).innerRadius(100)
            .group(taniCinsiyetGroup) // set group
            .dimension(taniCinsiyetDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniCinsiyetDim.top(Infinity));
                $("span.total_say").text(totalSum.value());
            })
            .valueAccessor(function(p) {
                return p.value;
            }).renderLabel(true).label(function(d) {
                return d.key + ':' + d.value + ' (%' + ((d.value / totalSum.value()) * 100).toFixed(2) + ')';
            }).title(function(d) {
                return d.key + ": " + d.value;
            }).legend(dc.legend()).renderTitle(true);

        taniYasChart.width(1200) // (optional) define chart width, :default = 200
            .height(500) // (optional) define chart height, :default = 200  
            .group(taniYasGroup) // set group
            .dimension(taniYasDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniYasDim.top(Infinity));
                $("span.total_say").text(totalSum.value());
            })
            .title(function(d) {
                return d.key + ": " + d.value;
            })
            .renderLabel(true)
            .label(function (d){
       			return d.key + " (" + d.value +")";
    		})
            .ordering(function(d) {
                return -d.value;
            }).xAxis().ticks(20);

        taniChart.width(1200) // (optional) define chart width, :default = 200
            .height(300000) // (optional) define chart height, :default = 200  
            .group(taniGroup) // set group
            .dimension(taniDim) // set dimension
            .valueAccessor(function(p) { 
                return p.value.total; 
            }) 
            .on("filtered", function() {
                dctable.bootstrapTable('load', taniDim.top(Infinity));
                $("span.total_say").text(totalSum.value());
            }).renderLabel(true)
            .label(function (d){
       			return d.key + "(" + d.value.total +")";
    		})
            .title(function(d) {
                return d.value.code+"-"+d.value.desc+ ":" + d.value.total;
            }).ordering(function(d) {
                return -d.value.total;
            }).xAxis().ticks(20);

        $("span.total_say").text(totalSum.value());


        dc.renderAll();

       /*----------------------------------
         C A L L  T O O L T I P S
         ----------------------------------*/
        //ROW TOOL TIP
        d3.selectAll("g.row").call(tip);
        d3.selectAll("g.row").on('mouseover', tip.show).on('mouseout', tip.hide);
        //PIE TOOL TIP
        d3.selectAll(".pie-slice").call(pieTip);
        d3.selectAll(".pie-slice").on('mouseover', pieTip.show)
               .on('mouseout', pieTip.hide);




 });
}//end function
}//endif