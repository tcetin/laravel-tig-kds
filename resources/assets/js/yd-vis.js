var path = window.location.pathname;

if(path=='/dogum/yd_kilo_verileri'){


var ydKiloChart = dc.rowChart('#yd_kilo'),
    ydKiloKurumChart = dc.pieChart('#yd_kilo_kurum_statu'),
    ydKiloCinsiyetChart = dc.pieChart('#yd_kilo_cinsiyet'),
    ydMap='', 
    mapData='',
    dctable = $('table#yd_kilo_frekans_tbl');

     

//tig-custom.js
if(ydKiloUrl=='' || ydKiloUrl===undefined){
        ydKiloUrl='/dogum/yd_kilo_verilerini_hesapla';
        makeYDKiloVis(ydKiloUrl);
}  

function makeYDKiloVis(ydKiloUrl){
    d3.json(ydKiloUrl, function(data) {

        dctable.bootstrapTable('load',data);

        /*------------------------------
         T O O L T İ P S
         ------------------------------*/
        //row tooltip
        var tip = d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(function(d) {
            return "<span style='color: #fff;text-shadow: 0 0 3px rgba(255,255,255,0.4);'>" + d.key + "</span> : " + d.value;
        });
        // tooltips for pie chart
        var pieTip = d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(function(d) {
             return "<span style='color: #fff;text-shadow: 0 0 3px rgba(255,255,255,0.4);'>" + d.data.key + "</span> : " + d.value;
        });

        var ndx = crossfilter(data);
        var all = ndx.groupAll();
        var totalSum = all.reduceSum(function(d) {
            return d.KiloDeger;
        });
        var kiloDim = ndx.dimension(function(d) {
            return d.KiloAralik
        });
        var kiloGroup = kiloDim.group().reduceSum(function(d) {
            return d.KiloDeger;
        });
        //Kurum
        var kurumStatuDim = ndx.dimension(function(d) {
            return d.Statu
        });
        var kurumStatuGroup = kurumStatuDim.group().reduceSum(function(d) {
            return d.KiloDeger
        });
        //Cinsiyet
        var cinsiyetDim = ndx.dimension(function(d) {
            return d.Cinsiyet
        });
        var cinsiyetGroup = cinsiyetDim.group().reduceSum(function(d) {
            return d.KiloDeger
        });

        //İller
        var ilDim = ndx.dimension(function(d) { 
            return d.id //"TR-"
        });
        var ilGroup = ilDim.group().reduceSum(function(d) {
            return d.KiloDeger
        });

        mapData= ilGroup.top(Infinity);
        //change key to id
        for (var i = mapData.length - 1; i >= 0; i--) {
             mapData[i].id = mapData[i]['key'];
             delete mapData[i].key;
        }



        //Charts
        ydKiloChart.width(1200) // (optional) define chart width, :default = 200
            .height(500) // (optional) define chart height, :default = 200
            .group(kiloGroup) // set group
            .dimension(kiloDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', kiloDim.top(Infinity));
                ydMap.dataProvider.areas = mapData;
                ydMap.validateData();
            }).renderLabel(true).title(function(d) {
                return d.key + ":" + d.value;
            }).ordering(function(d) {
                return d.value;
            }).xAxis().ticks(20);

        ydKiloKurumChart.width(600).height(500).slicesCap(4).innerRadius(100).dimension(kurumStatuDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', kurumStatuDim.top(Infinity));
                ydMap.dataProvider.areas = mapData;
                ydMap.validateData();
            }).group(kurumStatuGroup) // set group
            .valueAccessor(function(p) {
                return p.value;
            }).renderLabel(true).label(function(d) {
                return d.key + ':' + d.value + ' (%' + ((d.value / totalSum.value()) * 100).toFixed(2) + ')';
            }).title(function(d) {
                return d.key + ": " + d.value;
            }).legend(dc.legend()).renderTitle(true);

        ydKiloCinsiyetChart.width(600).height(500).slicesCap(4).innerRadius(100).dimension(cinsiyetDim) // set dimension
            .on("filtered", function() {
                dctable.bootstrapTable('load', cinsiyetDim.top(Infinity));
                ydMap.dataProvider.areas = mapData;
                ydMap.validateData();

            }).group(cinsiyetGroup) // set group
            .valueAccessor(function(p) {
                return p.value;
            }).renderLabel(true).label(function(d) {
                return d.key + ':' + d.value + ' (%' + ((d.value / totalSum.value()) * 100).toFixed(2) + ')';
            }).title(function(d) {
                return d.key + ": " + d.value;
            }).legend(dc.legend()).renderTitle(true);

        //Export AS CSV 
        $(document).on("click", "a.download-chart", function() {
            if ($(this).attr("chart-dim") == "yd_kilo") {
                var blob = new Blob([d3.csv.format(kiloGroup.top(Infinity))], {
                    type: "text/csv;charset=utf-8"
                });
                saveAs(blob, 'kilo_bazlı_yd_sayıları.csv');
            } else if ($(this).attr("chart-dim") == "kurum_statu") {
                var blob = new Blob([d3.csv.format(kurumStatuGroup.top(Infinity))], {
                    type: "text/csv;charset=utf-8"
                });
                saveAs(blob, 'kurum_statü_bazlı_yd_sayıları.csv');
            } else if ($(this).attr("chart-dim") == "cinsiyet") {
                var blob = new Blob([d3.csv.format(cinsiyetGroup.top(Infinity))], {
                    type: "text/csv;charset=utf-8"
                });
                saveAs(blob, 'cinsiyet_bazlı_yd_sayıları.csv');
            }
        });


        //map

        /*İllerdeki YeniDoğan Sayıları*/
        ydMap = AmCharts.makeChart( "yd_kilo_il", {
                  "type": "map",
                  "theme": "light",
                  "colorSteps": 10,
                  "dataProvider": {
                        "map": "turkeyLow",
                        "areas": mapData
                   },
                  "areasSettings": {
                    "autoZoom": false,
                    "selectable":true
                  },

                  "valueLegend": {
                    "right": 10,
                    "minValue": "En Az",
                    "maxValue": "En Çok"
                  },
                  "imagesSettings": {
                    "labelPosition": "middle",
                    "labelFontSize": 8
                  },

                  "export": {
                    "enabled": true,
                    "fileName":"İl Bazında Yenidoğan Sayıları"
                  },
                   "listeners": [{
                    "event": "clickMapObject",
                    "method": function(event) {
                        var cityCode = event.mapObject.id;
                        ilDim.filter(cityCode);
                        dctable.bootstrapTable('load', ilDim.top(Infinity));
                        dc.redrawAll();
                    }
                  }]

                } );

        ydMap.balloonLabelFunction = function (area, map) {      
                var valueText = "<strong><u><h6>"+area.title+"</h6></u></strong>"+"<br/>"+
                                "<p><strong>Yenidoğan Sayısı:</strong>"+area.value+"</p>";
                return valueText;                
            }

        $(document).on("click","a.reset-all",function(){
            ilDim.filterAll();

        });    



        dc.renderAll();
        /*----------------------------------
         C A L L  T O O L T I P S
         ----------------------------------*/
        //ROW TOOL TIP
        d3.selectAll("g.row").call(tip);
        d3.selectAll("g.row").on('mouseover', tip.show).on('mouseout', tip.hide);
        //PIE TOOL TIP
        d3.selectAll(".pie-slice").call(pieTip);
        d3.selectAll(".pie-slice").on('mouseover', pieTip.show)
               .on('mouseout', pieTip.hide);
    });
}
}