FROM php:7.1-fpm

RUN docker-php-ext-install pdo &&  docker-php-ext-install pdo_mysql

#####################################
# xDebug:
#####################################

ARG INSTALL_XDEBUG=true
RUN if [ ${INSTALL_XDEBUG} = true ]; then \
    # Install the xdebug extension
    pecl install xdebug && \
    docker-php-ext-enable xdebug \
;fi

# Copy xdebug configration for remote debugging
COPY .docker/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini


#####################################
# Git:
#####################################
RUN  apt-get update
RUN  apt-get install -y git


#####################################
# ZipArchive:
#####################################
RUN apt-get install -y zip unzip

#####################################
# Composer:
#####################################
RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer



RUN usermod -u 1000 www-data

WORKDIR /var/www

CMD ["php-fpm"]

EXPOSE 9000
EXPOSE 9001